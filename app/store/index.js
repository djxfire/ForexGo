import QuotationStore from './quotation';
import FavorateStore from './favorate';
import UserStore from './user';

const quotationStore = new QuotationStore();
const favorateStore = new FavorateStore();
const userStore = new UserStore();
//export default { quotationStore, favorateStore };
export default {
  quotationStore,
  favorateStore,
  userStore,
};
