import { observable, action } from 'mobx';
import OChart from '@/ochart/core/OChart';
import markvoWorker from '@/workers/markvoTool';
export default class QuotationStore {
    @observable quotations = {};

    constructor() {
        XUE.Ws.subscribe(
          [{
              title: 'QUOTATION_PUSH',
              callback: (response) => {
                  //console.log(data);
                if (response.id !== 'QuotationStore') {
                  return;
                }
                const dataArr = JSON.parse(response.data) || [];
                let quotations = {};
                for (let i = 0; i < dataArr.length; i++) {
                  quotations[dataArr[i].code] = Object.assign({}, dataArr[i]);
                  OChart.updateMinData(dataArr[i].code, dataArr[i].close);
                }
                let kmeans = localStorage.getItem('markvo');
                const vector = [
                  quotations['XAU'].pcg,
                  quotations['USDJPY'].pcg,
                  quotations['USDCAD'].pcg,
                  quotations['USDCHF'].pcg,
                  quotations['GBPUSD'].pcg,
                  quotations['EURUSD'].pcg,
                  quotations['AUDUSD'].pcg,
                  quotations['NZDUSD'].pcg,
                ];
                markvoWorker.postMessage({
                  vector,
                  kmeans,
                });
                this.quotations = Object.assign({}, this.quotations, quotations);
              }
          }], 'QuotationStore');
    }
}
