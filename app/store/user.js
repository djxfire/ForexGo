/*
* @Date: 2021/2/27
* @Author: XueChengwu <xuechengwu@erayt.com>
* @Copyright: 2015-2019 Erayt, Inc.
* @Description: If you have some questions, please contact: xuechengwu@erayt.com.
*/
import { observable, action } from 'mobx';
import Storage from 'utils/storage';

export default class UserStore{
  @observable user = {};
  @observable stars = [];
  constructor() {
    const user = Storage.get('user', true, {});
    this.user = Object.assign({}, user);
    XUE.addHandler('user:star:refresh', () => {
      XUE.fetch('local/api/user/stars')
        .then(res => {
          if (Number(res.code) === 0) {
            const { stars = [] } = res.data;
            console.log('stars===>', stars);
            this.stars = stars;
          }
        });
    });
    XUE.executeHandler('user:star:refresh');
  }
  @action setUser(user) {
    this.user = Object.assign({}, user);
  }
}
