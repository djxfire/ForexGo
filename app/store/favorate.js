import { observable, action } from 'mobx';
import Storage from '@/utils/storage';

export default class FavorateStore {
    @observable favorates = [];

    constructor() {
        const favorate = Storage.get('Favorate', true, []);
        console.log('favorate', favorate);
        this.favorates = favorate;
    }

    /**
     * 添加自选
     * @param favorates
     */
    @action add(favorates) {
        if (favorates instanceof Array) {
            for (let i = 0; i < favorates.length; i++) {
                if (this.favorates.indexOf(favorates[i]) !== -1) {
                    this.favorates.push(favorates[i]);
                }
            }
        } else {
            const favorateIndex = this.favorates.indexOf(favorates);
            if (favorateIndex === -1) {
                this.favorates.push(favorates);
            }
        }
        Storage.set('Favorate', this.favorates.concat());
    }

    /**
     * 清空自选
     */
    @action clear() {
        this.favorates.splice(0, this.favorates.length);
        Storage.set('Favorate', this.favorates.concat());
    }

    /**
     * 移除自选
     * @param favorate
     */
    @action remove(favorate) {
        const index = this.favorates.indexOf(favorate);
        if (index !== -1) {
            this.favorates.splice(index, 1);
        }
        Storage.set('Favorate', this.favorates.concat());
    }
}
