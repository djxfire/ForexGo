import React from 'react';
import ReactDOM from 'react-dom';
import './common';
import global from './golbal';
import { Provider } from 'mobx-react';
import stores from './store';
import './app.scss';
import './styles/fonts/iconfont';
import AppRouter from './router';


function Application() {
    return (
        <Provider {...stores}>
            <AppRouter />
        </Provider>
    )
}
console.log(window.XUE)
global().then(() => {
  ReactDOM.render(<Application />, document.getElementById('root'));
});

