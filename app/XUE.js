/*
* @Date: 2021/2/14
* @Author: XueChengwu <xuechengwu@erayt.com>
* @Copyright: 2015-2019 Erayt, Inc.
* @Description: If you have some questions, please contact: xuechengwu@erayt.com.
*/
import ws from './utils/ws';
import Net from './net/Net';
console.log(window.XUE)
if(!window.XUE) {
  window.XUE = {};
}
window.XUE.Ws = ws;
window.XUE.fetch = Net;
console.log(window.XUE)
