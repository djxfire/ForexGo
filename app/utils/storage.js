const Storage = {
  get(key, decode = false, defaultValue = '') {
    const target = localStorage.getItem(key);
    if (!target) {
      return defaultValue
    }
    if(decode) {
      return JSON.parse(target);
    }
    return target
  },

  set(key, value) {
    typeof value === 'object' ?
        localStorage.setItem(key, JSON.stringify(value)) :
        localStorage.setItem(key, value);
  }
}

export default Storage
