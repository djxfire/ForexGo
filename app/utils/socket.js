/*
* @Date: 2021/2/14
* @Author: XueChengwu <xuechengwu@erayt.com>
* @Copyright: 2015-2019 Erayt, Inc.
* @Description: If you have some questions, please contact: xuechengwu@erayt.com.
*/
function createWebSocket(title, socket) {
  let ws = null;
  if (window.WebSocket) ws = new WebSocket( title);
  if (window.MozWebSocket) ws = new MozWebSocket(title);
  ws.onopen = (e) => {
    // 将socket从penddings中移入running中
    let sock = socket.pool.penddings.find(item => item.uuid === socket.uuid);
    if (sock !== null && sock !== undefined) {
      sock.pool.runnings.push(sock);
      sock.pool.penddings.splice(sock.pool.penddings.indexOf(sock), 1);
    }
    // 检测该socket是否在stopping中，如果在stoppings中，则将其移入runnings中
    sock = socket.pool.stoppings.find(item => item.uuid === socket.uuid);
    if (sock !== null && sock !== undefined) {
      sock.pool.runnings.push(sock);
      sock.pool.stoppings.splice(sock.pool.stoppings.indexOf(sock), 1);
    }
  };
  ws.onclose = (e) => {
    // 将socket从runnings中移入stopping中
    let sock = socket.pool.runnings.find(item => item.uuid === socket.uuid);
    if (sock !== null && sock !== undefined) {
      socket.pool.stoppings.push(sock);
      socket.pool.runnings.splice(socket.pool.runnings.indexOf(sock), 1);
      // 判断是否需要重连
      if (socket.isValid && sock.timeout > 0) {
        // 断线重连
        socket.ws = createWebSocket(title, sock);
        sock.init();
      }
    } else {
      // 连接阶段断开重连
      sock = socket.pool.penddings.find(item => item.uuid === socket.uuid);
      if (sock !== null && sock !== undefined) {
        sock.pool.stoppings.push(sock);
        sock.pool.penddings.splice(socket.pool.penddings.indexOf(sock), 1);
        if (sock.isValid && sock.timeout > 0) {
          // 连接失败，重连
          sock.ws = createWebSocket(title, sock);
          sock.init();
        }
      }
    }
  };

  ws.onerror = (e) => {
    let sock = socket.pool.penddings.find(item => item.uuid === socket.uuid);
    if (sock !== null && sock !== undefined) {
      sock.pool.stoppings.push(sock);
      sock.pool.penddings.splice(socket.pool.penddings.indexOf(sock), 1);
      // 连接失败，重连
      if (sock.isValid && sock.timeout > 0) {
        sock.ws = createWebSocket(title, sock);
        sock.init();
      }
    } else {
      // 运行时期错误， 断线重连
      sock = socket.pool.runnings.find(item => item.uuid === socket.uuid);
      if (sock !== null && sock !== undefined) {
        sock.pool.stoppings.push(sock);
        sock.pool.runnings.splice(socket.pool.runnings.indexOf(sock), 1);
        // 断线重连
        if (sock.isValid && sock.timeout > 0) {
          sock.ws = createWebSocket(title, sock);
          sock.init();
        }
      } else {
        // 从停止的连接中查找，断线重连
        sock = socket.pool.stoppings.find(item => item.uuid === socket.uuid);
        if (sock !== null && sock !== undefined) {
          sock.pool.penddings.push(sock);
          sock.pool.stoppings.splice(sock.pool.stoppings.indexOf(sock), 1);
          // 断线重连
          if (sock.isValid && sock.timeout > 0) {
            sock.ws = createWebSocket(title, sock);
            sock.init();
          }
        }
      }
    }
  };
  return ws;
}

let uuid = 0;

class Socket {
  constructor(pool, title) {
    this.uuid = uuid++; // socket 标识符
    this.isValid = true;
    this.pool = pool;
    this.title = title;
    const socket = this.pool.penddings.find(item => item.uuid === this.uuid)
    if (socket === null || socket === undefined) {
      this.pool.penddings.push(this);
    }
    this.ws = createWebSocket(title, this);
    this.onMessage = null;
    this.timeout = 60; // 进行60此重连
    this.init();
  }

  init() {
    this.ws.onmessage = (e) => {
      this.onMessage && this.onMessage(e.data);
    };
    this.timeout--
  }

  setOnMessage(callback) {
    this.onMessage = callback;
  }

  close() {
    this.isValid = false;
    this.ws.close();
  }
}

class Ws {
  constructor() {
    this.penddings = []; // 准备中的socket
    this.runnings = []; // 运行中的socket
    this.stoppings = []; // 即将停止的socket
    const sock = new Socket(this, title);
    sock.setOnMessage(callback);
  }

  subscribe(title, callback) {


  }

  stopSocket(uid, title) {
    // 如果uid不为空，则停止uid,否则停止title
  }
}

const ws = new Ws();
export default ws;

