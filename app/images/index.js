import 中国 from './中国.png';
import 丹麦 from './丹麦.png';
import 乌克兰 from './乌克兰.png';
import 以色列 from './以色列.png';
import 俄罗斯 from './俄罗斯.png';
import 加拿大 from './加拿大.png';
import 匈牙利 from './匈牙利.png';
import 南非 from './南非.png';
import 印尼 from './印尼.png';
import 印度 from './印度.png';
import 哥伦比亚 from './哥伦比亚.png';
import 土耳其 from './土耳其.png';
import 墨西哥 from './墨西哥.png';
import 尼日利亚 from './尼日利亚.png';
import 巴西 from './巴西.png';
import 挪威 from './挪威.png';
import 捷克 from './捷克.png';
import 新西兰 from './新西兰.png';
import 日本 from './日本.png';
import 智利 from './智利.png';
import 欧盟 from './欧盟.png';
import 波兰 from './波兰.png';
import 泰国 from './泰国.png';
import 澳大利亚 from './澳大利亚.png';
import 瑞典 from './瑞典.png';
import 瑞士 from './瑞士.png';
import 罗马尼亚 from './罗马尼亚.png';
import 美国 from './美国.png';
import 英国 from './英国.png';
import 菲律宾 from './菲律宾.png';
import 韩国 from './韩国.png';
import 中国香港 from './中国香港.png';
import 西班牙 from './西班牙.png';
import 德国 from './德国.png';
import 意大利 from './意大利.png';

export default {
    中国,
    丹麦,
    乌克兰,
    以色列,
    俄罗斯,
    加拿大,
    匈牙利,
    南非,
    印尼,
    印度,
    哥伦比亚,
    土耳其,
    墨西哥,
    尼日利亚,
    巴西,
    挪威,
    捷克,
    新西兰,
    日本,
    智利,
    欧盟,
    波兰,
    泰国,
    澳大利亚,
    瑞典,
    瑞士,
    罗马尼亚,
    美国,
    英国,
    菲律宾,
    韩国,
    中国香港,
    西班牙,
    德国,
    欧元区: 欧盟,
    意大利
}
