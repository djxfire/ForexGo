let quoteWorker = new Worker('./quote.js');
export default function getQuote(name, params) {
    let eventId = XUE.getGuid();
    quoteWorker.postMessage({
        eventId,
        name,
        params,
    });
    return new Promise((resolve, reject) => {
        quoteWorker.addEventListener('message', (e) => {
            const { result } = e.data;
            if (eventId === e.data.eventId) {
                resolve(result);
            }
        });
    });
}
