import macd from 'ochart/quota/macd';
import kdj from 'ochart/quota/kdj';
import bias from 'ochart/quota/bias';
import rsi from 'ochart/quota/rsi';
import cci from 'ochart/quota/cci';
import ad from 'ochart/quota/ad';
import ma from 'ochart/quota/ma';
import arbr from 'ochart/quota/arbr';
import aroon from 'ochart/quota/aroon';
import atr from 'ochart/quota/atr';
import cmo from 'ochart/quota/cmo';
import cr from 'ochart/quota/cr';
import cv from 'ochart/quota/cv';
import dma from 'ochart/quota/dma';
import dmi from 'ochart/quota/dmi';
import dpo from 'ochart/quota/dpo';
import emv from 'ochart/quota/emv';
import forceIndex from 'ochart/quota/forceIndex';
import mfi from 'ochart/quota/mfi';
import mtm from 'ochart/quota/mtm';
import nvi from 'ochart/quota/nvi';
import obv from 'ochart/quota/obv';
import psy from 'ochart/quota/psy';
import pvi from 'ochart/quota/pvi';
import roc from 'ochart/quota/roc';
import rvi from 'ochart/quota/rvi';
import trix from 'ochart/quota/trix';
import vhf from 'ochart/quota/vhf';
import vr from 'ochart/quota/vr';
import wad from 'ochart/quota/wad';
import wms from 'ochart/quota/wms';
import wvad from 'ochart/quota/wvad';
self.addEventListener('message', (e) => {
    const { params, name, eventId } = e.data;
    if (name === 'macd') {
        const { close, DIFLen, DEALen, M } = params;
        const { DIF, DEA, BAR } = macd(close, DIFLen, DEALen, M);
        self.postMessage({
            eventId,
            result: {
                DIF,
                DEA,
                BAR,
            },
        });
    } else if (name === 'kdj') {
        const { high, low, close, N, M, L, S } = params;
        const { KValue, DValue, JValue } = kdj(high, low, close, N, M, L, S);
        self.postMessage({
            eventId,
            result: {
                KValue,
                DValue,
                JValue,
            },
        });
    } else if (name === 'bias') {
        const { N1, N2, N3, close } = params;
        const biasN1 = bias(close, N1);
        const biasN2 = bias(close, N2);
        const biasN3 = bias(close, N3);
        self.postMessage({
            eventId,
            result: {
                biasN1,
                biasN2,
                biasN3,
            },
        });
    } else if (name === 'rsi') {
        const { N1, N2, N3, close } = params;
        console.log('close', close);
        const rsiN1 = rsi(close, N1);
        const rsiN2 = rsi(close, N2);
        const rsiN3 = rsi(close, N3);
        self.postMessage({
            eventId,
            result:{
                rsiN1,
                rsiN2,
                rsiN3,
            }
        });
    } else if (name === 'cci') {
        const { high, low, close, N } = params;
        const CCI = cci(high, low, close, N);
        self.postMessage({
            eventId,
            result: {
                CCI
            },
        });
    } else if (name === 'ad') {
        const { high, low, close, volume, N = 6} = params;
        const AD = ad(high, low, close, volume);
        const ma1 = ma(AD, N);
        const Chaikin = [];
        for (let i = 0; i < ma1.length; i++) {
            Chaikin.push(AD[i] - ma1[i]);
        }
        self.postMessage({
            eventId,
            result: {
                Chaikin,
            },
        });
    } else if (name === 'arbr') {
        const { open, high, low, close, N } = params;
        const { ar, br } = arbr(open, high, low, close, N);
        self.postMessage({
            eventId,
            result: {
                ar,
                br,
            },
        });
    } else if (name === 'aroon') {
        const { high, low, N } = params;
        const { uparoon, downaroon } = aroon(high, low, N);
        self.postMessage({
            eventId,
            result: {
                uparoon,
                downaroon,
            },
        });
    } else if (name === 'atr') {
        const { high, low, close, N, M } = params;
        const atrV = atr(high, low, close, N);
        const atrMa = ma(atrV, M);
        self.postMessage({
            eventId,
            result: {
                atrV,
                atrMa,
            },
        });
    } else if (name === 'cmo') {
        const { close, N } = params;
        const cmoV = cmo(close, N);
        self.postMessage({
            eventId,
            result: {
                cmoV
            },
        });
    }  else if (name === 'cr') {
        const { high, low, close, N } = params;
        const crV = cr(high, low, close, N);
        self.postMessage({
            eventId,
            result: {
                crV
            },
        });
    } else if (name === 'cv') {
        const { high, low, N } = params;
        const cvV = cv(high, low, N);
        self.postMessage({
            eventId,
            result: {
                cvV,
            },
        });
    } else if (name === 'dma') {
        const {close, fast, slow, smooth} = params;
        const {dmaValue, amaValue} = dma(close, fast, slow, smooth);
        self.postMessage({
            eventId,
            result: {
                dmaValue,
                amaValue,
            },
        });
    } else if (name === 'dmi') {
        const { high, low, close, N } = params;
        const { PosDI, NegDI, ADX } = dmi(high, low, close, N);
        self.postMessage({
            eventId,
            result: {
                PosDI,
                NegDI,
                ADX,
            },
        });
    } else if (name === 'dpo') {
        const { close, N } = params;
        const dpoV = dpo(close, N);
        self.postMessage({
            eventId,
            result: {
                dpoV
            },
        });
    } else if (name === 'emv') {
        const { high, low, volume, N, M } = params;
        const { EMVValue, MAEMVValue } = emv(high, low, volume, N, M);
        self.postMessage({
            eventId,
            result: {
                EMVValue,
                MAEMVValue,
            },
        });
    } else if (name === 'fi') {
        const { close, volume, N } = params;
        const fi = forceIndex(close, volume, N);
        self.postMessage({
            eventId,
            result: {
                fi,
            },
        });
    } else if (name === 'mfi') {
        const { high, low, close, volume, N } = params;
        const mfiValue = mfi(high, low, close, volume, N);
        self.postMessage({
            eventId,
            result: {
                mfiValue,
            },
        });
    } else if (name === 'mtm') {
        const { close, N, M } = params;
        const mtmValue = mtm(close, N);
        const mtmMaValue = ma(mtmValue, M);
        self.postMessage({
            eventId,
            result: {
                mtmValue,
                mtmMaValue,
            },
        });
    } else if (name === 'nvi') {
        const { close, volume, M } = params;
        const nviValue = nvi(close,volume);
        const nviMaValue = ma(nviValue, M);
        self.postMessage({
            eventId,
            result: {
                nviValue,
                nviMaValue,
            },
        });
    } else if (name === 'obv') {
        const { close, volume, M } = params;
        const obvValue = obv(close, volume);
        const obvMaValue = ma(obvValue, M);
        self.postMessage({
            eventId,
            result: {
                obvValue,
                obvMaValue,
            },
        });
    } else if (name === 'psy') {
        const { close, M, N } = params;
        const psyValue = psy(close, N);
        const psyMaValue = ma(psyValue, M);
        self.postMessage({
            eventId,
            result: {
                psyValue,
                psyMaValue,
            },
        });
    } else if (name === 'pvi') {
        const { close, volume, M } = params;
        const pviValue = pvi(close, volume);
        const pviMaValue = ma(pviValue, M);
        self.postMessage({
            eventId,
            result: {
                pviValue,
                pviMaValue,
            },
        });
    } else if (name === 'roc') {
        const { close, N, M } = params;
        const rocValue = roc(close, N);
        const rocMaValue = ma(rocValue, M);
        self.postMessage({
            eventId,
            result: {
                rocValue,
                rocMaValue,
            },
        });
    } else if (name === 'rvi') {
        const { close, S, N, M } = params;
        const rviValue = rvi(close, S, N);
        const rviMaValue = ma(rviValue, M);
        self.postMessage({
            eventId,
            result: {
                rviValue,
                rviMaValue,
            },
        });
    } else if (name === 'trix') {
        const { close, N, S } = params;
        const { trixValue, matrixValue } = trix(close, N, S);
        self.postMessage({
            eventId,
            result: {
                trixValue,
                matrixValue
            },
        });
    } else if (name === 'vhf') {
        const { close, N } = params;
        const vhfValue = vhf(close, N);
        self.postMessage({
            eventId,
            result: {
                vhfValue
            },
        });
    } else if (name === 'vr') {
        const { close, volume, N, M } = params;
        const vrValue = vr(close, volume, N);
        const vrMaValue = ma(vrValue, M);
        self.postMessage({
            eventId,
            result: {
                vrValue,
                vrMaValue,
            },
        });
    } else if (name === 'wad') {
        const { high, low, close, M } = params;
        const wadValue = wad(high, low, close);
        const wadMaValue = ma(wadValue, M);
        self.postMessage({
            eventId,
            result: {
                wadValue,
                wadMaValue,
            },
        });
    } else if (name === 'wms') {
        const { high, low, close, N } = params;
        const wmsValue = wms(high, low, close, N);
        self.postMessage({
            eventId,
            result: {
                wmsValue,
            },
        });
    } else if (name === 'wvad') {
        const { open, high, low, close, volume, N, M } = params;
        const wvadValue = wvad(open, high, low, close, volume, N);
        const wvadMaValue = ma(wvadValue, M);
        self.postMessage({
            eventId,
            result: {
                wvadValue,
                wvadMaValue,
            },
        });
    }
});
