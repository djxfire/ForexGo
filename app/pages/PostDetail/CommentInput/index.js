/*
* @Date: 2021/2/28
* @Author: XueChengwu <xuechengwu@erayt.com>
* @Copyright: 2015-2019 Erayt, Inc.
* @Description: If you have some questions, please contact: xuechengwu@erayt.com.
*/
import React from 'react';
import './index.scss';
import ajax from 'decorate/ajax';
import comment from 'business/comment';
import Toast from 'components/Toast';

@ajax(comment.url, comment.meta)
export default class extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      content: ''
    };
  }

  onChangeComment = (e) => {
    const { value } = e.target;
    this.setState({
      content: value
    });
  }

  onReply = () => {
    const { content = '' } = this.state;
    if (content === '') {
      Toast.toast('请输入评论内容');
      return;
    }
    this.postAjax()
      .then(res => {
        const { onReplyed } = this.props;
        if (Number(res.code) === 0) {
          Toast.toast(res.msg);
          this.setState({
            content: '',
          }, () => {
            XUE.executeHandler('post:comment:update', {});
          });
        } else {
          Toast.toast(res.msg);
        }
      });
  }

  render() {
    const { content = '' } = this.state;
    const { commentName = ''} = this.props;
    return (
      <div className="post-comment-input">
        <input
          type="text"
          onChange={this.onChangeComment}
          value={content}
          placeholder={`回复@${commentName}`}
        />
        <span onClick={this.onReply}>发布</span>
      </div>
    )
  }
}
