/*
* @Date: 2021/2/28
* @Author: XueChengwu <xuechengwu@erayt.com>
* @Copyright: 2015-2019 Erayt, Inc.
* @Description: If you have some questions, please contact: xuechengwu@erayt.com.
*/
import React from 'react';
import Header from 'components/Header';
import './index.scss';
import Post from './Post';
import Comments from './Comments';
import CommentInput from './CommentInput';

export default class extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      post: {},
      commentName: '',
      toId: 0,
    };
  }

  onPostLoad = (post) => {
    console.log('post===>', post);
    this.setState({
      post,
      toId: 0,
      commentName: post.username,
    });
  }

  onCommentClick = (option) => {
    console.log('comment click===>', option);
    this.setState({
      commentName: option.commentName,
      toId: option.toId,
    });
  }

  render() {
    const { id } = this.props.match.params;
    const { commentName, toId } = this.state;
    return (
      <div className="xue-webview-app theme-bg">
        <Header
          backIcon={true}
          onBack={() => this.props.history.go(-1)}
        >
          详情
        </Header>
        <div className="xue-webview-app-content">
          <Post
            post={id}
            onPostLoad={this.onPostLoad}
          />
          <Comments
            post={id}
            onCommentClick={this.onCommentClick}
          />
        </div>
        <CommentInput
          post={id}
          to={toId}
          commentName={commentName}
        />
      </div>
    );
  }
}
