/*
* @Date: 2021/2/28
* @Author: XueChengwu <xuechengwu@erayt.com>
* @Copyright: 2015-2019 Erayt, Inc.
* @Description: If you have some questions, please contact: xuechengwu@erayt.com.
*/
import React from 'react';
import { observer, inject } from 'mobx-react';
import star from 'business/star';
import ajax from 'decorate/ajax';
import './index.scss';
import Toast from 'components/Toast';

@inject('userStore')
@observer
class Star extends React.Component {
  render() {
    const { starUser } = this.props;
    const { stars } = this.props.userStore;
    const isStar = stars.findIndex(vo => vo.userid === starUser);
    if (isStar >= 0) {
      return <div className="star-btn" onClick={this.props.onStar} data-status={1}>取消关注</div>
    }
    return <div className="star-btn" onClick={this.props.onStar} data-status={0}>关注</div>
  }
}

@ajax(star.url, star.meta)
export default class extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      status: 0,
    };
  }

  onStar = (e) => {
    const { status } = e.currentTarget.dataset;
    this.setState({
      status: Number(status),
    }, () => {
      this.postAjax().then((res) => {
        if (Number(res.code) === 0) {
          XUE.executeHandler('user:star:refresh');
        } else {
          Toast.toast(res.msg);
        }
      })
    });
  }

  render() {
    return <Star {...this.props} onStar={this.onStar} />
  }
}
