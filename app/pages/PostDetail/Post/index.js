/*
* @Date: 2021/2/28
* @Author: XueChengwu <xuechengwu@erayt.com>
* @Copyright: 2015-2019 Erayt, Inc.
* @Description: If you have some questions, please contact: xuechengwu@erayt.com.
*/
import React from 'react';
import './index.scss';
import ajax from 'decorate/ajax';
import postDetail from 'business/postDetail';
import Net from '@/net/Net';
import Star from './Star/index';
import Icon from 'components/Icon';
import Vote from 'pages/Home/Bbs/PostItem/Vote';

@ajax(postDetail.url, postDetail.meta, postDetail.resolve)
export default class extends React.Component {
  componentWillMount() {
    XUE.addHandler('post:comment:update', () => {
      this.postAjax();
    });
  }

  render() {
    const {
      username = '', medias = '', avatar = '', topic = '',
      commentNum = 0, content = '', upvote = 0, downvote = 0,
      id, vote = 0, user,
    } = this.props;
    let images = [];
    if (medias !== '') {
      images = medias.split(';');
    }
    return (
      <div className="post-detail">
        <div className="post-item-box">
          <div className="post-item-user">
            <div className="post-item-avatar">
              <img src={`${Net.hostUrl}/local/${avatar}`} />
            </div>
            <div className="post-item-username">
              { username }
            </div>
            <div className="seize" />
            <Star starUser={user} />
          </div>
          <div className="post-item-content">
            { topic !== '' && <span>#{topic}#</span> }
            <span>{content}</span>
          </div>
          <div className="post-item-images">
            {
              images.map(vo => (
                <div className="post-images-item">
                  <img src={`${Net.hostUrl}/local/${vo}`} />
                </div>

              ))
            }
          </div>
        </div>
        <div className="post-comment-info">
          <div className="post-comment-num">
            评论数&nbsp;&nbsp;{commentNum}
          </div>
          <div className="seize" />
          <div className="post-comment-vote">
            赞&nbsp;&nbsp;{ upvote }
          </div>
        </div>
      </div>
    );
  }
}
