/*
* @Date: 2021/2/28
* @Author: XueChengwu <xuechengwu@erayt.com>
* @Copyright: 2015-2019 Erayt, Inc.
* @Description: If you have some questions, please contact: xuechengwu@erayt.com.
*/
import React from 'react';
import ajax from 'decorate/ajax';
import commentList from 'business/commentList';
import Item from './Item';

@ajax(commentList.url, commentList.meta, commentList.resolve)
export default class extends React.Component {
  componentWillMount() {
    XUE.addHandler('post:comment:update', () => {
      this.postAjax();
    });
  }
  render() {
    const { data = [] } = this.props;
    return (
      <div className="post-comments-box">
        {
          data.map(vo => (
            <Item {... vo} onClick={this.props.onCommentClick}/>
          ))
        }
      </div>
    )
  }
}
