/*
* @Date: 2021/2/28
* @Author: XueChengwu <xuechengwu@erayt.com>
* @Copyright: 2015-2019 Erayt, Inc.
* @Description: If you have some questions, please contact: xuechengwu@erayt.com.
*/
import React from 'react';
import Net from '@/net/Net';
import './index.scss';

export default class extends React.Component {
  onItemClick = () => {
    const { author, user, onClick } = this.props;
    onClick && onClick({ commentName: author, toId: user });
  }

  render() {
    const {
      author = '', author2 = '', avatar = '',
      content = '', addtime
    } = this.props;
    return (
      <div className="comment-item-box" onClick={this.onItemClick}>
        <div className="comment-item-left">
          <img src={`${Net.hostUrl}/local/${avatar}`}/>
        </div>
        <div className="comment-item-right">
          <div className="comment-item-username">
            <span>{ author }</span>
            <div className="seize" />
            { XUE.formatDate(new Date(addtime), 'yyyy-MM-dd hh:mm:ss') }
          </div>
          <div className="comment-item-content">
            {
              author2 !== '' && author2 !== null && author2 !== undefined && <span className="comment-to-author">@${author2}:</span>
            }
            {
              content
            }
          </div>
        </div>
      </div>
    );
  }
}
