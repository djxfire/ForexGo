/*
* @Date: 2021/1/2
* @Author: XueChengwu <xuechengwu@erayt.com>
* @Copyright: 2015-2019 Erayt, Inc.
* @Description: If you have some questions, please contact: xuechengwu@erayt.com.
*/
import React from 'react';
import Canvas from '@/ochart/core/Canvas';
import VolumeLayer from './bu/VolumeLayer';
import AxisLayer from '@/ochart/layer/AxisLayer';
import Point from "ochart/core/Point";

export default class extends React.Component {
  constructor(props) {
    super(props);
    this.chartRef = React.createRef();
  }

  componentWillReceiveProps (nextProps, nextContext) {
    const { data = [], totalCount } = nextProps;
    if (
      this.props.data !== data
      || this.props.totalCount !== totalCount
    ) {
      this.volumeLayer.data = data;
      this.volumeLayer.count = totalCount;
      this.volumeLayer.make();
      this.canvas.paint();
    }
  }

  componentDidMount() {
    const { data = [], totalNum = 1440, name, style = {} } = this.props;
    this.canvas = new Canvas({
      ele: this.chartRef.current,
      canAction: false,
    });
    this.axisLayer = new AxisLayer(this.canvas, {
      yAxisType: AxisLayer.AxisType.NUMBER, // y轴为数值型
      xAxisType: AxisLayer.AxisType.LABEL,  // x轴时间为字符型
      xAxisGraduations: style.xAxis || 3,   // 网格5列
      yAxisGraduations: style.yAxis || 3,   // 网格5行
      xAxisLabels: [],
      xAxisPosition: AxisLayer.AxisPosition.INNER,  // X轴坐标不计算
      yAxisPosition: AxisLayer.AxisPosition.INNER,  // Y轴坐标计算
      yAxisRender: (value) => {
        const enob = style.enob || 2;
        return {
          text: XUE.formatterBigNumber(Number(value), enob),
          size: Number(style.yFontSize || 20),
          color: style.axisColor || '#999999',
          font: style.fontFamily || '微软雅黑',
        };
      },
      xAxisRender: (label) => {
        return {
          text: label,
          size: 0,
          color: style.axisColor || '#999999',
          font: style.fontFamily || '微软雅黑',
        };
      },
      color: '#999999',
    });
    this.volumeLayer = new VolumeLayer(this.canvas, {
      width: this.canvas.width,
      height: this.canvas.height,
      count: totalNum,
      position: new Point(0, 0),
      onMaked: ({ xStep, yStep, max }) => {
        this.axisLayer.yAxisMin = 0;
        this.axisLayer.yAxisMax = max;
        this.axisLayer.make();
      },
    }, data);
    this.volumeLayer.make();
    this.canvas.addChild(this.axisLayer, this.volumeLayer);
    this.canvas.paint();
  }

  render() {
    const { className = '' } = this.props;
    return (
      <div className={className} ref={this.chartRef} />
    );
  }
}
