import React from 'react';
import Header from 'components/Header';
import Storage from 'utils/storage';
import indicator from 'utils/indicator';
import './index.scss';

export default class extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            params: {},
            indicatorName: '',
        }
    }

    onEditQuote = () => {
        const { indicatorId } = this.props.match.params;
        const { params } = this.state;
        const indicators = Storage.get(`INDICATOR_QUOTE`, true);
        const indicatorIndex = indicators.findIndex(vo => vo.id === indicatorId);
        if (indicatorIndex >= 0) {
            indicators[indicatorIndex].params = params;
            Storage.set(`INDICATOR_QUOTE`, indicators, true);
        }
        this.props.history.go(-1);
    }

    componentDidMount() {
        const { indicatorId } = this.props.match.params;
        const indicators = Storage.get(`INDICATOR_QUOTE`, true);
        const index = indicators.findIndex((vo) => vo.id === indicatorId);
        if (index >= 0) {
            this.setState({
                params: indicators[index].params,
                indicatorName: indicators[index].name,
            });
        }
    }

    onParamsChange = (e, key) => {
        const { value } = e.target;
        const { params } = this.state;
        params[key] = Number(value);
        this.setState({
            params
        });
    }

    render() {
        const { params, indicatorName } = this.state;
        console.log('name===>', indicator[indicatorName]);
        return (
            <div className="xue-webview-app theme-bg">
                <div className="xue-webview-app-header theme-bg">
                    <Header
                        rightContent={
                            <React.Fragment>
                                <span onClick={this.onEditQuote}>确认</span>
                            </React.Fragment>

                        }
                        backIcon
                        onBack={() => this.props.history.go(-1)}
                    >
                        参数设置
                    </Header>
                </div>
                <div className="xue-webview-app-content app-quotation-wrapper">
                    {
                        Object.keys(params).map((vo) => {
                            return (
                                <div className="setting-params-item">
                                    <div className="setting-params-name">
                                        {vo}
                                    </div>
                                    <div className="setting-params-input">
                                        <input onChange={(e) => this.onParamsChange(e, vo)} value={params[vo]}/>
                                    </div>
                                </div>
                            );
                        })
                    }

                    <div className="setting-params-item">
                        <div className="setting-params-name">基本用法</div>
                        <div className="setting-params-description">
                        {
                            indicator[indicatorName] && indicator[indicatorName].usage
                        }
                        </div>
                    </div>
                    <div className="setting-params-item">
                        <div className="setting-params-name">简介</div>
                        <div className="setting-params-description">
                        {
                            indicator[indicatorName] && indicator[indicatorName].description
                        }
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
