/*
* @Date: 2021/1/1
* @Author: XueChengwu <xuechengwu@erayt.com>
* @Copyright: 2015-2019 Erayt, Inc.
* @Description: If you have some questions, please contact: xuechengwu@erayt.com.
*/
import Layer from '@/ochart/core/Layer';
import Rectangle from '@/ochart/base/Rectangle';
import Text from '@/ochart/base/Text';
import MultiLine from '@/ochart/base/MultiLine';
import Point from '@/ochart/core/Point';

export default class extends Layer {
  constructor(canvas, style) {
    super(canvas, style);
    this.maxInfo = style.maxInfo;
    this.minInfo = style.minInfo;
    this.currentInfo = style.currentInfo;
  }

  make() {
    this.childs.splice(0, this.childs.length);
    const [ maxPoint, maxValue, maxColor ] = [ this.maxInfo.point, this.maxInfo.value, this.maxInfo.color ];
    const maxTxt = new Text(this.canvas, {
      text: maxValue,
      color: '#FFFFFF',
      font: '微软雅黑',
      size: this.fontSize,
    });
    const maxRectangle = new Rectangle(this.canvas, {
      width: maxTxt.width + 20,
      height: this.fontSize + 10,
      color: maxColor,
      type: Rectangle.TYPE.FILL,
    });
    if (maxPoint.x > this.width / 2) {
      let line = new MultiLine(this.canvas, {
        color: maxColor,
      }, [
        new Point(maxPoint.x, maxPoint.y),
        new Point(maxPoint.x, maxPoint.y + 20),
        new Point(maxPoint.x - 20, maxPoint.y + 20),
      ]);
      this.addChild(line);
      maxTxt.setPosition(
        maxPoint.x - 20 - maxRectangle.width + 10,
        maxPoint.y + 20 - maxTxt.height / 2
      );
      maxRectangle.setPosition(
        maxPoint.x - 20 - maxRectangle.width / 2,
        maxPoint.y + 20
      );
    } else {
      let line = new MultiLine(this.canvas, {
        color: maxColor,
      }, [
        new Point(maxPoint.x, maxPoint.y),
        new Point(maxPoint.x, maxPoint.y + 20),
        new Point(maxPoint.x + 20, maxPoint.y + 20),
      ]);
      this.addChild(line);
      maxTxt.setPosition(
        maxPoint.x + 20 + 10,
        maxPoint.y + 20 - maxTxt.height / 2
      );
      maxRectangle.setPosition(
        maxPoint.x + 20 + maxRectangle.width / 2,
        maxPoint.y + 20
      );
    }
    this.addChild(maxRectangle, maxTxt);
    const [ minPoint, minValue, minColor ] = [ this.minInfo.point, this.minInfo.value, this.minInfo.color ];
    const minTxt = new Text(this.canvas, {
      text: minValue,
      color: '#FFFFFF',
      font: '微软雅黑',
      size: this.fontSize,
    });
    const minRectangle = new Rectangle(this.canvas, {
      width: minTxt.width + 20,
      height: this.fontSize + 10,
      color: minColor,
      type: Rectangle.TYPE.FILL,
    });
    if (minPoint.x > this.width / 2) {
      let line = new MultiLine(this.canvas, {
        color: minColor,
      }, [
        new Point(minPoint.x, minPoint.y),
        new Point(minPoint.x, minPoint.y - 20),
        new Point(minPoint.x - 20, minPoint.y - 20),
      ]);
      this.addChild(line);
      minTxt.setPosition(
        minPoint.x - 20 - minRectangle.width + 10,
        minPoint.y - 20 - minTxt.height / 2
      );
      minRectangle.setPosition(
        minPoint.x - 20 - minRectangle.width / 2,
        minPoint.y - 20
      );
    } else {
      let line = new MultiLine(this.canvas, {
        color: minColor,
      }, [
        new Point(minPoint.x, minPoint.y),
        new Point(minPoint.x, minPoint.y - 20),
        new Point(minPoint.x + 20, minPoint.y - 20),
      ]);
      this.addChild(line);
      minTxt.setPosition(
        minPoint.x + 20 + 10,
        minPoint.y - 20 - minTxt.height / 2
      );
      minRectangle.setPosition(
        minPoint.x + 20 + minRectangle.width / 2,
        minPoint.y - 20
      );
    }
    this.addChild(minRectangle, minTxt);
    const [ currentPoint, currentValue, currentColor ] = [ this.currentInfo.point, this.currentInfo.value, this.currentInfo.color ];
    const currentTxt = new Text(this.canvas, {
      text: currentValue,
      font: '微软雅黑',
      color: '#FFFFFF',
      size: this.fontSize,
    });
    const currentLine = new MultiLine(this.canvas, {
      color: currentColor,
      lineDash: [8, 8],
      lineWidth: 1,
    }, [
      new Point(currentPoint.x, currentPoint.y),
      new Point(this.position.x, currentPoint.y),
    ]);
    const rectangle = new Rectangle(this.canvas, {
      width: currentTxt.width + 20,
      height: currentTxt.height + 10,
      color: currentColor,
      type: Rectangle.TYPE.FILL,
    });
    currentTxt.setPosition(
      this.position.x + 10,
      currentPoint.y - currentTxt.height / 2
    );
    rectangle.setPosition(
      this.position.x + rectangle.width / 2,
      currentPoint.y
    );
    this.addChild(rectangle, currentLine, currentTxt);

  }
}
