/*
* @Date: 2021/2/17
* @Author: XueChengwu <xuechengwu@erayt.com>
* @Copyright: 2015-2019 Erayt, Inc.
* @Description: If you have some questions, please contact: xuechengwu@erayt.com.
*/
import React from 'react';
import ajax from 'decorate/ajax';
import Header from 'components/Header';
import './index.scss';
import forexNewsDetail from 'business/forexNewsDetail';

@ajax(forexNewsDetail.url, forexNewsDetail.meta, forexNewsDetail.resolve)
export default class extends React.Component {
  constructor(props) {
    super(props);
  }

  onBack = () => {
    this.props.history.go(-1);
  }

  render() {
    const { title, abstract, content = [] } = this.props;
    return (
      <div className="xue-webview-app theme-bg">
        <div className="xue-webview-app-header theme-bg">
          <Header backIcon onBack={this.onBack}>
            资讯详情
          </Header>
        </div>
        <div className="xue-webview-app-content">
          <div className="quotation-news-wrapper">
            <div className="quotation-news-title">
              {title}
            </div>
            {
              abstract !== '' && (
                <div className="quotation-news-abstract">
                  { abstract }
                </div>
              )
            }

            <div className="quotation-news-content">
              {
                content.map((item) => {
                  return <p>{item}</p>
                })
              }
            </div>
          </div>

        </div>
      </div>
    )
  }
}
