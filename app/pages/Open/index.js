/*
* @Date: 2021/3/3
* @Author: XueChengwu <xuechengwu@erayt.com>
* @Copyright: 2015-2019 Erayt, Inc.
* @Description: If you have some questions, please contact: xuechengwu@erayt.com.
*/
import React from 'react';
import './index.scss';
import { observer, inject } from 'mobx-react';
import Header from "components/Header";
import Icon from "components/Icon";

@inject('quotationStore')
@observer
export default class extends React.Component {
  render() {
    return (
      <div className="xue-webview-app theme-bg">
        <Header
          theme="xue-webview-app-header theme-bg"
          leftContent={<Icon type="close" onClick={() => this.props.history.go(-1)}/>}
        >
          开仓
        </Header>
        <div className="xue-webview-app-content">
          <div className="">
            <select>
              <option type="1">市价开仓</option>
              <option type="2">限价买入</option>
              <option type="3">限价卖出</option>
              <option type="4">买入止损</option>
              <option type="5">卖出止损</option>
            </select>
          </div>
          <div className="">
            <div className>

            </div>
          </div>
        </div>
      </div>
    )
  }
}
