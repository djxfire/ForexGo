/*
* @Date: 2021/2/25
* @Author: XueChengwu <xuechengwu@erayt.com>
* @Copyright: 2015-2019 Erayt, Inc.
* @Description: If you have some questions, please contact: xuechengwu@erayt.com.
*/
import React from 'react';
import Header from 'components/Header';
import Button from 'components/Button';
import Icon from 'components/Icon';
import ajax from 'decorate/ajax';
import Login from 'business/login';
import Toast from 'components/Toast';
import './index.scss';
import { inject, observer } from 'mobx-react';

@inject('userStore')
@observer
@ajax(Login.url, Login.meta, Login.resolve)
export default class extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      username: '',
      password: '',
    };
  }

  onNameChange = (e) => {
    const { value } = e.currentTarget;
    this.setState({
      username: value,
    });
  }

  onPasswordChange = (e) => {
    const { value } = e.currentTarget;
    this.setState({
      password: value,
    });
  }

  goRegister = (e) => {
    this.props.history.push('/register');
  }

  onLogin = () => {
    const { username, password } = this.state;
    if (username === '') {
      Toast.toast('请输入用户名或邮箱');
      return;
    }
    if (password === '') {
      Toast.toast('请输入密码');
      return;
    }
    this.postAjax();
  }

  render() {
    return (
      <div className="xue-webview theme-bg">
        <Header
          theme="xue-webview-app-header theme-bg"
          leftContent={<Icon type="close" onClick={() => this.props.history.go(-1)}/>}
          rightContent={<label onClick={this.goRegister}>注册</label>}
        >
          登录
        </Header>
        <div className="xue-webview-app-content">
          <div className="login-box">
            <h1>欢迎登录</h1>
            <div className="login">
              <div className="login-form-item">
                <label>用户名或邮箱</label>
                <div className="login-form-input">
                  <input type="text" onChange={this.onNameChange} />
                </div>
              </div>
              <div className="login-form-item">
                <label>密码</label>
                <div className="login-form-input">
                  <input type="password" onChange={this.onPasswordChange} />
                </div>
              </div>
            </div>
            <div className="login-box-button">
              <Button onClick={this.onLogin}>登录</Button>
            </div>
            <div className="login-info">
              <label>忘记密码</label>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
