/*
* @Date: 2021/2/28
* @Author: XueChengwu <xuechengwu@erayt.com>
* @Copyright: 2015-2019 Erayt, Inc.
* @Description: If you have some questions, please contact: xuechengwu@erayt.com.
*/
import React from 'react';
import Net from '@/net/Net';
import Icon from 'components/Icon';
import Vote from './Vote';
import './index.scss';

export default class extends React.Component {
  constructor(props) {
    super(props);
  }

  onVote = (e) => {
    const { id, vote } = this.props;
    const { voteStatus } = e.currentTarget.dataset;

  }

  onComments = () => {
    const { id } = this.props;
    this.props.history.push(`/postDetail/${id}`);
  }

  render() {
    const {
      username, medias = '', avatar, topic,
      commentNum, content, upvote, downvote,
      id, vote
    } = this.props;
    let images = [];
    if (medias !== '') {
      images = medias.split(';');
    }
    return (
      <div className="post-item-box">
        <div className="post-item-user">
          <div className="post-item-avatar">
            <img src={`${Net.hostUrl}/local/${avatar}`} />
          </div>
          <div className="post-item-username">
            { username }
          </div>
        </div>
        <div className="post-item-content">
          { topic !== '' && <span>#{topic}#</span> }
          <span>{content}</span>
        </div>
        <div className="post-item-images">
          {
            images.map(vo => (
              <div className="post-images-item">
                <img src={`${Net.hostUrl}/local/${vo}`} />
              </div>

            ))
          }
        </div>
        <div className="post-item-append">
          <div className="post-append-item" onClick={this.onComments}>
            <Icon type="pinglun" />
            { Number(commentNum) === 0 ? '评论' : Number(commentNum) > 999 ? '999+' : commentNum  }
          </div>
          <div className="post-append-item">
            <Vote
              post={id}
              status={Number(vote) === 1 ? 1 : 0}
              vote={1}
              onVoteEnd={this.props.onVoteEnd}
            >
              <Icon type="dianzan" />
              { Number(upvote) === 0 ? '赞' : Number(upvote) > 999 ? '999+' : upvote }
            </Vote>
          </div>
          <div className="post-append-item">
            <Vote
              post={id}
              status={Number(vote) === 2 ? 1 : 0}
              vote={2}
              onVoteEnd={this.props.onVoteEnd}
            >
              <Icon type="dianzan" className="dianzan-reverse" />
              { Number(downvote) === 0 ? '踩' : Number(downvote) > 999 ? '999+' : downvote }
            </Vote>
          </div>
        </div>
      </div>
    );
  }
}
