/*
* @Date: 2021/2/28
* @Author: XueChengwu <xuechengwu@erayt.com>
* @Copyright: 2015-2019 Erayt, Inc.
* @Description: If you have some questions, please contact: xuechengwu@erayt.com.
*/
import React from 'react';
import ajax from 'decorate/ajax';
import postVote from 'business/postVote';
import Toast from 'components/Toast';


@ajax(postVote.url, postVote.meta)
export default class extends React.Component {
  doVote = () => {
    console.log('click===>', post);
    const { post, status, vote, onVoteEnd } = this.props;
    this.postAjax({
      data() {
        return {
          post,
          status,
          vote,
        };
      },
    }).then((res) => {
      if (Number(res.code) === 0) {
        onVoteEnd && onVoteEnd({
          post,
          status,
          vote,
          postModal: res.data.post,
        });
      } else {
        Toast.toast(res.msg);
      }
    });
  }

  render() {
    const { children, status } = this.props;
    return (
      <div onClick={this.doVote} className={`post-append-vote ${Number(status) === 1 ? 'active' : ''}`}>
        { children }
      </div>
    );
  }
}
