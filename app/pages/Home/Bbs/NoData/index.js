/*
* @Date: 2021/2/28
* @Author: XueChengwu <xuechengwu@erayt.com>
* @Copyright: 2015-2019 Erayt, Inc.
* @Description: If you have some questions, please contact: xuechengwu@erayt.com.
*/
import React from 'react';
import Icon from 'components/Icon';
import './index.scss';
export default class extends React.Component {
  render() {
    return (
      <div className="bbs-no-data">
        <Icon type="kong" />
        <label>没有记录</label>
      </div>
    )
  }
}
