/*
* @Date: 2021/2/27
* @Author: XueChengwu <xuechengwu@erayt.com>
* @Copyright: 2015-2019 Erayt, Inc.
* @Description: If you have some questions, please contact: xuechengwu@erayt.com.
*/
import React from 'react';
import './index.scss';
import Icon from 'components/Icon';
import Net from '@/net/Net';
import Header from 'components/Header';
import { inject, observer } from 'mobx-react';
import Toast from 'components/Toast';
import Last from './Last';
import AddBox from './AddBox';

@inject('userStore')
@observer
export default class extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      type: 0,
    }
  }

  onDataType = (e) => {
    const { type } = e.currentTarget.dataset;
    if (Number(type) === 2) {
      // 判断是否登录
      const { user } = this.props.userStore;
      if (!user.username) {
        Toast.toast('请先登录');
        setTimeout(() => {
          this.props.history.push('/login');
        }, 1000);
      }
    }
    this.setState({
      type: Number(type),
    }, () => {
      console.log('data-type====>', this.state.type)
    });
  }

  render() {
    const { show } = this.props;
    const { type } = this.state;
    const { user } = this.props.userStore;
    return (
      <div className="xue-webview-app" style={{display: show ? 'flex' : 'none'}}>
        <div className="xue-webview-app-header news-header bottom-line">
          <Header
            rightContent={
              user.avatar && (
                <div className="bbs-avatar-box">
                  <img src={`${Net.hostUrl}/local/${user.avatar}`} className="bbs-user-avatar"/>
                </div>
              )
            }
            theme="bbs-header"
          >
            <div className="bbs-type-box">
              <div className={`bbs-type-item ${type === 0 ? 'active' : ''}`} data-type="0" onClick={this.onDataType}>最新</div>
              <div className={`bbs-type-item ${type === 1 ? 'active' : ''}`} data-type="1" onClick={this.onDataType}>推荐</div>
              <div className={`bbs-type-item ${type === 2 ? 'active' : ''}`} data-type="2" onClick={this.onDataType}>关注</div>
            </div>
          </Header>
        </div>
        <div className="xue-webview-app-content bbs-content-box">
          <Last {...this.props} show={type === 0} type={0} />
          <Last {...this.props} show={type === 1} type={1} />
          <Last {...this.props} show={type === 2} type={2} />
        </div>
        <AddBox className="bbs-add" {...this.props} />
      </div>
    );
  }
}
