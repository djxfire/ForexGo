/*
* @Date: 2021/2/27
* @Author: XueChengwu <xuechengwu@erayt.com>
* @Copyright: 2015-2019 Erayt, Inc.
* @Description: If you have some questions, please contact: xuechengwu@erayt.com.
*/
import React from 'react';
import Icon from 'components/Icon';
import './index.scss';

export default class extends React.Component {
  onAddPost = () => {
    this.props.history.push('/newPost');
  }

  render() {
    const { className = '' } = this.props;
    return (
      <div className={`${className} bbs-add-box`}>
        <Icon type="edit" onClick={this.onAddPost}/>
      </div>
    )
  }
}
