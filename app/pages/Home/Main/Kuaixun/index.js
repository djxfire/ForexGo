/*
* @Date: 2021/3/6
* @Author: XueChengwu <xuechengwu@erayt.com>
* @Copyright: 2015-2019 Erayt, Inc.
* @Description: If you have some questions, please contact: xuechengwu@erayt.com.
*/
import React from 'react';
import './index.scss';
import ajax from 'decorate/ajax';
import Kuaixun from 'business/Kuaixun';
import Pulldown from 'components/Pulldown';
import Swiper from 'components/Swiper';
import './index.scss';
import Icon from "components/Icon";

@ajax(Kuaixun.url, Kuaixun.meta, Kuaixun.resolve)
export default class extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
    };
  }

  onPulldown  = () => {
    return this.postAjax();
  }

  render() {
    const { data = [] } = this.props;
    const { show = false } = this.props;
    if (data.length === 0 && show) {
      return (
        <Pulldown
          onLoading={this.onPulldown}
        >
          <div className="app-no-data">
            <Icon type="kong"/>
            <div className="no-data">暂无数据</div>
          </div>
        </Pulldown>
      )
    }
    return (
      <Swiper className="market-news-wrapper" style={{ display: show ? 'flex' : 'none' }}>
        {
          data.map((item) => {
            return (
              <div className="fast-news-item" key={item.id}>
                <div className="fast-news-content-wrapper">
                  <div className="fast-news-time">
                    {
                      item.showtime
                    }
                  </div>
                  <div className="fast-news-content">
                    { item.digest}
                  </div>
                </div>
              </div>
            );
          })
        }
      </Swiper>
    )
  }
}
