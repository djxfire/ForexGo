/*
* @Date: 2021/3/6
* @Author: XueChengwu <xuechengwu@erayt.com>
* @Copyright: 2015-2019 Erayt, Inc.
* @Description: If you have some questions, please contact: xuechengwu@erayt.com.
*/
import React from 'react';
import ajax from 'decorate/ajax';
import './index.scss';
import ViewPoint from 'business/ViewPoint';
import Pulldown from 'components/Pulldown';
import Icon from 'components/Icon';
import Swiper from 'components/Swiper';

@ajax(ViewPoint.url, ViewPoint.meta, ViewPoint.resolve)
export default class extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      page: 1,
      last_page: 1,
      onAjaxing: false,
    };
  }

  onPulldown = () => {
    return new Promise((resolve, reject) => {
      this.setState({
        page: 1,
        last_page: 1,
        onAjaxing: false,
      }, () => {
        this.postAjax().then((res) => {
          resolve(res);
        });
      });
    })
  }

  render() {
    const { data = [] } = this.props;
    const { show = false } = this.props;
    if (data.length === 0 && show) {
      return (
        <Pulldown
          onLoading={this.onPulldown}
        >
          <div className="app-no-data">
            <Icon type="kong"/>
            <div className="no-data">暂无数据</div>
          </div>
        </Pulldown>
      )
    }
    return (
      <Swiper className="app-news-wrapper" style={{ display: show ? 'flex' : 'none' }}>
        {
          data.map((item) => {
            return (
              <div className="view-point-item-wrapper">
                <div className="view-point-item">
                  <div className="view-point-header">
                    <div className="view-point-title">{item.title}</div>
                    <div className="view-point-time">{item.time}</div>
                  </div>
                  <div className="view-point-info">
                    {item.info}
                  </div>
                </div>
              </div>
            );
          })
        }
      </Swiper>
    );
  }
}
