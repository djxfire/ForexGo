/*
* @Date: 2021/1/10
* @Author: XueChengwu <xuechengwu@erayt.com>
* @Copyright: 2015-2019 Erayt, Inc.
* @Description: If you have some questions, please contact: xuechengwu@erayt.com.
*/
import React from 'react';
import './index.scss';
import Header from '@/components/Header';
import Icon from '@/components/Icon';
import Radar from 'ochart/react/Radar';
import { observer, inject } from 'mobx-react';
import markvoWorker from '@/workers/markvoTool';
import Tab from 'components/Tab';
import Kuaixun from './Kuaixun';
import ViewPoint from './ViewPoint';
import CentralBank from './CentralBank';
import Data from './Data';

@inject('quotationStore')
@observer
export default class extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      cast: [0, 0, 0, 0, 0, 0, 0, 0, 0],
      avg: [0, 0, 0, 0, 0, 0, 0, 0],
      key: 'fastnews'
    };
  }

  onTabChange = (key) => {
    this.setState({
      key
    });
  }
  componentDidMount() {
    markvoWorker.addEventListener('message', (e) => {
      const { result, avgCenter } = e.data;
      this.setState({
        cast: result,
        avg: avgCenter,
      });
    });
  }

  onHelp = () => {

  }

  render() {
    const { show } = this.props;
    const { quotations } = this.props.quotationStore;
    const { cast, avg, key } = this.state;
    return (
      <div className="xue-webview-app" style={{display: show ? 'flex' : 'none' }}>
        <Header
          className="xue-webview-app-header theme-bg"
          rightContent={<Icon type="wenhao" onClick={this.onHelp}/>}
        >
          全景图
        </Header>
        <div className="xue-webview-app-content">
          <div className="full-market-legend">
            <div className="legend-a">当前</div>
            <div className="legend-b">预测</div>
            <div className="legend-c">预测均值</div>
          </div>
          <Radar
            className="radar-chart"
            data={[{
              color: quotations['USDCAD'] ? quotations['USDCAD'].pcg > 0 ? '#D42922' : '#00AE00' : '#A3A3A3',
              label: `   加元\n(${quotations['USDCAD'] && quotations['USDCAD'].close})`,
              a: quotations['USDCAD'] ? quotations['USDCAD'].pcg : 0,
              b: cast[3],
              c: avg[3],
            }, {
              color: quotations['USDJPY'] ? quotations['USDJPY'].pcg > 0 ? '#D42922' : '#00AE00' : '#A3A3A3',
              label: `   日元\n(${quotations['USDJPY'] && quotations['USDJPY'].close})`,
              a: quotations['USDJPY'] ? quotations['USDJPY'].pcg : 0,
              b: cast[1],
              c: avg[1],
            }, {
              color: quotations['XAU'] ? quotations['XAU'].pcg > 0 ? '#D42922' : '#00AE00' : '#A3A3A3',
              label: `黄金\n(${quotations['XAU'] && quotations['XAU'].close})`,
              a: quotations['XAU'] ? quotations['XAU'].pcg : 0,
              b: cast[0],
              c: avg[0],
            }, {
              color: quotations['NZDUSD'] ? quotations['NZDUSD'].pcg > 0 ? '#D42922' : '#00AE00' : '#A3A3A3',
              label: `纽元   \n(${quotations['NZDUSD'] && quotations['NZDUSD'].close})`,
              a: quotations['NZDUSD'] ? quotations['NZDUSD'].pcg : 0,
              b: cast[7],
              c: avg[7],
            }, {
              color: quotations['AUDUSD'] ? quotations['AUDUSD'].pcg > 0 ? '#D42922' : '#00AE00' : '#A3A3A3',
              label: `澳元   \n(${quotations['AUDUSD'] && quotations['AUDUSD'].close})`,
              a: quotations['AUDUSD'] ? quotations['AUDUSD'].pcg : 0,
              b: cast[6],
              c: avg[6],
            }, {
              color: quotations['EURUSD'] ? quotations['EURUSD'].pcg > 0 ? '#D42922' : '#00AE00' : '#A3A3A3',
              label: `欧元   \n(${quotations['EURUSD'] && quotations['EURUSD'].close})`,
              a: quotations['EURUSD'] ? quotations['EURUSD'].pcg : 0,
              b: cast[4],
              c: avg[4],
            }, {
              color: quotations['GBPUSD'] ? quotations['GBPUSD'].pcg > 0 ? '#D42922' : '#00AE00' : '#A3A3A3',
              label: `英镑\n(${quotations['GBPUSD'] && quotations['GBPUSD'].close})`,
              a: quotations['GBPUSD'] ? quotations['GBPUSD'].pcg : 0,
              b: cast[5],
              c: avg[5],
            }, {
              color: quotations['USDCHF'] ? quotations['USDCHF'].pcg > 0 ? '#D42922' : '#00AE00' : '#A3A3A3',
              label: `   瑞郎\n(${quotations['USDCHF'] && quotations['USDCHF'].close})`,
              a: quotations['USDCHF'] ? quotations['USDCHF'].pcg : 0,
              b: cast[2],
              c: avg[2],
            },]}
            style={{
              color: '#C3C3C3',
              fontColor: '#A3A3A3',
              enob: 2,
              type: Radar.TYPE.FILL,
              colors: {
                a: '#3575D8',
                b: '#E4D733',
                c: '#F9D4C9'
              },
              lineWidth: 1,
            }}
          />
          <Tab
            data={[{
              key: 'fastnews',
              label: '快讯',
            }, {
              key: 'viewpoint',
              label: '观点'
            }, {
              key: 'centralbank',
              label: '央行'
            }, {
              key: 'data',
              label: '数据'
            }]}
            type="info"
            defaultValue="viewpoint"
            onTabSwitch={this.onTabChange}
          />
          <Kuaixun {...this.props} show={key === 'fastnews'} />
          <ViewPoint {...this.props} show={key === 'viewpoint'} />
          <CentralBank {...this.props} show={key === 'centralbank'} />
          <Data {...this.props} show={key === 'data'} />
        </div>
      </div>
    );
  }
}
