/*
* @Date: 2021/3/1
* @Author: XueChengwu <xuechengwu@erayt.com>
* @Copyright: 2015-2019 Erayt, Inc.
* @Description: If you have some questions, please contact: xuechengwu@erayt.com.
*/
import React from 'react';
import Net from '@/net/Net';
import './index.scss';
import { observer, inject } from 'mobx-react';

@inject('userStore')
@observer
export default class extends React.Component {

  render() {
    const { user = {}, stars = [] } = this.props.userStore;
    return (
      <div className="user-info-box">
        <div className="user-info-avatar">
          <img src={`${Net.hostUrl}/local/${user.avatar}`} />
        </div>
        <div className="user-info-detail">
          <p className="user-info-username">{user.username}</p>
          <p className="user-info-email">
            邮箱:{user.email}
          </p>
        </div>
      </div>
    );
  }
}
