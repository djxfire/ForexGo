/*
* @Date: 2021/3/2
* @Author: XueChengwu <xuechengwu@erayt.com>
* @Copyright: 2015-2019 Erayt, Inc.
* @Description: If you have some questions, please contact: xuechengwu@erayt.com.
*/
import React from 'react';
import Button from 'components/Button';
import Icon from 'components/Icon';
import './index.scss';

export default class extends React.Component {
  render() {
    return (
      <React.Fragment>
        <div className="me-operation-box">
          <div className="me-operation-item">
            动态
            <div className="seize"/>
            <Icon type="back" className="me-operation-reverse"/>
          </div>
          <div className="me-operation-item">
            账户
            <div className="seize"/>
            <Icon type="back" className="me-operation-reverse"/>
          </div>
        </div>
        <div className="me-operation-box">
          <div className="me-operation-item">
            我的持仓
            <div className="seize"/>
            <Icon type="back" className="me-operation-reverse"/>
          </div>
          <div className="me-operation-item">
            我的订单
            <div className="seize"/>
            <Icon type="back" className="me-operation-reverse"/>
          </div>
          <div className="me-operation-item">
            交易记录
            <div className="seize"/>
            <Icon type="back" className="me-operation-reverse"/>
          </div>
        </div>
        <div className="me-operation-button">
          <Button>
            退出登录
          </Button>
        </div>
      </React.Fragment>

    );
  }
}
