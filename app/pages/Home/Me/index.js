/*
* @Date: 2021/3/1
* @Author: XueChengwu <xuechengwu@erayt.com>
* @Copyright: 2015-2019 Erayt, Inc.
* @Description: If you have some questions, please contact: xuechengwu@erayt.com.
*/
import React from 'react';
import './index.scss';
import Info from './Info';
import Header from 'components/Header';
import Bos from './Bos';
import Operation from './Operation';

export default class extends React.Component {
  render() {
    const { show } = this.props;
    return (
      <div className="xue-webview-app" style={{display: show ? 'flex' : 'none' }}>
        <Header
          theme="xue-webview-app-header theme-bg"
        >
          我
        </Header>
        <Info />
        <Bos />
        <Operation />
      </div>
    )
  }
}
