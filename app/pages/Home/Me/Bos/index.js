/*
* @Date: 2021/3/2
* @Author: XueChengwu <xuechengwu@erayt.com>
* @Copyright: 2015-2019 Erayt, Inc.
* @Description: If you have some questions, please contact: xuechengwu@erayt.com.
*/
import React from 'react';
import './index.scss';
import { inject, observer } from 'mobx-react';

@inject('userStore')
@observer
export default class extends React.Component {
  render() {
    const { user = {}, stars = [] } = this.props.userStore;
    return (
      <div className="me-bos-box">
        <div className="me-bos-item">
          <p>{stars.length}</p>
          <p>关注</p>
        </div>
        <div className="me-bos-item">
          <p>{user.stars}</p>
          <p>被关注</p>
        </div>
        <div className="me-bos-item">
          <p>11</p>
          <p>动态</p>
        </div>
        <div className="me-bos-item">
          <p>0</p>
          <p>交易</p>
        </div>
      </div>
    )
  }
}
