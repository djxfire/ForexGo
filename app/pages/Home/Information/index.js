/*
* @Date: 2021/2/23
* @Author: XueChengwu <xuechengwu@erayt.com>
* @Copyright: 2015-2019 Erayt, Inc.
* @Description: If you have some questions, please contact: xuechengwu@erayt.com.
*/
import React from 'react';
import BlockTab from 'components/BlockTab';
import './index.scss';
import News from './News';
import Calendar from './Calendar';
import Event from './Event';

export default class extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      tab: 'news'
    };
  }

  onTabChange = (value) => {
    this.setState({
      tab: value,
    });
  }

  render() {
    const { show } = this.props;
    const { tab } = this.state;
    return (
      <div className="xue-webview-app" style={{display: show ? 'flex' : 'none'}}>
        <div className="xue-webview-app-header news-header bottom-line">
          <BlockTab
            className="news-header-tab"
            defaultValue="news"
            data={[{
              key: 'news',
              name: '7*24'
            }, {
              key: 'calendar',
              name: '日历'
            }, {
              key: 'event',
              name: '事件'
            }]}
            onChange={this.onTabChange}
          />
        </div>
        <div className="xue-webview-app-content">
          { tab === 'news' && <News /> }
          { tab === 'calendar' && <Calendar /> }
          { tab === 'event' && <Event />}
        </div>
      </div>
    );
  }
}

