import React from 'react';
import Header from 'components/Header';
import Icon from 'components/Icon';
import MineItem from './MineItem';
import './index.scss';
import { inject, observer } from 'mobx-react';
import Storage from '@/utils/storage';

@inject('favorateStore')
@observer
export default class extends React.Component {
    constructor(props) {
        super(props);
    }

    onSearch = () => {
        this.props.history.push('/search');
    }

    onItemClick = (secid) => {
        this.props.history.push(`/quotation/${secid}`);
    }

    render() {
        const {show} = this.props;
        const { favorates = [] } = this.props.favorateStore;
        const products = Storage.get('fx_products', true, []);
        return (
            <div className="xue-webview-app" style={{display: show ? 'flex' : 'none'}}>
                <div className="xue-webview-app-header">
                    <Header
                        rightContent={
                            <React.Fragment>
                                <Icon type="search" className="favorate-search" onClick={this.onSearch}/>
                            </React.Fragment>

                        }
                    >
                        自选
                    </Header>
                </div>
                {
                    favorates.map((item) => {
                        const productIndex = products.findIndex(vo => vo.symbol === item);
                        console.log('item==>', item, productIndex);
                        if (productIndex === -1) {
                            return null;
                        }
                        return (
                          <MineItem
                            productId={item}
                            secid={products[productIndex].secid}
                            name={products[productIndex].name}
                            decimal={products[productIndex].decimal}
                            onClick={this.onItemClick}
                            show={show}
                          />
                          );
                    })
                }
            </div>
        )
    }
}
