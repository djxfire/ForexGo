import React from 'react';
import Minite from '@/ochart/react/Minite';
import Trend from '@/business/trend';
import ajax from 'decorate/ajax';

@ajax(Trend.url, Trend.meta, Trend.resolve)
export default class extends React.Component {
    render() {
        const { data = [], preClose, trendsTotal = 0, className = '', style = {}, id } = this.props;
        return (
          <Minite
            data={data}
            baseLine={preClose}
            totalCount={trendsTotal}
            className={className}
            style={style}
            id={id}
          />
          );
    }
}
