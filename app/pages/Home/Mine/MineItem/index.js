import React from 'react';
import { inject, observer } from 'mobx-react';
import './index.scss';
import Chart from './Chart';

@inject('quotationStore')
@observer
export default class extends React.Component {
    constructor(props) {
        super(props);
    }

    onItemClick = (e) => {
        const { secid, onClick } = this.props;
        onClick && onClick(secid);
    }

    render() {
        const { productId, secid, name, decimal = 4, show = false } = this.props;
        const { quotations } = this.props.quotationStore;
        return (
            <React.Fragment>
                {
                    quotations[productId] && (
                        <div className="mine-item-wrapper" onClick={this.onItemClick}>
                            { show && <Chart id={productId} productId={secid} className="mine-item-chart" style={{color: Number(quotations[productId].amt) > 0 ? '#F42922' : '#00EE00'}}/> }
                            <div className="mine-item-box">
                                <div className="mine-item-f1">
                                    <div>
                                        <span className="mine-item-name">{name}</span>
                                        <span className="mine-item-code">&nbsp;{quotations[productId].code}</span>
                                    </div>
                                    <div className={`mine-price-box ${Number(quotations[productId].amt) > 0 ? 'price-up' : 'price-down'}`}>
                                        <div className="mine-price-buy">{quotations[productId].amt}</div>
                                        <div className="mine-price-sell">{quotations[productId].pcg}%</div>
                                    </div>
                                </div>
                                <div className="mine-item-f2">

                                </div>
                                <div className="mine-item-f3">
                                    <div className="price-info-box">
                                        <div>开&nbsp;{Number(quotations[productId].open).toFixed(decimal)}</div>
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        <div className={`price-current ${Number(quotations[productId].amt) > 0 ? 'price-up' : 'price-down'}`}>
                                            {Number(quotations[productId].close).toFixed(decimal)}
                                        </div>
                                    </div>
                                    <div className="price-info-box">
                                        <div>高&nbsp;{Number(quotations[productId].high).toFixed(decimal)}</div>
                                        <div>&nbsp;&nbsp;&nbsp;&nbsp;低&nbsp;{Number(quotations[productId].low).toFixed(decimal)}</div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    )
                }
            </React.Fragment>
        );
    }
}
