import React from 'react';
import Icon from 'components/Icon';
import './index.scss';
import Main from './Main';
import Mine from './Mine';
import Information from './Information';
import Bbs from './Bbs';
import Me from './Me';

import alive from 'decorate/alive';


@alive('home')
export default class extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            tab: 'home',
        };
    }

    onChangePage = (e) => {
        const { page } = e.currentTarget.dataset;
        this.setState({
            tab: page,
        });
    }

    render() {
        const { tab } = this.state;
        return (
            <div className="xue-webview">
                <div className="xue-webview-content theme-bg">
                    <Main show={tab === 'home'} {...this.props} />
                    <Mine show={tab === 'star'} {...this.props} />
                    <Information show={tab === 'news'} {...this.props} />
                    <Bbs show={tab === 'pengyouquan'} {...this.props} />
                    <Me show={tab === 'user'} {...this.props} />
                </div>
                <div className="xue-webview-bottom-tab">
                    <div className={`${tab === 'home' && 'active'} tab-item`} data-page="home" onClick={this.onChangePage}>
                        <p>{ tab === 'home' ? <Icon type="home"/> : <Icon type="home1" /> }</p>
                        <p>首页</p>
                    </div>
                    <div className={`${tab === 'news' && 'active'} tab-item`} data-page="news" onClick={this.onChangePage}>
                        <p>{ tab === 'news' ? <Icon type="news2"/> : <Icon type="news" /> }</p>
                        <p>资讯</p>
                    </div>
                    <div className={`${tab === 'pengyouquan' && 'active'} tab-item`} data-page="pengyouquan" onClick={this.onChangePage}>
                        <p>{ tab === 'pengyouquan' ? <Icon type="pengyouquan1"/> : <Icon type="pengyouquan" /> }</p>
                        <p>社区</p>
                    </div>
                    <div className={`${tab === 'star' && 'active'} tab-item`} data-page="star" onClick={this.onChangePage}>
                        <p>{ tab === 'star' ? <Icon type="star"/> : <Icon type="star1" /> }</p>
                        <p>自选</p>
                    </div>
                    <div className={`${tab === 'user' && 'active'} tab-item`} data-page="user" onClick={this.onChangePage}>
                        <p>{ tab === 'user' ? <Icon type="user2"/> : <Icon type="user" /> }</p>
                        <p>我</p>
                    </div>
                </div>
            </div>
        )
    }
}
