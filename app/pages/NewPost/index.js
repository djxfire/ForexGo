/*
* @Date: 2021/2/27
* @Author: XueChengwu <xuechengwu@erayt.com>
* @Copyright: 2015-2019 Erayt, Inc.
* @Description: If you have some questions, please contact: xuechengwu@erayt.com.
*/
import React from 'react';
import Header from 'components/Header';
import Icon from 'components/Icon';
import Uploader from 'components/Uploader';
import './index.scss';
import postAdd from 'business/postAdd';
import ajax from 'decorate/ajax';
import Toast from 'components/Toast';

@ajax(postAdd.url, postAdd.meta)
export default class extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      content: '',
      images: [],
      files: [],
    };
  }

  enterContent = (e) => {
    e.currentTarget.height = 'auto';
    e.currentTarget.scrollTop = 0;
    e.currentTarget.style.height = e.currentTarget.scrollHeight + 'px';
    this.setState({
      content: e.currentTarget.value
    });
  }

  onFileRead = (image) => {
    const { images = [] } = this.state;
    images.push(image);
    this.setState({
      images,
    });
  }

  onFileChange = (file) => {
    const { files = [] } = this.state;
    files.push(file);
    this.setState({
      files
    });
  }

  onPub = () => {
    const { content } = this.props;
    if (content === '') {
      Toast.toast('请输入内容');
      return;
    }
    this.postAjax();
  }

  render() {
    const { images = [] } = this.state;
    return (
      <div className="xue-webview-app theme-bg">
        <Header
          theme="xue-webview-app-header theme-bg"
          leftContent={<Icon type="close" onClick={() => this.props.history.go(-1)}/>}
          rightContent={<label onClick={this.onPub}>发布</label>}
        >
        </Header>
        <div className="xue-webview-app-content">
          <textarea className="post-textarea" placeholder="请输入..." onInput={this.enterContent}></textarea>
          <div className="post-images">
            {
              images.map((item, key) => {
                return (
                  <div className="images-box-item" key={key}>
                    <img src={item} />
                  </div>
                );
              })
            }
            <Uploader
              onChange={this.onFileChange}
              isAvatar={false}
              onReader={this.onFileRead}
            />
          </div>
        </div>
      </div>
    )
  }
}
