/*
* @Date: 2020/11/10
* @Author: XueChengwu <xuechengwu@erayt.com>
* @Copyright: 2015-2019 Erayt, Inc.
* @Description: If you have some questions, please contact: xuechengwu@erayt.com.
*/
import React from 'react';
import { Icon } from 'components';
import { inject, observer } from 'mobx-react';
import './index.scss';

@inject('favorateStore')
@observer
export default class extends React.Component {
  onFollow = (code, operation) => {
    if (operation === 0) {
      this.props.favorateStore.remove(code);
    } else if (operation === 1) {
      this.props.favorateStore.add(code);
    }
  }

  render() {
    const { props } = this;
    const { favorates = [] } = this.props.favorateStore;
    return (
      <div className="search-item-box">
        <div className="search-item-info">
          <p className="search-item-name">{props.name}</p>
          <p className="search-item-code">{props.symbol}</p>
        </div>
        <div className="search-item-operation">
          {
              favorates.indexOf(props.symbol) >= 0
            ? <Icon type="star" className="search-star" onClick={() => this.onFollow(props.symbol, 0)} />
            : <Icon type="star1" className="search-star" onClick={() => this.onFollow(props.symbol, 1)}/>
          }
        </div>
      </div>
    )
  }
}
