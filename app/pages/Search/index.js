import React from 'react';
import './index.scss';
import Search from 'components/Search';
import SearchItem from './SearchItem';
import Storage from '@/utils/storage';

export default class extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            data: Storage.get('fx_products', true, []),
        };
    }

    onSearch = (keyword) => {
        const all = Storage.get('fx_products', true, []);
        const data = all.filter(vo => vo.symbol.indexOf(keyword.trim()) !== -1 || vo.name.indexOf(keyword.trim()) !== -1);
        this.setState({
          data: data || [],
        });
    }

    onBack = () => {
        this.props.history.go(-1);
    }

    render() {
        const { data = [] } = this.state;
        return (
           <div className="xue-webview-app theme-bg">
               <div className="xue-webview-app-header search-box-header header-bar">
                   <div className="search-box-wrapper">
                       <Search placeholder="请输入品种代码或名称" onSearch={this.onSearch} />
                   </div>
                   <span onClick={this.onBack}>取消</span>
               </div>
               <div className="xue-webview-app-content">
                   {
                       data.map((item) => {
                           return (
                            <SearchItem {...item} />
                           );
                       })
                   }
               </div>
           </div>
        )
    }
}
