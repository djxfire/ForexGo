/*
* @Date: 2021/2/15
* @Author: XueChengwu <xuechengwu@erayt.com>
* @Copyright: 2015-2019 Erayt, Inc.
* @Description: If you have some questions, please contact: xuechengwu@erayt.com.
*/
import React from 'react';
import Chart from './Chart';
import Header from "components/Header";
import Storage from '@/utils/storage';
import Base from './Base';
import Tab from 'components/Tab';
import News from './News';
import Organ from './Organ';
import Data from './Data';
import Analyse from './Analyse';
import Tool from './Tool';
import indicatorMap from 'utils/indicator';
import './index.scss';
import OChart from "ochart/core/OChart";
import alive from 'decorate/alive';

@alive('quotation')
export default class extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      activeTab: 'strategy',
      klineType: '001',
      indicators: [],
    }
  }

  componentDidMount() {
    const indicators = Storage.get('INDICATOR_QUOTE', true, []);
    this.setState({
      indicators,
    });
  }

  onTabChange = (key) => {
    this.setState({
      activeTab: key,
    });
  }

  onKLineTypeChange = (klineType) => {
    this.setState({
      klineType,
    });
  }

  onIndicatorCreated = (indicator, params) => {
    const { indicators = [] } = this.state;
    const { klineType } = this.state;
    const { productId = '' } = (this.props.match || {}).params;
    if (Number(indicatorMap[indicator].type) === 0) {
      const mainIndicators = Storage.get(`${productId}_${klineType}_MAIN_QUOTE`, true, []);
      const mainIndicator = {
        id: XUE.getGuid(),
        params,
        name: indicator,
      };
      mainIndicators.push(mainIndicator);
      Storage.set(`${productId}_${klineType}_MAIN_QUOTE`, mainIndicators);
      OChart.addQuote(`chart-${productId}-${klineType}`, mainIndicator);
    } else {
      indicators.push({
        id: XUE.getGuid(),
        name: indicator,
        params: params,
      });
      this.setState({
        indicators,
      }, () => {
        Storage.set('INDICATOR_QUOTE', this.state.indicators);
      });
    }
  }

  onQuoteDelete = (id) => {
    const { indicators } = this.state;
    const indicatorIndex = indicators.findIndex(vo => vo.id === id);
    if (indicatorIndex >= 0) {
      indicators.splice(indicatorIndex, 1);
      this.setState({
        indicators,
      }, () => {
        Storage.set('INDICATOR_QUOTE', this.state.indicators);
      });
    }
  }

  onQuoteSetting = (id) => {
    console.log('quote setting ===>', id);
    const { indicators } = this.state;
    const indicatorIndex = indicators.findIndex(vo => vo.id === id);
    if (indicatorIndex >= 0) {
      this.props.history.push(`/quoteSetting/${id}`);
    }
  }

  render() {
    const { productId = '' } = (this.props.match || {}).params;
    const { activeTab, klineType, indicators } = this.state;
    const products = Storage.get('fx_products', true, []);
    const productIndex = products.findIndex(vo => vo.secid === productId);
    return (
      <React.Fragment>
        <div className="xue-webview-app theme-bg">
          <Header
            theme="xue-webview-app-header theme-bg"
          >
            <div>
              {
                productIndex !== -1 ? products[productIndex].name : '--'
              }
            </div>
            <div className="product-symbol">
              {
                productIndex !== -1 ? products[productIndex].symbol : '--'
              }
            </div>
          </Header>
          <div className="xue-webview-app-content">
            <Base {...products[productIndex]} productId={productIndex !== -1 ? products[productIndex].symbol : '--'} />
            <Chart
              productId={productId}
              onKLineTypeChange={this.onKLineTypeChange}
              enob={productIndex !== -1 ? products[productIndex].decimal : 2}
              indicators={indicators}
              onQuoteDelete={this.onQuoteDelete}
              onQuoteSetting={this.onQuoteSetting}
            />
            <Tab
              type="info"
              data={[{
                key: 'strategy',
                label: '策略',
              }, {
                key: 'news',
                label: '资讯',
              }, {
                key: 'analyse',
                label: '分析',
              }, {
                key: 'institution',
                label: '机构',
              }, {
                key: 'data',
                label: '数据'
              }]}
              onTabSwitch={this.onTabChange}
            />
            {
              activeTab === 'news' && (
                <News
                  productId={productIndex === - 1 ? '' : products[productIndex].symbol}
                  { ...this.props }
                />
              )
            }
            {
              activeTab === 'institution' && (
                <Organ
                  productId={productIndex === - 1 ? '' : products[productIndex].symbol}
                  { ...this.props }
                />
              )
            }
            {
              activeTab === 'data' && (
                <Data
                  productId={productIndex === - 1 ? '' : products[productIndex].symbol}
                  { ...this.props }
                />
              )
            }
            {
              activeTab === 'analyse' && (
                <Analyse
                  productId={productIndex === - 1 ? '' : products[productIndex].secid}
                  symbol={productIndex === - 1 ? '' : products[productIndex].symbol}
                  klineType={'101'}
                  { ...this.props }
                />
              )
            }
          </div>
          <Tool
            productId={productIndex === - 1 ? '' : products[productIndex].symbol}
            secid={productId}
            klineType={klineType}
            onIndicatorCreated={this.onIndicatorCreated}
          />
        </div>

      </React.Fragment>
    )
  }
}
