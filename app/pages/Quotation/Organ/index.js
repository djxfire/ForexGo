/*
* @Date: 2021/2/22
* @Author: XueChengwu <xuechengwu@erayt.com>
* @Copyright: 2015-2019 Erayt, Inc.
* @Description: If you have some questions, please contact: xuechengwu@erayt.com.
*/
import React from 'react';
import CmeChart from './CmeChart';
import CftcChart from './CftcChart';
import Cftc2Chart from './Cftc2Chart';
import './index.scss';

export default class extends React.Component {
  constructor(props){
    super(props);
  }

  render() {
    const { productId } = this.props;
    const firstCurrency = productId.substr(0, 3);
    const secondCurrency = productId.substr(3, 3);
    return (
      <div>
        <CmeChart firstCurrency={firstCurrency} secondCurrency={secondCurrency} />
        <Cftc2Chart firstCurrency={firstCurrency} secondCurrency={secondCurrency}/>
        <CftcChart firstCurrency={firstCurrency} secondCurrency={secondCurrency} />
      </div>
    )
  }
}
