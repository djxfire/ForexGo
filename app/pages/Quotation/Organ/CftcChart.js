/*
* @Date: 2021/2/23
* @Author: XueChengwu <xuechengwu@erayt.com>
* @Copyright: 2015-2019 Erayt, Inc.
* @Description: If you have some questions, please contact: xuechengwu@erayt.com.
*/
import React from 'react';
import ajax from 'decorate/ajax';
import Cftc4 from 'business/Cftc4';
import List from 'components/List';
import Popup from 'components/Popup';
import Icon from 'components/Icon';
import Histogram from 'ochart/react/Histogram';

const currency = {
  'USD': '美元',
  'CNY': '人民币',
  'EUR': '欧元',
  'GBP': '英镑',
  'JPY': '日元',
  'AUD': '澳元',
  'NZD': '纽元',
  'CHF': '瑞郎',
  'CAD': '加元'
};
// 0: 多头 1： 空头对比， 2： 空多差值比， 3: 第一货币多空， 4: 第二货币多空,
function typeToLegend(type, color1, color2, first, second) {
  if (type === 0) {
    return (
      <React.Fragment>
        <div style={{background: color1}} />
        {currency[first]} 多头
        <div stype={{background: color2}}/>
        {currency[second]} 多头
      </React.Fragment>
    )
  } else if (type === 1) {
    return (
      <React.Fragment>
        <div style={{background: color1}} />
        {currency[first]} 空头
        <div style={{background: color2}}/>
        {currency[second]} 空头
      </React.Fragment>
    )
  } else if (type === 2) {
    return (
      <React.Fragment>
        <div style={{background: color1}}/>
        {currency[first]}多空差
        <div style={{background: color2}}/>
        {currency[second]}多空差
      </React.Fragment>
    )
  } else if (type === 3) {
    return (
      <React.Fragment>
        {currency[first]}
        <div style={{background: color1}}/>
        多头
        <div style={{background: color2}}/>
        空头
      </React.Fragment>
    )
  } else if (type === 4) {
    return (
      <React.Fragment>
        {currency[second]}
        <div style={{background: color1}} />
        多头
        <div style={{background: color2}} />
        空头
      </React.Fragment>
    );
  }
  return null;
}
const typeList = [{
  key: 0,
  label: '多头比'
} ,{
  key: 1,
  label: '空头比'
}, {
  key: 2,
  label: '多空差值比'
}, {
  key: 3,
  label: '第一货币多空比'
}, {
  key: 4,
  label: '第二货币多空比'
}];

@ajax(Cftc4.url, Cftc4.meta, Cftc4.resolve)
export default class extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      type: 0, // 0: 多头 1： 空头对比， 2： 空多差值比， 3: 第一货币多空， 4: 第二货币多空,
      color1: '#00B2EE',
      color2: '#1874CD',
      showPopup: false,
      label: '多头比'
    };
  }

  onDataClick = (data) => {
    console.log(data);
  }

  onRelease = () => {

  }

  onKeyChange = (e) => {
    const { key, label } = e.currentTarget.dataset;
    this.setState({
      type: Number(key),
      label,
      showPopup: false,
    });
  }

  onShowPop = (e) => {
    this.setState({
      showPopup: true,
    });
  }

  shouldComponentUpdate(nextProps, nextState, nextContext) {
    if (
      this.props.data !== nextProps.data
      || this.props.firstCurrency !== nextProps.firstCurrency
      || this.props.secondCurrency !== nextProps.secondCurrency
      || this.state.label !== nextState.label
      || this.state.type !== nextState.type
      || this.state.color1 !== nextState.color1
      || this.state.color2 !== nextState.color2
      || this.state.showPopup !== nextState.showPopup
    ) {
      return true;
    }
    return false;
  }

  render() {
    const { data = [], firstCurrency, secondCurrency } = this.props;
    const { type = 0, color1, color2, label, showPopup } = this.state;
    let organ = data.map((item) => {
      if (type === 0) {
        return {
          label: XUE.formatDate(new Date(item.date), 'MM-dd'),
          data: [{
            color: '#00B2EE',
            value: item[firstCurrency] ? Number(item[firstCurrency].long) : 0
          }, {
            color: '#1874CD',
            value: item[secondCurrency] ? Number(item[secondCurrency].long) : 0
          }]
        };
      } else if (type === 1){
        return {
          label: XUE.formatDate(new Date(item.date), 'MM-dd'),
          data: [{
            color: '#00B2EE',
            value: item[firstCurrency] ? Number(item[firstCurrency].short) : 0
          }, {
            color: '#1874CD',
            value: item[secondCurrency] ? Number(item[secondCurrency].short) : 0
          }]
        };
      } else if (type === 2) {
        return {
          label: XUE.formatDate(new Date(item.date), 'MM-dd'),
          data: [{
            color: '#00B2EE',
            value: item[firstCurrency] ? Number(item[firstCurrency].position) : 0
          }, {
            color: '#1874CD',
            value: item[secondCurrency] ? Number(item[secondCurrency].position) : 0
          }]
        };
      } else if (type === 3) {
        return {
          label: XUE.formatDate(new Date(item.date), 'MM-dd'),
          data: [{
            color: '#00B2EE',
            value: item[firstCurrency] ? Number(item[firstCurrency].long) : 0
          }, {
            color: '#1874CD',
            value: item[firstCurrency] ? Number(item[firstCurrency].short) : 0
          }]
        };
      } else {
        return {
          label: XUE.formatDate(new Date(item.date), 'MM-dd'),
          data: [{
            color: '#00B2EE',
            value: item[secondCurrency] ? Number(item[secondCurrency].long) : 0
          }, {
            color: '#1874CD',
            value: item[secondCurrency] ? Number(item[secondCurrency].short) : 0
          }]
        };
      }
    });
    return (
      <React.Fragment>
        <div className="organ-chart-wrapper">
          <div className="organ-chart-tab">
            <div className="organ-chart-title">
              <span>非商业外汇（{currency[firstCurrency]}/{currency[secondCurrency]}）</span>
            </div>
            <div className="chart-legend-box">
              <div className="chart-legend-pop"  onClick={this.onShowPop}>
                {
                  label
                }
                <Icon type="xiala"/>
              </div>
              <div className="seize" />
              <div className="organ-chart-legend">
                {
                  typeToLegend(type, color1, color2, firstCurrency, secondCurrency)
                }
              </div>
            </div>
            <Histogram
              className="organ-cftc-chart"
              data={organ}
              style={{
                showCount: 12,
                canMove: true,
                color: '#A3A3A3',
                fontColor: '#A3A3A3',
              }}
              onClick={this.onDataClick}
              onRelease={this.onRelease}
            />
          </div>
        </div>
        {
          showPopup && (
            <Popup
              show={showPopup}
              className="quotation-popup-panel"
              header={<div className="organ-popup-header">货币持仓类型</div>}
            >
              <List>
                {
                  typeList.map((item) => {
                    return (
                      <List.Item>
                        <div
                          className={`organ-popup-list-item ${item.key === type && 'active'}`}
                          data-key={item.key}
                          data-label={item.label}
                          onClick={this.onKeyChange}

                        >
                          {item.label}
                          <div className="seize" />
                          { item.key === type && <Icon type="click1"/> }
                        </div>
                      </List.Item>
                    )
                  })
                }
              </List>
            </Popup>
          )
        }
      </React.Fragment>
    );
  }
}
