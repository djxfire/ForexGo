/*
* @Date: 2021/2/22
* @Author: XueChengwu <xuechengwu@erayt.com>
* @Copyright: 2015-2019 Erayt, Inc.
* @Description: If you have some questions, please contact: xuechengwu@erayt.com.
*/
import React from 'react';
import Cme1 from 'business/Cme1';
import ajax from 'decorate/ajax';
import List from 'components/List';
import Popup from 'components/Popup';
import Icon from 'components/Icon';
import Histogram from 'ochart/react/Histogram';

const currency = {
  'USD': '美元',
  'CNY': '人民币',
  'EUR': '欧元',
  'GBP': '英镑',
  'JPY': '日元',
  'AUD': '澳元',
  'NZD': '纽元',
  'CHF': '瑞郎',
  'CAD': '加元'
};

const cmeTypeList = [{
  key: 0,
  label: '电子合约'
}, {
  key: 1,
  label: '场内合约'
}, {
  key: 2,
  label: '场外合约'
}, {
  key: 3,
  label: '成交量'
}, {
  key: 4,
  label: '未平仓合约'
}, {
  key: 5,
  label: '持仓变化'
}];

@ajax(Cme1.url, Cme1.meta, Cme1.resolve)
export default class extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      type: 0,
      color1: '#00B2EE',
      color2: '#1874CD',
      showPopup: false,
      label: '电子合约'
    }
  }

  onKeyChange = (e) => {
    const { key, label } = e.currentTarget.dataset;
    this.setState({
      type: Number(key),
      label,
      showPopup: false
    });
  }

  onShowPop = () => {
    this.setState({
      showPopup: true
    });
  }

  onDataClick = () => {
  }

  onRelease = () => {
  }

  shouldComponentUpdate(nextProps, nextState, nextContext) {
    if (
      this.props.data !== nextProps.data
      || this.props.firstCurrency !== nextProps.firstCurrency
      || this.props.secondCurrency !== nextProps.secondCurrency
      || this.state.label !== nextState.label
      || this.state.type !== nextState.type
      || this.state.color1 !== nextState.color1
      || this.state.color2 !== nextState.color2
      || this.state.showPopup !== nextState.showPopup
    ) {
      console.log(111111);
      return true;
    }
    return false;
  }

  render() {
    const { data = [], firstCurrency, secondCurrency } = this.props;
    const { label, type, color1, color2, showPopup } = this.state;
    let organ = data.map((item) => {
      if (type === 0) {
        return {
          label: XUE.formatDate(new Date(item.date), 'MM-dd'),
          data: [{
            color: '#00B2EE',
            value: item[firstCurrency] ? Number(item[firstCurrency].electric) : 0
          }, {
            color: '#1874CD',
            value: item[secondCurrency] ? Number(item[secondCurrency].electric) : 0
          }]
        };
      } else if (type === 1){
        return {
          label: XUE.formatDate(new Date(item.date), 'MM-dd'),
          data: [{
            color: '#00B2EE',
            value: item[firstCurrency] ? Number(item[firstCurrency].onside) : 0
          }, {
            color: '#1874CD',
            value: item[secondCurrency] ? Number(item[secondCurrency].onside) : 0
          }]
        };
      } else if (type === 2) {
        return {
          label: XUE.formatDate(new Date(item.date), 'MM-dd'),
          data: [{
            color: '#00B2EE',
            value: item[firstCurrency] ? Number(item[firstCurrency].outside) : 0
          }, {
            color: '#1874CD',
            value: item[secondCurrency] ? Number(item[secondCurrency].outside) : 0
          }]
        };
      } else if (type === 3) {
        return {
          label: XUE.formatDate(new Date(item.date), 'MM-dd'),
          data: [{
            color: '#00B2EE',
            value: item[firstCurrency] ? Number(item[firstCurrency].volume) : 0
          }, {
            color: '#1874CD',
            value: item[secondCurrency] ? Number(item[secondCurrency].volume) : 0
          }]
        };
      } else if (type === 4){
        return {
          label: XUE.formatDate(new Date(item.date), 'MM-dd'),
          data: [{
            color: '#00B2EE',
            value: item[firstCurrency] ? Number(item[firstCurrency].nop) : 0
          }, {
            color: '#1874CD',
            value: item[secondCurrency] ? Number(item[secondCurrency].nop) : 0
          }]
        };
      } else {
        return {
          label: XUE.formatDate(new Date(item.date), 'MM-dd'),
          data: [{
            color: '#00B2EE',
            value: item[firstCurrency] ? Number(item[firstCurrency].change) : 0
          }, {
            color: '#1874CD',
            value: item[secondCurrency] ? Number(item[secondCurrency].change) : 0
          }]
        };
      }

    });
    return (
      <React.Fragment>
        <div className="organ-chart-wrapper">
          <div className="organ-chart-tab">
            <div className="organ-chart-title">
              <span>CME期货合约</span>
            </div>
            <div className="chart-legend-box">
              <div className="chart-legend-pop"  onClick={this.onShowPop}>
                {
                  label
                }
                <Icon type="xiala"/>
              </div>
              <div className="seize" />
              <div className="organ-chart-legend">
                <div style={{background: color1}} />
                { currency[firstCurrency] }
                <div style={{background: color2}}/>
                { currency[secondCurrency] }
              </div>
            </div>
            <Histogram
              className="organ-cftc-chart"
              data={organ}
              style={{
                showCount: 12,
                canMove: true,
                color: '#A3A3A3',
                fontColor: '#A3A3A3',
              }}
              onClick={this.onDataClick}
              onRelease={this.onRelease}
            />

          </div>
        </div>
        {
          showPopup && (
            <Popup
              show={showPopup}
              className="quotation-popup-panel"
              header={<div className="organ-popup-header">期货交易</div>}
            >
              <List>
                {
                  cmeTypeList.map((item) => {
                    return (
                      <List.Item>
                        <div
                          className={`organ-popup-list-item ${item.key === type && 'active'}`}
                          data-key={item.key}
                          data-label={item.label}
                          onClick={this.onKeyChange}

                        >
                          {item.label}
                          <div className="seize" />
                          { item.key === type && <Icon type="click1"/> }

                        </div>
                      </List.Item>
                    )
                  })
                }

              </List>
            </Popup>
          )
        }

      </React.Fragment>
    );
  }
}
