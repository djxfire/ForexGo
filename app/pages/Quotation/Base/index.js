/*
* @Date: 2021/2/16
* @Author: XueChengwu <xuechengwu@erayt.com>
* @Copyright: 2015-2019 Erayt, Inc.
* @Description: If you have some questions, please contact: xuechengwu@erayt.com.
*/
import React from 'react';
import './index.scss';
import { inject, observer } from 'mobx-react';

@inject('quotationStore')
@observer
export default class extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const {
      productId,
      decimal = 4,
    } = this.props;
    const { quotations } = this.props.quotationStore;
    return (
      <div className="base-wrapper">
        <div className="base-close">
          <div className="base-close-current">
            { quotations[productId] ? Number(quotations[productId].close).toFixed(decimal) : '--' }
          </div>
          <div>
            {(quotations[productId] ? quotations[productId].amt : 0).toFixed(decimal)}
            &nbsp;&nbsp;&nbsp;&nbsp;
            {(quotations[productId] ? quotations[productId].pcg : '0.00')}%
          </div>
        </div>
        <div className="base-open-close">
          <div>
            开 &nbsp;&nbsp;<span>{(quotations[productId] ? quotations[productId].open : '--')}</span>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            高 &nbsp;&nbsp;<span>{(quotations[productId] ? quotations[productId].high : '--')}</span>
          </div>
          <div>
            收&nbsp;&nbsp;<span>{(quotations[productId] ? quotations[productId].low : '--')}</span>
          </div>
        </div>
      </div>
    );
  }
}
