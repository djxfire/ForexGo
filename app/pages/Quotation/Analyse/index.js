/*
* @Date: 2021/3/6
* @Author: XueChengwu <xuechengwu@erayt.com>
* @Copyright: 2015-2019 Erayt, Inc.
* @Description: If you have some questions, please contact: xuechengwu@erayt.com.
*/
import React from 'react';
import BarSequence from 'business/BarSequence';
import ajax from 'decorate/ajax';
import Icon from 'components/Icon';
import { inject, observer } from 'mobx-react';
import './index.scss';

@inject('quotationStore')
@observer
@ajax(BarSequence.url, BarSequence.meta, BarSequence.resolve)
export default class extends React.Component {
  constructor(props) {
    super(props);
    this.leftRef = React.createRef();
    this.rightRef = React.createRef();
    this.state = {
      data: [],
    }
  }

  componentWillReceiveProps(nextProps, nextContext) {
    if (this.props.data !== nextProps.data) {
      this.setState({
        data: nextProps.data,
      }, () => {
        setTimeout(() => {
          const childWidth = parseFloat(getComputedStyle(this.leftRef.current.childNodes[0].childNodes[0], null).width)
          const width = this.leftRef.current.childNodes[0].childNodes.length * childWidth;
          const viewWidth = parseFloat(getComputedStyle(this.leftRef.current).width);
          this.leftRef.current.scrollLeft = width - viewWidth;
        }, 0);
      });
    }
  }

  /**
   * 左边滚动监听
   * @param e
   */
  onLeftScroll = (e) => {
    const childWidth = parseFloat(getComputedStyle(this.leftRef.current.childNodes[0].childNodes[0], null).width)
    const width = this.leftRef.current.childNodes[0].childNodes.length * childWidth;
    const viewWidth = parseFloat(getComputedStyle(this.leftRef.current).width);
    this.rightRef.current.scrollLeft = width - parseFloat(e.currentTarget.scrollLeft) - viewWidth;
  }
  /**
   * 右边滚动监听
   * @param e
   */
  onRightScroll = (e) => {
    const childWidth = parseFloat(getComputedStyle(this.rightRef.current.childNodes[0].childNodes[0], null).width)
    const width = this.rightRef.current.childNodes[0].childNodes.length * childWidth;
    const viewWidth = parseFloat(getComputedStyle(this.rightRef.current).width);
    this.leftRef.current.scrollLeft = width - parseFloat(e.currentTarget.scrollLeft) - viewWidth;
  }

  render() {
    const { data = [] } = this.state;
    const { quotations } = this.props.quotationStore;
    const { symbol, className = '' } = this.props;
    if (data.length < 60 || !quotations[symbol]) {
      return (
        <div className="app-no-data">
          <Icon type="kong" />
          <div>没有记录</div>
        </div>
      );
    }
    let max1 = Number.MIN_VALUE, min1 = Number.MAX_VALUE, max5 = max1, min5 = min1,
      max10 = max1, min10 = min1, max20 = max1, min20 = min1, max30 = max1, min30 = min1,
      max60 = max1, min60 = min1;
    const len = data.length;
    for (let i = 1; i <= 60; i++) {
      if (max60 < data[len - i].high) {
        max60 = data[len - i].high;
      }
      if (min60 > data[len - i].low) {
        min60 = data[len - i].low;
      }
      if (i <= 30) {
        if (max30 < data[len - i].high) {
          max30 = data[len - i].high;
        }
        if (min30 > data[len - i].low) {
          min30 = data[len - i].low;
        }
      }
      if (i <= 20) {
        if (max20 < data[len - i].high) {
          max20 = data[len - i].high;
        }
        if (min20 > data[len - i].low) {
          min20 = data[len - i].low;
        }
      }
      if (i <= 10) {
        if (max10 < data[len - i].high) {
          max10 = data[len - i].high;
        }
        if (min10 > data[len - i].low) {
          min10 = data[len - i].low;
        }
      }
      if (i <= 5) {
        if (max5 < data[len - i].high) {
          max5 = data[len - i].high;
        }
        if (min5 > data[len - i].low) {
          min5 = data[len - i].low;
        }
      }
      if (i === 1) {
        max1 = data[len - i].high;
        min1 = data[len - i].low;
      }
    }
    return (
      <div className="analyse-box">
        <div className={`analyse-tlayer-wrapper ${className}`}>
          <div className="analyse-tlayer-strong">
            <div className="strong-title">阻力点（R）</div>
            <div className="tlayer-box-strong">
              <div className="tlayer-strong-wrapper" onScroll={this.onLeftScroll} ref={this.leftRef}>
                <div className="tlayer-strong-table-header tlayer-line">
                  <div className="th">S1</div>
                  <div className="th">S2</div>
                  <div className="th">S3</div>
                </div>
                <div className="flex tlayer-line tlayer-line-box">
                  <div className={`th  ${((max1 - min1) * 1.328 + min1) < quotations[symbol].close ? 'bg-light-red' : 'bg-light-green'}`}>
                    {
                      ((max1 - min1) * 1.328 + min1)
                        .toFixed(quotations[symbol] ? quotations[symbol].decimal : 4)
                    }
                  </div>
                  <div className={`th  ${max1 < quotations[symbol].close ? 'bg-light-red' : 'bg-light-green'}`}>
                    {
                      (max1)
                        .toFixed(quotations[symbol] ? quotations[symbol].decimal : 4)
                    }
                  </div>
                  <div className={`th  ${((max1 - min1) * 0.618 + min1) < quotations[symbol].close ? 'bg-light-red' : 'bg-light-green'}`}>
                    {
                      ((max1 - min1) * 0.618 + min1)
                        .toFixed(quotations[symbol] ? quotations[symbol].decimal : 4)
                    }
                  </div>
                </div>
                <div className="flex tlayer-line tlayer-line-box">
                  <div className={`th  ${((max5 - min5) * 1.328 + min5) < quotations[symbol].close ? 'bg-light-red' : 'bg-light-green'}`}>
                    {
                      ((max5 - min5) * 1.328 + min5)
                        .toFixed(quotations[symbol] ? quotations[symbol].decimal : 4)
                    }
                  </div>
                  <div className={`th  ${max5 < quotations[symbol].close ? 'bg-light-red' : 'bg-light-green'}`}>
                    {
                      (max5)
                        .toFixed(quotations[symbol] ? quotations[symbol].decimal : 4)
                    }
                  </div>
                  <div className={`th  ${((max5 - min5) * 0.618 + min5) < quotations[symbol].close ? 'bg-light-red' : 'bg-light-green'}`}>
                    {
                      ((max5 - min5) * 0.618 + min5)
                        .toFixed(quotations[symbol] ? quotations[symbol].decimal : 4)
                    }
                  </div>
                </div>
                <div className="flex tlayer-line tlayer-line-box">
                  <div className={`th  ${((max10 - min10) * 1.328 + min10) < quotations[symbol].close ? 'bg-light-red' : 'bg-light-green'}`}>
                    {
                      ((max10 - min10) * 1.328 + min10)
                        .toFixed(quotations[symbol] ? quotations[symbol].decimal : 4)
                    }
                  </div>
                  <div className={`th  ${max10 < quotations[symbol].close ? 'bg-light-red' : 'bg-light-green'}`}>
                    {
                      (max10)
                        .toFixed(quotations[symbol] ? quotations[symbol].decimal : 4)
                    }
                  </div>
                  <div className={`th  ${((max10 - min10) * 0.618 + min10) < quotations[symbol].close ? 'bg-light-red' : 'bg-light-green'}`}>
                    {
                      ((max10 - min10) * 0.618 + min10)
                        .toFixed(quotations[symbol] ? quotations[symbol].decimal : 4)
                    }
                  </div>
                </div>
                <div className="flex tlayer-line tlayer-line-box">
                  <div className={`th  ${((max20 - min20) * 1.328 + min20) < quotations[symbol].close ? 'bg-light-red' : 'bg-light-green'}`}>
                    {
                      ((max20 - min20) * 1.328 + min20)
                        .toFixed(quotations[symbol] ? quotations[symbol].decimal : 4)
                    }
                  </div>
                  <div className={`th  ${max20 < quotations[symbol].close ? 'bg-light-red' : 'bg-light-green'}`}>
                    {
                      (max20)
                        .toFixed(quotations[symbol] ? quotations[symbol].decimal : 4)
                    }
                  </div>
                  <div className={`th  ${((max20 - min20) * 0.618 + min20) < quotations[symbol].close ? 'bg-light-red' : 'bg-light-green'}`}>
                    {
                      ((max20 - min20) * 0.618 + min20)
                        .toFixed(quotations[symbol] ? quotations[symbol].decimal : 4)
                    }
                  </div>
                </div>
                <div className="flex tlayer-line tlayer-line-box">
                  <div className={`th  ${((max30 - min30) * 1.328 + min30) < quotations[symbol].close ? 'bg-light-red' : 'bg-light-green'}`}>
                    {
                      ((max30 - min30) * 1.328 + min30)
                        .toFixed(quotations[symbol] ? quotations[symbol].decimal : 4)
                    }
                  </div>
                  <div className={`th  ${max30 < quotations[symbol].close ? 'bg-light-red' : 'bg-light-green'}`}>
                    {
                      (max30)
                        .toFixed(quotations[symbol] ? quotations[symbol].decimal : 4)
                    }
                  </div>
                  <div className={`th  ${((max30 - min30) * 0.618 + min30) < quotations[symbol].close ? 'bg-light-red' : 'bg-light-green'}`}>
                    {
                      ((max30 - min30) * 0.618 + min30)
                        .toFixed(quotations[symbol] ? quotations[symbol].decimal : 4)
                    }
                  </div>
                </div>
                <div className="flex tlayer-line tlayer-line-box">
                  <div className={`th  ${((max60 - min60) * 1.328 + min60) < quotations[symbol].close ? 'bg-light-red' : 'bg-light-green'}`}>
                    {
                      ((max60 - min60) * 1.328 + min60)
                        .toFixed(quotations[symbol] ? quotations[symbol].decimal : 4)
                    }
                  </div>
                  <div className={`th  ${max60 < quotations[symbol].close ? 'bg-light-red' : 'bg-light-green'}`}>
                    {
                      (max60)
                        .toFixed(quotations[symbol] ? quotations[symbol].decimal : 4)
                    }
                  </div>
                  <div className={`th  ${((max60 - min60) * 0.618 + min60) < quotations[symbol].close ? 'bg-light-red' : 'bg-light-green'}`}>
                    {
                      ((max60 - min60) * 0.618 + min60)
                        .toFixed(quotations[symbol] ? quotations[symbol].decimal : 4)
                    }
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="analyse-tlayer-strike">
            <div>周期(日)</div>
            <div>轴点</div>
            <div className="tlayer-line-box txt-center">
              {
                ((max1 - min1) * 0.5 + min1)
                  .toFixed(quotations[symbol] ? quotations[symbol].decimal : 4)
              }
              (0)
            </div>
            <div className="tlayer-line-box txt-center">
              {
                ((max5 - min5) * 0.5 + min5)
                  .toFixed(quotations[symbol] ? quotations[symbol].decimal : 4)
              }
              (5)
            </div>
            <div className="tlayer-line-box txt-center">
              {
                ((max10 - min10) * 0.5 + min10)
                  .toFixed(quotations[symbol] ? quotations[symbol].decimal : 4)
              }
              (10)
            </div>
            <div className="tlayer-line-box txt-center">
              {
                ((max20 - min20) * 0.5 + min20)
                  .toFixed(quotations[symbol] ? quotations[symbol].decimal : 4)
              }
              (20)
            </div>
            <div className="tlayer-line-box txt-center">
              {
                ((max30 - min30) * 0.5 + min30)
                  .toFixed(quotations[symbol] ? quotations[symbol].decimal : 4)
              }
              (30)
            </div>
            <div className="tlayer-line-box txt-center">
              {
                ((max60 - min60) * 0.5 + min60)
                  .toFixed(quotations[symbol] ? quotations[symbol].decimal : 4)
              }
              (60)
            </div>
          </div>
          <div className="analyse-tlayer-weak">
            <div className="weak-title">支撑点（S）</div>
            <div className="tlayer-box-weak">
              <div className="tlayer-strong-wrapper" onScroll={this.onRightScroll} ref={this.rightRef}>
                <div className="tlayer-weak-table-header tlayer-line">
                  <div className="th">R3</div>
                  <div className="th">R2</div>
                  <div className="th">R1</div>
                </div>
                <div className="flex tlayer-line tlayer-line-box">
                  <div className={`th  ${((max1 - min1) * 0.328 + min1) < quotations[symbol].close ? 'bg-light-red' : 'bg-light-green'}`}>
                    {
                      ((max1 - min1) * 0.328 + min1)
                        .toFixed(quotations[symbol] ? quotations[symbol].decimal : 4)
                    }
                  </div>
                  <div className={`th  ${(min1) < quotations[symbol].close ? 'bg-light-red' : 'bg-light-green'}`}>
                    {
                      (min1)
                        .toFixed(quotations[symbol] ? quotations[symbol].decimal : 4)
                    }
                  </div>
                  <div className={`th  ${((max1 - min1) * -0.328 + min1) < quotations[symbol].close ? 'bg-light-red' : 'bg-light-green'}`}>
                    {
                      ((max1 - min1) * -0.328 + min1)
                        .toFixed(quotations[symbol] ? quotations[symbol].decimal : 4)
                    }
                  </div>
                </div>
                <div className="flex tlayer-line tlayer-line-box">
                  <div className={`th  ${((max5 - min5) * 0.328 + min5) < quotations[symbol].close ? 'bg-light-red' : 'bg-light-green'}`}>
                    {
                      ((max5 - min5) * 0.328 + min5)
                        .toFixed(quotations[symbol] ? quotations[symbol].decimal : 4)
                    }
                  </div>
                  <div className={`th  ${(min5) < quotations[symbol].close ? 'bg-light-red' : 'bg-light-green'}`}>
                    {
                      (min5)
                        .toFixed(quotations[symbol] ? quotations[symbol].decimal : 4)
                    }
                  </div>
                  <div className={`th  ${((max5 - min5) * -0.328 + min5) < quotations[symbol].close ? 'bg-light-red' : 'bg-light-green'}`}>
                    {
                      ((max5 - min5) * -0.328 + min5)
                        .toFixed(quotations[symbol] ? quotations[symbol].decimal : 4)
                    }
                  </div>
                </div>
                <div className="flex tlayer-line tlayer-line-box">
                  <div className={`th  ${((max10 - min10) * 0.328 + min10) < quotations[symbol].close ? 'bg-light-red' : 'bg-light-green'}`}>
                    {
                      ((max10 - min10) * 0.328 + min10)
                        .toFixed(quotations[symbol] ? quotations[symbol].decimal : 4)
                    }
                  </div>
                  <div className={`th  ${(min10) < quotations[symbol].close ? 'bg-light-red' : 'bg-light-green'}`}>
                    {
                      (min10)
                        .toFixed(quotations[symbol] ? quotations[symbol].decimal : 4)
                    }
                  </div>
                  <div className={`th  ${((max10 - min10) * -0.328 + min10) < quotations[symbol].close ? 'bg-light-red' : 'bg-light-green'}`}>
                    {
                      ((max10 - min10) * -0.328 + min10)
                        .toFixed(quotations[symbol] ? quotations[symbol].decimal : 4)
                    }
                  </div>
                </div>
                <div className="flex tlayer-line tlayer-line-box">
                  <div className={`th  ${((max20 - min20) * 0.328 + min20) < quotations[symbol].close ? 'bg-light-red' : 'bg-light-green'}`}>
                    {
                      ((max20 - min20) * 0.328 + min20)
                        .toFixed(quotations[symbol] ? quotations[symbol].decimal : 4)
                    }
                  </div>
                  <div className={`th  ${(min20) < quotations[symbol].close ? 'bg-light-red' : 'bg-light-green'}`}>
                    {
                      (min20)
                        .toFixed(quotations[symbol] ? quotations[symbol].decimal : 4)
                    }
                  </div>
                  <div className={`th  ${((max20 - min20) * -0.328 + min20) < quotations[symbol].close ? 'bg-light-red' : 'bg-light-green'}`}>
                    {
                      ((max20 - min20) * -0.328 + min20)
                        .toFixed(quotations[symbol] ? quotations[symbol].decimal : 4)
                    }
                  </div>
                </div>
                <div className="flex tlayer-line tlayer-line-box">
                  <div className={`th  ${((max30 - min30) * 0.328 + min30) < quotations[symbol].close ? 'bg-light-red' : 'bg-light-green'}`}>
                    {
                      ((max30 - min30) * 0.328 + min30)
                        .toFixed(quotations[symbol] ? quotations[symbol].decimal : 4)
                    }
                  </div>
                  <div className={`th  ${(min30) < quotations[symbol].close ? 'bg-light-red' : 'bg-light-green'}`}>
                    {
                      (min30)
                        .toFixed(quotations[symbol] ? quotations[symbol].decimal : 4)
                    }
                  </div>
                  <div className={`th  ${((max30 - min30) * -0.328 + min30) < quotations[symbol].close ? 'bg-light-red' : 'bg-light-green'}`}>
                    {
                      ((max30 - min30) * -0.328 + min30)
                        .toFixed(quotations[symbol] ? quotations[symbol].decimal : 4)
                    }
                  </div>
                </div>
                <div className="flex tlayer-line tlayer-line-box">
                  <div className={`th  ${((max60 - min60) * 0.328 + min60) < quotations[symbol].close ? 'bg-light-red' : 'bg-light-green'}`}>
                    {
                      ((max60 - min60) * 0.328 + min60)
                        .toFixed(quotations[symbol] ? quotations[symbol].decimal : 4)
                    }
                  </div>
                  <div className={`th  ${(min60) < quotations[symbol].close ? 'bg-light-red' : 'bg-light-green'}`}>
                    {
                      (min60)
                        .toFixed(quotations[symbol] ? quotations[symbol].decimal : 4)
                    }
                  </div>
                  <div className={`th  ${((max60 - min60) * -0.328 + min60) < quotations[symbol].close ? 'bg-light-red' : 'bg-light-green'}`}>
                    {
                      ((max60 - min60) * -0.328 + min60)
                        .toFixed(quotations[symbol] ? quotations[symbol].decimal : 4)
                    }
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
