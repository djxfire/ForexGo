/*
* @Date: 2021/2/23
* @Author: XueChengwu <xuechengwu@erayt.com>
* @Copyright: 2015-2019 Erayt, Inc.
* @Description: If you have some questions, please contact: xuechengwu@erayt.com.
*/
import React from 'react';
import ajax from 'decorate/ajax';
import forexData from 'business/forexData';
import MarketData from './MarketData';
import Icon from 'components/Icon';
import Popup from 'components/Popup';
import List from 'components/List';
import Line from 'ochart/react/Line';

const currencys = {
  'USD': '美元',
  'CNY': '人民币',
  'EUR': '欧元',
  'GBP': '英镑',
  'JPY': '日元',
  'AUD': '澳元',
  'NZD': '纽元',
  'CHF': '瑞郎',
  'CAD': '加元'
};

@ajax(forexData.url, forexData.meta, forexData.resolve)
export default class extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      mkt: 0,
      stat: 0,
      label: '',
      data: [],
      showPop: false,
    };
  }

  componentWillReceiveProps(nextProps, nextContext) {
    const { currency } = nextProps;
    if (this.props.currency !== currency && MarketData[currency]) {
      this.setState({
        mkt: MarketData[currency][0].mkt,
        stat: MarketData[currency][0].stat,
        label: MarketData[currency][0].label,
        data: MarketData[currency]
      }, () => {
        this.postAjax();
      });
    }
  }

  componentDidMount() {
    const { currency } = this.props;
    if(MarketData[currency]) {
      this.setState({
        mkt: MarketData[currency][0].mkt,
        stat: MarketData[currency][0].stat,
        label: MarketData[currency][0].label,
        data: MarketData[currency]
      }, () => {
        this.postAjax();
      });
    }
  }

  onMouseOver = (legend) => {
    return '';
  }

  onDataPick = () => {
    this.setState({
      showPop: true,
    }, () => {
      console.log(this.state.showPop);
    })
  }

  onDataChange = (e) => {
    const { mkt, stat } = e.currentTarget.dataset;
    const { currency } = this.props;
    const index = MarketData[currency].findIndex(vo => vo.mkt === Number(mkt) && vo.stat === Number(stat));
    if (index >=  0) {
      this.setState({
        mkt: MarketData[currency][index].mkt,
        stat: MarketData[currency][index].stat,
        label: MarketData[currency][index].label,
        showPop: false,
      }, () => {
        this.postAjax();
      });
    }
  }

  render() {
    const { data = [], currency } = this.props;
    const { label, showPop, stat } = this.state;
    return (
      <React.Fragment>
        <div className="data-line-chart-card">
          <div className="data-line-chart-title bottom-line">
            <div className="data-line-chart-name">
              { currencys[currency]}区经济数据
            </div>
            <div className="seize" />
            <div className="data-line-chart-select" onClick={this.onDataPick}>
              { label }
              <Icon type="xiala"/>
            </div>
          </div>
          <div className="data-line-chart-content">
            <Line
              className="market-data-line"
              data={data}
              showNum={20}
              canDrag={true}
              style={{
                color: '#A3A3A3',
                fontColor: '#A3A3A3',
                enob: 2,
                type: 2,
                lineCap: Line.LINE_CAP.ROUND,
                colors: {
                  last: '#3575D8',
                  now: '#F46586',
                }
              }}
              onMouseOver={this.onMouseOver}
            />
          </div>
        </div>
        {
          showPop && (
            <Popup
              show={showPop}
              className="quotation-popup-panel"
              header={<div className="data-popup-header">{currencys[currency]}区经济数据</div>}
            >
              <List>
                {
                  MarketData[currency] && MarketData[currency].map((item) => {
                    return (
                      <List.Item>
                        <div
                          className={`data-popup-list-item ${item.stat === stat && 'active'}`}
                          data-mkt={item.mkt}
                          data-stat={item.stat}
                          onClick={this.onDataChange}

                        >
                          {item.label}
                          <div className="seize" />
                          { item.stat === stat && <Icon type="click1"/> }
                        </div>
                      </List.Item>
                    );
                  })
                }
              </List>
            </Popup>
          )
        }
      </React.Fragment>
    );
  }
}
