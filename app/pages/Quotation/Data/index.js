/*
* @Date: 2021/2/23
* @Author: XueChengwu <xuechengwu@erayt.com>
* @Copyright: 2015-2019 Erayt, Inc.
* @Description: If you have some questions, please contact: xuechengwu@erayt.com.
*/
import React from 'react';
import LineChart from './LineChart';
import './index.scss';

export default class extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      firstCurrency: '',
      sercondCurrency: '',
    };
  }

  componentWillReceiveProps(nextProps, nextContext) {
    const { productId } = nextProps;
    if (this.props.productId !== productId) {
      const firstCurrency = productId.substr(0, 3);
      const secondCurrency = productId.substr(3, 3);
      this.setState({
        firstCurrency,
        secondCurrency,
      });
    }
  }

  componentDidMount() {
    const { productId } = this.props;
    const firstCurrency = productId.substr(0, 3);
    const secondCurrency = productId.substr(3, 3);
    this.setState({
      firstCurrency,
      secondCurrency,
    });
  }

  render() {
    const { firstCurrency, secondCurrency } = this.state;
    console.log(firstCurrency, secondCurrency)
    return (
      <div className="quotation-chart-data">
        <div>
          { firstCurrency && <LineChart currency={firstCurrency} /> }
        </div>
        <div style={{marginTop: '24px'}}>
          { secondCurrency && <LineChart currency={secondCurrency} /> }
        </div>
      </div>
    );
  }
}
