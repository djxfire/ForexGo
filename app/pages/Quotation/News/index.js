/*
* @Date: 2021/2/17
* @Author: XueChengwu <xuechengwu@erayt.com>
* @Copyright: 2015-2019 Erayt, Inc.
* @Description: If you have some questions, please contact: xuechengwu@erayt.com.
*/
import React from 'react';
import BlockTab from 'components/BlockTab';
import ajax from 'decorate/ajax';
import forexNews from 'business/forexNews';
import './index.scss';

const currency = {
  'USD': '美元',
  'CNY': '人民币',
  'EUR': '欧元',
  'GBP': '英镑',
  'JPY': '日元',
  'AUD': '澳元',
  'NZD': '纽元',
  'CHF': '瑞郎',
  'CAD': '加元'
}

@ajax(forexNews.url, forexNews.meta, forexNews.resolve)
export default class extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      firstCurrency: '',
      sercondCurrency: '',
      activeCurrency: '',
      page: 1,
    };
  }

  componentWillReceiveProps(nextProps, nextContext) {
    const { productId } = this.props;
    if (this.props.productId !== productId) {
      const firstCurrency = productId.substr(0, 3);
      const secondCurrency = productId.substr(3, 3);
      this.setState({
        firstCurrency,
        secondCurrency,
        activeCurrency: firstCurrency,
        page: 1,
      }, () => {
        this.postAjax();
      });
    }
  }

  componentDidMount() {
    const { productId } = this.props;
    const firstCurrency = productId.substr(0, 3);
    const secondCurrency = productId.substr(3, 3);
    console.log('product id', productId);
    this.setState({
      firstCurrency,
      secondCurrency,
      activeCurrency: firstCurrency,
      page: 1,
    }, () => {
      this.postAjax();
    });
  }

  onNewsDetail = (e) => {
    const { newsId } = e.currentTarget.dataset;
    this.props.history.push(`/news/${newsId}`);
  }

  onCurrencyChange = (key) => {
    this.setState({
      activeCurrency: key,
      page: key === this.state.activeCurrency ? this.state.page : 1,
    }, () => {
      if (this.state.page === 1) {
        this.postAjax();
      }
    });
  }

  render() {
    const { data = [] } = this.props;
    const { firstCurrency, secondCurrency, activeCurrency } = this.state;
    return (
      <div className="quotation-news-wrapper">
        <BlockTab
          data={[{
            name: currency[firstCurrency],
            key: `${firstCurrency}`
          }, {
            name: currency[secondCurrency],
            key: `${secondCurrency}`
          }]}
          defaultValue={firstCurrency}
          onChange={this.onCurrencyChange}
        />
        <div className="quotation-news-list">
          {
            data.map((item) => {
              return (
                <div className="quotation-news-list-item bottom-line" data-news-id={item.id} onClick={this.onNewsDetail}>
                  <div className="quotation-news-list-title">
                    {item.title}
                  </div>
                  <div className="quotation-news-list-time">
                    {item.time}
                  </div>
                </div>
              );
            })
          }
        </div>
      </div>
    );
  }
}
