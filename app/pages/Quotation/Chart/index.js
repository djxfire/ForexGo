/*
* @Date: 2020/12/29
* @Author: XueChengwu <xuechengwu@erayt.com>
* @Copyright: 2015-2019 Erayt, Inc.
* @Description: If you have some questions, please contact: xuechengwu@erayt.com.
*/
import React from 'react';
import Tab from '@/components/Tab';
import Icon from '@/components/Icon';
import './index.scss';
import MiniChart from './MiniChart';
import Candlestick from '@/pages/Candlestick';
import Storage from '@/utils/storage';
import FiveChart from './FiveChart';

/**
 * 图表组件
 */
export default class extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      showMore: false,
      moreTxt: '更多',
      infoKey: 1,
      klineType: '201',
    };
  }
  onTabChange = (key) => {
    const { onKLineTypeChange } = this.props;
    let klineType = '101';
    if (Number(key) === 2) {
      klineType = '101';
    } else if (Number(key) === 3) {
      klineType = '102';
    } else if (Number(key) === 4) {
      klineType = '103';
    } else if (Number(key) === 0) {
      klineType = '201';
    } else if (Number(key) === 1) {
      klineType = '202';
    }
    this.setState({
      klineType,
    }, () => {
      onKLineTypeChange && onKLineTypeChange(this.state.klineType);
    });
  }

  onShowMore = () => {
    const { showMore } = this.state;
    this.setState({
      showMore: !showMore,
    });
  }

  onMoreClick = (e) => {
    const { key } = e.currentTarget.dataset;
    const { onKLineTypeChange } = this.props;
    let txt = `${key}分`;
    let klineType = '005';
    if (Number(key) === 5) {
      klineType = '005';
    } else if (Number(key) === 15) {
      klineType = '015';
    } else if (Number(key) === 30) {
      klineType = '030';
    } else if (Number(key) === 60) {
      klineType = '060';
    }
    this.setState({
      moreTxt: txt,
      showMore: false,
      klineType,
    }, () => {
      onKLineTypeChange && onKLineTypeChange(this.state.klineType);
    });
  }

  onInfoTabChange = (e) => {
    const { key } = e.currentTarget.dataset;
    this.setState({
      infoKey: Number(key),
    });
  }

  render() {
    const { showMore, moreTxt, infoKey, klineType = '201' } = this.state;
    const { productId, enob } = this.props;
    const products = Storage.get('fx_products', true, []);
    const productIndex = products.findIndex(vo => vo.secid === productId);
    return (
      <React.Fragment>
        <div className="chart-nav-box">
          <Tab
            className="chart-nav-tab"
            type="info"
            data={
              [
                { key: 0, label: '分时' },
                { key: 1, label: '五日' },
                { key: 2, label: '日K' },
                { key: 3, label: '周K' },
                { key: 4, label: '月K' },
                ]
            }
            onTabSwitch={this.onTabChange}
          />
          <div className="chart-nav-more">
            <span onClick={this.onShowMore}>{moreTxt}</span>
            {
              showMore && (
                <div className="chart-nav-more-box">
                  <div className="chart-nav-more-item" onClick={this.onMoreClick} data-key={5}>5分</div>
                  <div className="chart-nav-more-item" onClick={this.onMoreClick} data-key={15}>15分</div>
                  <div className="chart-nav-more-item" onClick={this.onMoreClick} data-key={30}>30分</div>
                  <div className="chart-nav-more-item" onClick={this.onMoreClick} data-key={60}>60分</div>
                </div>
              )
            }
          </div>
          <div className="chart-nav-setting">
            <Icon type="setting" />
          </div>
        </div>
        {
          klineType === '202' && (
            <div className="chart-body-box">
              <div className="chart-body-chart">
                <FiveChart
                  id={productIndex !== -1 ? products[productIndex].symbol : ''}
                  className="mini-chart"
                  productId={productId}
                  style={{
                    color: '#29A2F4',
                    enob: productIndex !== -1 ? products[productIndex].decimal : 2
                  }}
                  volumeClassName="volume-chart"
                />
              </div>
            </div>
          )
        }
        {
          klineType === '201' && (
            <div className="chart-body-box">
              <div className="chart-body-chart">
                <MiniChart
                  id={productIndex !== -1 ? products[productIndex].symbol : ''}
                  className="mini-chart"
                  productId={productId}
                  style={{
                    color: '#29A2F4',
                    enob: productIndex !== -1 ? products[productIndex].decimal : 2
                  }}
                  volumeClassName="volume-chart"
                />
              </div>
            </div>
          )
        }
        {
          klineType !== '201' && klineType !== '202' && (
            <Candlestick
              productId={productId}
              klineType={klineType}
              enob={productIndex !== -1 ? products[productIndex].decimal : 2}
              indicators={this.props.indicators}
              onQuoteDelete={this.props.onQuoteDelete}
              onQuoteSetting={this.props.onQuoteSetting}
            />
          )
        }
      </React.Fragment>
    );
  }
}
