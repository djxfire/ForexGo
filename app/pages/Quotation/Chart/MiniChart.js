/*
* @Date: 2021/1/1
* @Author: XueChengwu <xuechengwu@erayt.com>
* @Copyright: 2015-2019 Erayt, Inc.
* @Description: If you have some questions, please contact: xuechengwu@erayt.com.
*/
import React from 'react';
import ajax from 'decorate/ajax';
import Trend from 'business/trend';
import Time from '../../Time';
import Volume from '../../Volume';

@ajax(Trend.url, Trend.meta, Trend.resolve)
export default class extends React.Component {
  render() {
    const { data = [], preClose, trendsTotal = 0, className = '', style = {}, id } = this.props;
    return (
      <React.Fragment>
        <Time
          id={id}
          data={data}
          baseLine={preClose}
          totalCount={trendsTotal}
          className={className}
          style={style}
        />
      </React.Fragment>
    );
  }
}
