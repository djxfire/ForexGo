/*
* @Date: 2021/3/6
* @Author: XueChengwu <xuechengwu@erayt.com>
* @Copyright: 2015-2019 Erayt, Inc.
* @Description: If you have some questions, please contact: xuechengwu@erayt.com.
*/
import React from 'react';
import Icon from 'components/Icon';
import './index.scss';
import Button from 'components/Button';
import Toast from 'components/Toast';
import { inject, observer } from 'mobx-react';
import Popup from 'components/Popup';
import List from 'components/List';
import OChart from 'ochart/core/OChart';
import indicator from 'utils/indicator';
import Dialog from 'components/Dialog';
import IndicatorDialog from './IndicatorDialog';
const lines = [{
  label: '直线',
  value: 1,
}, {
  label: '线段',
  value: 2,
}, {
  label: '水平线',
  value: 3,
}, {
  label: '黄金分割线',
  value: 4,
}, {
  label: '江恩线',
  value: 5,
}, {
  label: '黄金分割环',
  value: 6,
}, {
  label: '斐纳波契时间',
  value: 7,
},{
  label: '回归线',
  value: 8,
}, {
  label: '甘氏线',
  value: 9,
}, {
  label: '周期线',
  value: 10,
}];

@inject('favorateStore')
@observer
export default class extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      showLinePop: false,
      lineColor: '#FFFFFF',
      showIndicatorPop: false,
    };
  }
  onTradeClick = () => {
    Toast.toast('敬请期待');
  }

  addFavorate = () => {
    const { productId } = this.props;
    this.props.favorateStore.add(productId);
  }

  removeFavorate = () => {
    const { productId } = this.props;
    this.props.favorateStore.remove(productId);
  }

  onColorChange = (e) => {
    const { color } = e.currentTarget.dataset;
    this.setState({
      lineColor: color,
    });
  }

  onShowDraw = () => {
    this.setState({
      showLinePop: true,
    });
  }

  onShowIndicator = () => {
    this.setState({
      showIndicatorPop: true,
    });
  }

  onCreateIndicator = (indicator, params) => {
    const { onIndicatorCreated } = this.props;
    onIndicatorCreated && onIndicatorCreated(indicator, params);
    Dialog.show({ isShow: false });
  }

  onIndicatorAdd = (e) => {
    const { indicatorName } = e.currentTarget.dataset;
    if (Object.keys(indicator[indicatorName].params).length === 0) {
      this.setState({
        showIndicatorPop: false,
      }, () => {

      });
    } else {
      this.setState({
        showIndicatorPop: false,
      }, () => {
        Dialog.show({
          title: '参数设置',
          hasMask: true,
          content: <IndicatorDialog indicatorName={indicatorName} onCreateIndicator={this.onCreateIndicator}/>
        });
      });

    }
  }

  onDrawing = (e) => {
    const { lineType } = e.currentTarget.dataset;
    const { lineColor } = this.state;
    const { secid, klineType } = this.props;
    OChart.drawLine(`chart-${secid}-${klineType}`, {
      type: Number(lineType),
      color: lineColor,
      onCreated: (line) => {
        console.log('line===>', line);
      }
    });
    this.setState({
      showLinePop: false,
    });
  }

  render() {
    const { className = '', productId, klineType } = this.props;
    const { favorates = [], } = this.props.favorateStore;
    const { showLinePop, lineColor, showIndicatorPop } = this.state;
    return (
      <React.Fragment>
        <div className={`${className} quotation-tool-box`}>
          <div className="quotation-tool-item">
            {
              favorates.findIndex(vo => vo === productId) === -1
                ? <Icon type="favorite" onClick={this.addFavorate}/>
                : <Icon type="favorite1" className="active" onClick={this.removeFavorate}/>
            }
          </div>
          <div className="quotation-tool-item">
            <Icon type="diejia"/>
          </div>
          <div className="quotation-tool-item">
            <Icon type="drawing" onClick={this.onShowDraw}/>
          </div>
          <div className="quotation-tool-item">
            <Icon type="chart" onClick={this.onShowIndicator}/>
          </div>
          <div className="quotation-tool-item">
            <Button type="primary" onClick={this.onTradeClick} >交易</Button>
          </div>
        </div>
        {
          showLinePop && (
            <Popup
              show={showLinePop}
              onClose={() => this.setState({ showLinePop: false })}
              className="quotation-popup-panel"
              header={
                <div className="drawing-popup-header">
                  线段类型
                  <div className="seize" />
                  <div className="drawing-line-colors">
                    <div
                      className={`drawing-color-item ${lineColor === '#FFFFFF' ? 'active' : ''}`}
                      onClick={this.onColorChange}
                      style={{background: '#FFFFFF'}}
                      data-color="#FFFFFF"
                    />
                    <div
                      className={`drawing-color-item ${lineColor === '#FFFF33' ? 'active' : ''}`}
                      onClick={this.onColorChange}
                      data-color="#FFFF33"
                      style={{background: '#FFFF33'}}
                    />
                    <div
                      className={`drawing-color-item ${lineColor === '#00AE00' ? 'active' : ''}`}
                      onClick={this.onColorChange}
                      data-color="#00AE00"
                      style={{background: '#00AE00'}}
                    />
                    <div
                      className={`drawing-color-item ${lineColor === '#D42922' ? 'active' : ''}`}
                      onClick={this.onColorChange}
                      data-color="#D42922"
                      style={{background: '#D42922'}}
                    />
                    <div
                      className={`drawing-color-item ${lineColor === '#FFA10D' ? 'active' : ''}`}
                      onClick={this.onColorChange}
                      data-color="#FFA10D"
                      style={{background: '#FFA10D'}}
                    />
                    <div
                      className={`drawing-color-item ${lineColor === '#88AAFF' ? 'active' : ''}`}
                      onClick={this.onColorChange}
                      data-color="#88AAFF"
                      style={{background: '#88AAFF'}}
                    />
                  </div>
                </div>
              }
            >
              <List>
                {
                  lines.map((item) => {
                    return (
                      <List.Item>
                        <div
                          className="drawing-popup-list-item"
                          onClick={this.onDrawing}
                          data-line-type={item.value}
                        >
                          {item.label}
                        </div>
                      </List.Item>
                    );
                  })
                }
              </List>
            </Popup>
          )
        }
        {
          showIndicatorPop && (
            <Popup
              show={showIndicatorPop}
              onClose={() => this.setState({ showIndicatorPop: false })}
              className="quotation-popup-panel"
              header={
                <div className="drawing-popup-header">
                  线段类型
                </div>
              }
            >
              <List>
                {
                  Object.keys(indicator).map((vo) => {
                    return (
                      <List.Item>
                        <div
                          className="drawing-popup-list-item"
                          onClick={this.onIndicatorAdd}
                          data-indicator-name={vo}
                        >
                          {
                            indicator[vo].name
                          }
                        </div>
                      </List.Item>
                    );

                  })
                }
              </List>
            </Popup>
          )
        }
      </React.Fragment>
    );
  }
}
