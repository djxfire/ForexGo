import React from "react";
import indicator from "utils/indicator";
import Dialog from "components/Dialog";

export default class extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            indicatorParams: {},
        };
    }

    componentDidMount() {
        const { indicatorName } = this.props;
        const indicatorParams = indicator[indicatorName].params;
        this.setState({
            indicatorParams
        });
    }

    onParamsChange = (e) => {
        const { value, name } = e.currentTarget;
        const { indicatorParams } = this.state;
        indicatorParams[name] = value;
        this.setState({
            indicatorParams
        });
    }

    onIndicator = () => {
        const { indicatorParams } = this.state;
        const { indicatorName, onCreateIndicator } = this.props;
        onCreateIndicator && onCreateIndicator(indicatorName, indicatorParams);
    }

    render() {
        const { indicatorParams } = this.state;
        const paramNames = [];
        for (let key in indicatorParams) {
            paramNames.push(key);
        }
        return (
            <div className="indicator-dialog">
                <div className="indicator-params-wrapper">
                    {
                        paramNames.map((item) => {
                            return (
                                <div className="indicator-params-item">
                                    <label>{item}</label>
                                    <div className="indicator-params-input">
                                        <input type="text" value={indicatorParams[item]} name={item} onChange={this.onParamsChange}/>
                                    </div>
                                </div>
                            )
                        })
                    }
                </div>
                <div className="indicator-params-buttons">
                    <div className="indicator-button-cancel" onClick={() => Dialog.show({ isShow: false })}>取消</div>
                    <div className="indicator-button-ok" onClick={this.onIndicator}>
                        确认
                    </div>
                </div>
            </div>
        );
    }
}
