/*
* @Date: 2021/2/26
* @Author: XueChengwu <xuechengwu@erayt.com>
* @Copyright: 2015-2019 Erayt, Inc.
* @Description: If you have some questions, please contact: xuechengwu@erayt.com.
*/
import React from 'react';
import Button from 'components/Button';
import Header from 'components/Header';
import Icon from 'components/Icon';
import Register from 'business/register';
import ajax from 'decorate/ajax';
import Toast from 'components/Toast';
import './index.scss';
import Uploader from 'components/Uploader';

@ajax(Register.url, Register.meta)
export default class extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      username: '',
      password: '',
      email: '',
      avatar: '',
    };
  }

  onNameChange = (e) => {
    const { value } = e.currentTarget;
    this.setState({
      username: value,
    });
  }

  onEmailChange = (e) => {
    const { value } = e.currentTarget;
    this.setState({
      email: value,
    });
  }

  onPasswordChange = (e) => {
    const { value } = e.currentTarget;
    this.setState({
      password: value,
    });
  }

  onChangeAvatar = (value) => {
    this.setState({
      avatar: value,
    });
  }

  onRegister = () => {
    const { username, password, email, avatar } = this.state;
    if (username === '') {
      Toast.toast('用户名不能为空');
      return;
    }
    if (password === '') {
      Toast.toast('密码不能为空');
      return;
    }
    if (email === '') {
      Toast.toast('邮箱不能为空');
      return;
    }
    if (avatar === '') {
      Toast.toast('请上传头像');
      return;
    }
    this.postAjax();
  }

  render() {
    return (
      <div className="xue-webview theme-bg">
        <Header
          theme="xue-webview-app-header theme-bg"
          rightContent={<Icon type="close" onClick={() => this.props.history.go(-1)}/>}
        >
          注册
        </Header>
        <div className="xue-webview-app-content">
          <div className="login-box">
            <h1>欢迎注册XX</h1>
            <div className="login">
              <div className="register-avatar-box">
                <Uploader
                  name="avatar"
                  className="register-avatar"
                  onChange={this.onChangeAvatar}
                  isAvatar={true}
                />
              </div>
              <div className="login-form-item">
                <label>用户名</label>
                <div className="login-form-input">
                  <input type="text" onChange={this.onNameChange} />
                </div>
              </div>
              <div className="login-form-item">
                <label>邮箱</label>
                <div className="login-form-input">
                  <input type="text" onChange={this.onEmailChange} />
                </div>
              </div>
              <div className="login-form-item">
                <label>密码</label>
                <div className="login-form-input">
                  <input type="password" onChange={this.onPasswordChange} />
                </div>
              </div>
            </div>
            <div className="login-box-button">
              <Button onClick={this.onRegister}>注册</Button>
            </div>
            <div className="register-info">
              <label>已有账号？去<span className="register-go-login" onClick={() => this.props.history.go(-1)}>登录</span></label>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
