/*
* @Date: 2021/1/1
* @Author: XueChengwu <xuechengwu@erayt.com>
* @Copyright: 2015-2019 Erayt, Inc.
* @Description: If you have some questions, please contact: xuechengwu@erayt.com.
*/
import React from 'react';
import ajax from '@/decorate/ajax';
import BarSequence from '@/business/BarSequence';
import Candlestick from 'ochart/react/Candlestick';
import Quote from 'ochart/react/Quote';
import Icon from 'components/Icon';
import './index.scss'

@ajax(BarSequence.url, BarSequence.meta, BarSequence.resolve)
export default class extends React.Component {
  constructor(props) {
    super(props);
  }

  componentWillReceiveProps (nextProps, nextContext){
    const { productId, klineType } = nextProps;
    if (productId !== this.props.productId || klineType !== this.props.klineType) {
      this.postAjax({
        data() {
          const end = XUE.formatDate(new Date(), 'yyyyMMdd');
          return {
            secid: productId,
            klt: klineType,
            fqt: 0,
            lmt: 2880,
            end,
            iscca: 1,
            fields1: 'f1,f2,f3,f4,f5',
            fields2: 'f51,f52,f53,f54,f55,f56,f57',
            ut:'f057cbcbce2a86e2866ab8877db1d059',
            forcect: 1
          }
        }
      });
    }
  }

  render() {
    const { data = [], productId, enob, klineType, indicators = [] } = this.props;
    return (
      <React.Fragment>
        <Candlestick
          data={data}
          className="bar-chart"
          name={`chart-${productId}-${klineType}`}
          style={{
            color: '#FFFFFF',
            xFontSize: 20,
            yFontSize: 20,
            enob,
            xFormatter: (label) => {
              return XUE.formatDate(new Date(label), 'yyyy-MM-dd')
            }
          }}
        />
        <div className="chart-quote-box">
          <Quote
            className="quote-chart"
            data={data}
            chart={`chart-${productId}-${klineType}`}
          />
        </div>
        {
          indicators.map(vo => (
            <div className="chart-quote-box">
              <Quote
                className="quote-chart"
                data={data}
                indicator={vo.name}
                params={vo.params}
                onDelete={() => this.props.onQuoteDelete(vo.id)}
                onSetting={() => this.props.onQuoteSetting(vo.id)}
                chart={`chart-${productId}-${klineType}`}
              />
            </div>
          ))
        }
      </React.Fragment>
      );
  }
}
