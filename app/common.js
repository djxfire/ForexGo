/*
* @Date: 2021/1/1
* @Author: XueChengwu <xuechengwu@erayt.com>
* @Copyright: 2015-2019 Erayt, Inc.
* @Description: If you have some questions, please contact: xuechengwu@erayt.com.
*/
import ws from "utils/ws";
import Net from "@/net/Net";
import EventEmitter from 'events';

let emitter = new EventEmitter();

window.XUE = {
  listeners: {},
  uid: 0,
  formatDate: (date, format) => {
    let o = {
      "M+" : date.getMonth()+1,                 //月份
      "d+" : date.getDate(),                    //日
      "h+" : date.getHours(),                   //小时
      "m+" : date.getMinutes(),                 //分
      "s+" : date.getSeconds(),                 //秒
      "q+" : Math.floor((date.getMonth()+3)/3), //季度
      "S"  : date.getMilliseconds()             //毫秒
    };
    if(/(y+)/.test(format))
      format=format.replace(RegExp.$1, (date.getFullYear()+"").substr(4 - RegExp.$1.length));
    for(let k in o)
      if(new RegExp("("+ k +")").test(format))
        format = format.replace(RegExp.$1, (RegExp.$1.length==1) ? (o[k]) : (("00"+ o[k]).substr((""+ o[k]).length)));
    return format;
  },
  addHandler(handler, callback) {
    emitter.on(handler, callback);
  },
  executeHandler(handler, params) {
    emitter.emit(handler, params);
  },
  formatTime: (date) => {
    function formatNumber (n) {
      const str = n.toString()
      return str[1] ? str : `0${str}`
    }
    const year = date.getFullYear()
    const month = date.getMonth() + 1
    const day = date.getDate()

    const hour = date.getHours()
    const minute = date.getMinutes()
    const second = date.getSeconds()

    const t1 = [year, month + 1, day].map(formatNumber).join('-')
    const t2 = [hour, minute, second].map(formatNumber).join(':')

    return `${t1} ${t2}`
  },
  addDate: (date, days) => {
    let time = date.getTime();
    let timeDiff = time + days * 24 * 60 * 60 * 1000;
    return new Date(timeDiff);
  },
  formatterBigNumber(val, decimal = 2) {
    if (!val) {
      return '';
    }
    if (isNaN(Number(val))) {
      return val;
    }
    if (Math.abs(Number(val)) > 100000000) {
      return `${(Number(val) / 100000000).toFixed(decimal)}亿`;
    } else if (Math.abs(Number(val)) > 10000) {
      return `${(Number(val) / 10000).toFixed(decimal)}万`;
    }
    return `${Number(val).toFixed(decimal)}`;
  },
  bgColor(val) {
    return Number(val) > 0 ? 'background-red' : Number(val) < 0 ? 'background-green' : 'background-normal';
  },
  emit(title, data) {
    if (this.listeners[title]) {
      this.listeners[title].forEach(vo => {
        vo.callback && vo.callback({ title, data });
      });
    }
  },
  subscribe(title, callback, scopeId) {
    if (!this.listeners[title]) {
      this.listeners[title] = [];
    }
    this.listeners[title].push({ callback, scopeId });
  },
  unsubscribe(title, scopeId) {
    if (!this.listeners[title]) {
      const index = this.listeners[title].findIndex(vo => vo.scopeId === scopeId);
      if (index >= 0) {
        this.listeners[title].splice(index, 1);
      }
    }
  },
  uuid() {
    return ++this.uid;
  },
  getGuid() {
    const target = 'xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx';
    return target.replace(/[x]/g, (c) => {
      const r = Math.random() * 16 | 0,
        v = c === 'x' ? r : (r & 0x3 | 0x8);
      return v.toString(16);
    });
  },
  Ws: ws,
  fetch: Net,
}
