import React from 'react';
import './index.scss';

export default class Modal extends React.Component {

    onClose = () => {
        const { onClose } = this.props;
        onClose && onClose();
    }

    render() {
        const {
            children,
            showOver = false,
            show = false,
            title = '',
            width = '400px',
            bodyClassName='',
            className=''
        } = this.props;
        return (
            <React.Fragment>
                { showOver && <div className="okay-modal-over" onClick={this.onClose} />}
                {
                    show && (
                        <div className={`${className} okay-modal`} style={{width}}>
                            {
                                title !== '' && (
                                    <div className="okay-modal-header">
                                        {title}
                                        <svg aria-hidden="true" className="icon close-icon" onClick={this.onClose} fill="#FFFFFF">
                                            <use xlinkHref="#icon-close" />
                                        </svg>
                                    </div>
                                )
                            }

                            <div className={`okay-modal-content ${bodyClassName}`}>
                                {children}
                            </div>

                        </div>
                    )
                }
            </React.Fragment>
        );
    }
}
