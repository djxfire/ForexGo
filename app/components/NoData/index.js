import React from 'react';
import Icon from '../Icon';
import './index.scss';

export default class extends React.Component {
    render() {
        const { className = '', info = '暂无数据' } = this.props;
        return (
            <div className={`${className} no-data-box`}>
                <div className="no-data-view">
                    <Icon type="kong" />
                    <p>{info}</p>
                </div>
            </div>
        )

    }
}
