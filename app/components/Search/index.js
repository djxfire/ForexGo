import React from 'react';
import Icon from '../Icon';
import './index.scss';

export default class Search extends React.Component {
    constructor(props) {
        super(props);
    }

    onSearch = (e) => {
        const { onSearch } = this.props;
        const { value } = e.target;
        onSearch && onSearch(value);
    }


    render() {
        const { placeholder = '', color = '#FFFFFF', className = '' } = this.props;
        return (
            <div className={`search-box ${className}`}>
                <Icon type="search" color={color}/>
                <div className="search-box-input">
                    <input type="search" placeholder={placeholder} onChange={this.onSearch}/>
                </div>
            </div>
        );
    }
}
