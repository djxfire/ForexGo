/*
* @Date: 2021/1/10
* @Author: XueChengwu <xuechengwu@erayt.com>
* @Copyright: 2015-2019 Erayt, Inc.
* @Description: If you have some questions, please contact: xuechengwu@erayt.com.
*/
import React from 'react';
import Icon from '../Icon';
import './index.scss';
export default class extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isShow: false,

    };
  }

  onToggle = () => {
    const { isShow } = this.state;
    this.setState({
      isShow: !isShow
    });
  }

  render() {
    const { isShow = false } = this.state;
    const { title, children, className = '' } = this.props;
    return (
      <div className={`drawer-box ${className}`}>
        <div className="drawer-box-header" onClick={this.onToggle}>
          <div className="drawer-box-header-title">{ title }</div>
          <div className="seize" />
          <Icon type="back1" className={ isShow ? 'drawer-box-down' : 'drawer-box-up' }/>
        </div>
        {
          isShow && (
            <div className="drawer-box-content">
              { children }
            </div>
          )
        }

      </div>
    );
  }
}
