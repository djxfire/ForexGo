import React from 'react';
import './index.scss';

export default class VerticalCarousel extends React.Component {
    render() {
        const { children, className = '' } = this.props;
        return (
            <div className="vertical-carousel-wrapper">
                <div className={`${className} vertical-carousel`}>
                    {children}
                </div>
            </div>

        );
    }
}

VerticalCarousel.Item = class extends React.Component {
    render() {
        const { className = '', children } = this.props;
        return (
            <div className={`${className} vertical-carousel-item`}>
                {children}
            </div>
        )
    }
}
