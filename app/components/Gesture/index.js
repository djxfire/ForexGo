import React from 'react';

export default class Gesture extends React.Component {
    onTouchStart = (e) => {

    }
    onTouchMove = (e) => {

    }

    onTouchEnd = (e) => {

    }
    
    render() {
        const { className = '', children } = this.props;
        return (
            <div
                className={className}
                onTouchStart={this.onTouchStart}
            >
                {
                    children
                }
            </div>
        )
    }
}

Gesture.Item = class extends React.Component {

}
