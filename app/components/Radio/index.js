import React from 'react';
import './index.scss';

export default class Radio extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isChecked: props.value
        };
    }

    onChange = () => {
        const { onChange } = this.props;
        this.setState({
            isChecked: !this.state.isChecked
        }, () => {
           onChange && onChange(this.state.isChecked);
        });
    }

    componentWillReceiveProps(nextProps, nextContext) {
        const { value } = nextProps;
        if (value !== this.state.isChecked) {
            this.setState({
                isChecked: value
            });
        }
    }


    render() {
        const { isChecked } = this.state;
        const { theme = 'success' } = this.props;
        return (
          <div className={`okay-radio-wrapper ${theme} ${isChecked ? 'active' : ''}`} onClick={this.onChange}>
              { isChecked && <div className="okay-radio-span" />}
          </div>
        );
    }
}
