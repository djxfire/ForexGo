/*
* @Date: 2021/2/27
* @Author: XueChengwu <xuechengwu@erayt.com>
* @Copyright: 2015-2019 Erayt, Inc.
* @Description: If you have some questions, please contact: xuechengwu@erayt.com.
*/
import './index.scss';


export default {
  toast(message, time = 2000) {
    const toastDom = document.createElement('div');
    toastDom.classList = 'toast-box';
    toastDom.innerText = message;
    document.body.appendChild(toastDom);
    setTimeout(() => {
      document.body.removeChild(toastDom);
    }, time);
  },
  message(type, message) {
    const messageBox = document.createElement('div');
    if (type === 'info') {
      messageBox.classList = 'message-box message-info';
    } else if (type === 'success') {
      messageBox.classList = 'message-box message-success';
    } else if (type === 'warning') {
      messageBox.classList = 'message-box message-warning';
    } else if (type === 'danger') {
      messageBox.classList = 'message-box message-danger';
    } else if (type === 'primary') {
      messageBox.classList = 'message-box message-primary';
    }
    messageBox.innerText = message;
    document.body.appendChild(messageBox);
    setTimeout(() => {
      document.body.removeChild(messageBox);
    }, 2000);
  }
}
