import React from 'react';
import './index.scss'

export default class Sperator extends React.Component {
    render() {
        const { className = '' } = this.props;
        return (
          <hr className={`erayt-separator ${className}`} />
        );
    }
}
