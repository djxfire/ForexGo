import React from 'react';
import Icon from '../Icon';
import './index.scss';

export default class extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        const { total = 5, value = 0, important = false } = this.props;
        const view = [];
        for(let i = 0; i < total; i++) {
            if (i < value) {
                view.push(<Icon type="star" className={`star-active ${important ? 'important' : ''}`} />)
            } else {
                view.push(<Icon type="star" className="star-no-active" />)
            }

        }
        return view;
    }
}
