import React from 'react';
import './index.scss';

export default function Flex(props) {
  const { className, children } = props
  return (
    <div className={`erayt-flex ${className ? className : ''}`}>
      {children}
    </div>
  );
}

Flex.Item = function(props) {
  const { className = '', children, column } = props
  const co = column ? `erayt-column-${column}` : 'erayt-column-1'
  return (
    <div className={`${co} ${className}`}>
      {children}
    </div>
  );
}
