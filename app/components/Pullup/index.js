/*
* @Date: 2021/2/28
* @Author: XueChengwu <xuechengwu@erayt.com>
* @Copyright: 2015-2019 Erayt, Inc.
* @Description: If you have some questions, please contact: xuechengwu@erayt.com.
*/
import React from 'react';
import './index.scss';

export default class extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      onAjaxing: false,
    };
  }
  onMore = (e) => {
    const { onAjaxing = true } = this.state;
    const { onLoading } = this.props;
    if (
      e.currentTarget.scrollHeight - e.currentTarget.scrollTop - e.currentTarget.offsetHeight < 40
      && onAjaxing
    ) {
      this.setState({
        onAjaxing: true,
      }, () => {
        onLoading && onLoading().then(() => {
          this.setState({
            onAjaxing: false,
          });
        });
      });
    }
  }

  render() {
    const { children } = this.props;
    return (
      <div className="pull-up-box" onScroll={this.onMore}>
        { children }
      </div>
    );
  }
}
