import React from 'react';
import './index.scss';

export default class extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            value: ''
        }
    }

    componentWillReceiveProps(nextProps, nextContext) {
        if (nextProps.defaultValue !== this.props.defaultValue) {
            this.setState({
                value: nextProps.defaultValue,
            });
        }
    }

    componentDidMount() {
        const { defaultValue } = this.props;
        this.setState({
            value: defaultValue
        });
    }

    onClick = (e) => {
        const { key } = e.currentTarget.dataset;
        const { onChange } = this.props;
        this.setState({
            value: key
        }, () => {
            onChange && onChange(this.state.value);
        })
    }

    render() {
        const { data = [], className = '' } = this.props;
        const { value } = this.state;
        return (
            <div className={`xue-block-tab-wrapper ${className}`}>
                <div className="xue-block-tab">
                    {
                        data.map((item) => {
                            return (
                                <div className={`xue-block-tab-item ${value === item.key ? 'active' : ''}`} onClick={this.onClick} data-key={item.key}>
                                    <div className={`xue-block-tab-item-content  ${value === item.key ? 'active' : ''}`}>
                                        {
                                            item.name
                                        }
                                    </div>

                                </div>
                            );
                        })
                    }
                </div>
            </div>

        );
    }
}
