import React from 'react';
import './index.scss';

export default class extends React.Component {
    constructor(props) {
        super(props);
        this.fileRef = React.createRef();
    }

    onFileOpen = () => {
        this.fileRef.current.click();
    }

    onFileRead = (e) => {
        const files = e.currentTarget.files;
        const { onRead } = this.props;
        const formData = new FormData();
        for(let file of files) {
            formData.append('files[]', file)
        }
        onRead && onRead(formData);
    }

    render() {
        return (
            <div className="images-add" onClick={this.onFileOpen}>
                <span>+</span>
                <input type="file" multiple="multiple" ref={this.fileRef} onChange={this.onFileRead} />
            </div>
        )
    }
}
