import React from 'react';
import './index.scss';
import Radio from '../Radio';
import List from '../List';

export default class Index extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            value: props.value ? props.value : props.data[0].value
        };
    }

    onChange = (value) => {
        const { onChange } = this.props;
        this.setState({
            value
        }, () => {
            onChange && onChange(value);
        })
    }

    render() {
        const { data, type = 'list', className = '', theme='success' } = this.props;
        const { value } = this.state;
        return (
          <div className={`okay-radio-group-wrapper ${className}`}>
              {
              type === 'list'
              ? (
                <List>
                    {
                        data.map((item) => {
                            return (
                                <List.Item onClick={() => {this.onChange(item.value)}} className="okay-radio-list-item">
                                    <Radio value={item.value === value} theme={theme} />
                                    <span className="okay-radio-list-label">{item.label}</span>
                                </List.Item>
                            )
                        })
                    }
                </List>
              )
              : (
                <div>
                    {
                        data.map((item) => {
                            return (
                                <div onClick={() => {this.onChange(item.value)}}>
                                    <Radio value={item.value === value} theme={theme}/>
                                    <span>{item.label}</span>
                                </div>
                            );
                        })
                    }
                </div>
              )
              }
          </div>
        );
    }
}
