import React from 'react';

export default class Icon extends React.Component {
  onClick = (e) => {
    const { onClick } = this.props;
    onClick && onClick(e);
  }

  render() {
    const { type, className = '', color = '#FFFFFF' } = this.props;
    return (
      <svg aria-hidden="true" className={`icon ${className}`} onClick={this.onClick}>
        <use xlinkHref={`#icon${type}`} />
      </svg>
    );
  }
}
