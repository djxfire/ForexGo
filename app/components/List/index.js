import React from 'react';
import './index.scss';

export default class List extends React.Component {
  render() {
    const { listBar, listHeader, children, className = '' } = this.props;
    return (
      <div className={`erayt-list ${className}`}>
        { listBar }
        {
          listHeader && (
          <div className="erayt-list-header">
            {listHeader}
          </div>
          )
        }

        <div className="erayt-list-box">
          {children}
        </div>
      </div>
    );
  }
}

List.Item = (props) => {
  const { children, onClick, className } = props
  return (
      <React.Fragment>
        <div className={`erayt-list-item bottom-line ${className ? className : ''}`} onClick={onClick}>
          {children}
        </div>
      </React.Fragment>

  );
}

List.SlideItem = class extends React.Component {
  constructor(props) {
    super(props);
    this.slideStarted = false;
    this.myRef = React.createRef();
    this.slideRef = React.createRef();
    this.startSlideX = 0;
    this.endSlideX = 0;
    this.state = {
      slideDist: 0
    };
  }

  startSlide = (e) => {
    this.slideStarted = true;
    this.startSlideX = e.touches[0].clientX;
  }

  moveSlide = (e) => {
    const operatorWidth = parseInt(getComputedStyle(this.myRef.current).width);
    console.log(operatorWidth)
    this.endSlideX = e.touches[0].clientX;
    // 判断是左滑还是右滑
    if (this.startSlideX < this.endSlideX) {
      // 右滑
      if (this.state.slideDist - this.endSlideX + this.startSlideX > 0) {
        this.setState({
          slideDist: this.state.slideDist - this.endSlideX + this.startSlideX
        });
      }
    } else {
      // 左滑
      if (this.startSlideX - this.endSlideX < operatorWidth) {
        this.setState({
          slideDist: this.startSlideX - this.endSlideX
        });
      }
    }
  }

  endSlide = (e) => {
    if (this.slideStarted) {
      this.slideStarted = false;
      let operatorWidth = parseInt(getComputedStyle(this.myRef.current).width);
      console.log(operatorWidth)
      // 判断是左滑还是右滑
      if (this.startSlideX < this.endSlideX) {
        // 右滑
        this.setState({
          slideDist: 0
        });
      } else {
        // 左滑
        this.setState({
          slideDist: operatorWidth
        });
      }
    }
  }

  componentDidMount() {
    const slideStyle = getComputedStyle(this.slideRef.current);
    this.myRef.current.style.height = slideStyle.height;
  }

  render() {
    const { children, className, operator, onClick } = this.props;
    const { slideDist } = this.state
    return (
      <div className={`bottom-line erayt-list-slide-item ${className ? className : ''}`}>
        <div className="info" ref={this.slideRef} onClick={onClick} onTouchStart={this.startSlide} onTouchMove={this.moveSlide} onTouchEnd={this.endSlide} style={{transform: `translate3d(-${slideDist}px, 0px, 0px)`}}>
          {children}
        </div>
        <div className="slide" ref={this.myRef} style={{transform: `translate3d(-${slideDist}px, 0px, 0px)`}}>
          {operator}
        </div>
      </div>
    );
  }
}
