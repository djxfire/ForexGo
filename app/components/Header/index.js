import React from 'react';
import './index.scss';
export default class Header extends React.Component {
  render() {
    const { theme, backIcon, onBack,leftContent, children, rightContent } = this.props
    return (
      <div className={['header-bar', theme].join(' ')}>
        <div className="header-bar-left">
          {backIcon && <svg className="icon" aria-hidden="true" onClick={(e) => { return onBack ? onBack(e) : false} }>
            <use xlinkHref="#iconback" />
          </svg>}
          {leftContent}
        </div>
        <div className="header-bar-title">
          {children}
        </div>
        <div className="header-bar-right">
          {rightContent}
        </div>
      </div>
    );
  }
}
