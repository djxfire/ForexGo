import React from 'react';
import './index.scss';

export default class Button extends React.Component {
    constructor(props) {
        super(props);
    }

    onClick = (e) => {
        const { onClick } = this.props;
        onClick && onClick(e);
    }

    render () {
        const { children, className = '', type = 'primary', outline = false, small = false } = this.props
        return (
            <div className={`okay-button okay-button-${type} ${outline ? 'outline' : ''} ${small ? 'small' : ''}  ${className}`} onClick={this.onClick}>
                { children }
            </div>
        );
    }
}
