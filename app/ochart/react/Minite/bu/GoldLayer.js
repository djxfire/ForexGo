import Layer from '../../../core/Layer';
import Line from '../../../base/Line';
import Text from '../../../base/Text';
import Point from '../../../core/Point';


export default class extends Layer {
    constructor(canvas, style, option) {
        super(canvas, style);
        this.xStep = option.xStep;
        this.yStep = option.yStep;
        this.minIndex = option.minIndex;
        this.maxIndex = option.maxIndex;
        this.baseLine = option.baseLine;
        this.min = option.min;
        this.max = option.max;
        this.decimal = option.decimal || 2;
    }

    make() {
        this.childs.splice(0, this.childs.length);
        [-0.618, -0.382, 0, 0.236, 0.382, 0.5, 0.618, 0.809, 1, 1.382, 1.618].forEach(vo => {
            const targetValue = (this.max - this.min) * vo + this.min
            const yPosition = (targetValue - this.baseLine) * this.yStep + this.height / 2;
            const line = new Line(this.canvas, {
                position: new Point(this.position.x, yPosition + this.position.y),
                to: new Point(this.width, yPosition + this.position.y),
                color: this.color,
            });
            const txt = new Text(this.canvas, {
                text: targetValue.toFixed(this.decimal),
                color: this.color,
                size: 16,
            });
            txt.setPosition(this.width - txt.width, this.position.y + yPosition);
            this.addChild(line, txt);
        });
    }
}
