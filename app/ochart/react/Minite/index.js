import React from 'react';
import MiniLayer from './bu/MiniLayer';
import AxisLayer from '../../layer/AxisLayer';
import Canvas from '../../core/Canvas';
import Point from "../../core/Point";
import OChart from '../../core/OChart';

export default class extends React.Component {
    constructor(props) {
        super(props);
        this.chartRef = React.createRef();
    }

    componentWillReceiveProps (nextProps, nextContext) {
        const { data = [], totalCount, baseLine } = nextProps;
        if (
            this.props.data !== data
            || this.props.totalCount !== totalCount
            || this.props.baseLine !== baseLine
        ) {
            this.miniLayer.data = data;
            this.miniLayer.count = totalCount;
            this.miniLayer.baseLine = baseLine;
            this.miniLayer.make();
            this.canvas.paint();
        }
    }

    updateRate = ({ midRate }) => {
        const { data = [] } = this.miniLayer;
        const label = new Date().getTime();
        if (data.length === 0) {
            data.push({
                value: midRate,
                label,
            });
        } else {
            const vo = data[data.length - 1];
            if (label - vo.label < 60000) {
                data[data.length - 1] = {
                    ...data[data.length - 1],
                    label,
                    value: midRate,
                }
            } else {
                data.push({
                    label,
                    value: midRate,
                });
            }
        }
        this.miniLayer.data = data;
        this.miniLayer.make();
        this.canvas.paint();
    }

    componentDidMount () {
        const {
            style = { color: '#333333', axisColor: '#333333', xFontSize: 20 },
            data = [],
            totalCount = 24 * 60 * 60,
            baseLine = 0,
            id,
        } = this.props;
        const xFontSize = style.xFontSize || 0;
        this.canvas = new Canvas({
            ele: this.chartRef.current,
            canAction: false,
        });
        // 坐标系基础配置
        this.axisLayer = new AxisLayer(this.canvas, {
            yAxisType: AxisLayer.AxisType.NUMBER, // y轴为数值型
            xAxisType: AxisLayer.AxisType.LABEL,  // x轴时间为字符型
            xAxisGraduations: style.xAxis || 3,   // 网格5列
            yAxisGraduations: style.yAxis || 5,   // 网格5行
            xAxisPosition: AxisLayer.AxisPosition.BLOCK,  // X轴坐标不计算
            yAxisPosition: AxisLayer.AxisPosition.INNER,  // Y轴坐标计算
            yAxisRender: (value) => {
                const enob = style.enob || 2;
                return {
                    text: Number(value).toFixed(enob),
                    size: Number(style.yFontSize || 20),
                    color: style.axisColor || '#999999',
                    font: style.fontFamily || 'PingFang SC',
                };
            },
            xAxisRender: (label) => {
                const { value } = label;
                return {
                    text: value,
                    size: Number(style.xFontSize || 20),
                    color: style.axisColor || '#999999',
                    font: style.fontFamily || 'PingFang SC',
                };
            },
            color: style.color,
        });
        this.miniLayer = new MiniLayer(this.canvas, {
            color: style.color,
            height: (this.canvas.height - xFontSize) * 0.8, // 预留20%的空白空间
            width: this.canvas.width,
            position: new Point(0, xFontSize * 0.90 + 0.1 * this.canvas.height), // 预留的10% + 坐标的高度
            count: totalCount,
            baseLine,
            baseY: xFontSize,
            onMaked: ({ xStep, yStep, baseLine, pcg, maxIndex, minIndex, max, min }) => {
                let yAxisMax = max + (this.canvas.height - xFontSize) * 0.1 / yStep;
                let yAxisMin = min - (this.canvas.height - xFontSize) * 0.1 / yStep;
                this.axisLayer.yAxisMin = yAxisMin;
                this.axisLayer.yAxisMax = yAxisMax;
                this.axisLayer.make();
            }
        }, data);
        this.miniLayer.make();
        //this.canvas.addChild(this.axisLayer);
        this.canvas.addChild(this.miniLayer);
        this.canvas.paint();
        OChart.on(`${id}:updateMinData`, this.updateRate);
    }

    componentWillUnmount() {
        const { id } = this.props;
        OChart.remove(`${id}:updateMinData`, this.updateRate)
    }

    render() {
        const { className = '' } = this.props;
        return (
            <div className={className} ref={this.chartRef}/>
        )
    }
}
