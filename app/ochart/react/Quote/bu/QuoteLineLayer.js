import Layer from '../../../core/Layer';
import Point from '../../../core/Point';
import MultiLine from '../../../base/MultiLine';
import Line from '../../../base/Line';
import Text from '../../../base/Text';

export default class extends Layer {
    constructor(canvas, style, data) {
        super(canvas, style);
        this.data = data || [];
        this.thresholds = style.thresholds || [];
        this.max = style.max;
        this.min = style.min;
        this.xStep = style.xStep || 1;
        this.base = style.base || 0;
        this.fixed = style.fixed || false;
    }
    make() {
        if (this.data.length === 0) {
            return;
        }
        this.childs.splice(0, this.childs.length);
        let max = this.data[0].data[0];
        let min = max;
        for (let i = 0; i < this.data.length; i++) {
            for (let j = 0; j < this.data[i].data.length; j++) {
                if (max < this.data[i].data[j]) {
                    max = this.data[i].data[j];
                }
                if (min > this.data[i].data[j]) {
                    min = this.data[i].data[j];
                }
            }
        }
        if (max < this.max) {
            max = this.max;
        }
        if (min > this.min) {
            min = this.min;
        }
        let dist = max - min;
        if (this.fixed) {
            dist = 2 * (Math.abs(max - this.base) > Math.abs(min - this.base) ? Math.abs(max - this.base) : Math.abs(min - this.base))
            min = this.base - dist / 2;
        }
        const yStep = this.height / dist;
        for (let i = 0; i < this.data.length; i++) {
            let points = [];
            let line = this.data[i];
            for (let j = 0; j < line.data.length; j++) {
                points.push(
                    new Point(
                        this.position.x + j * this.xStep,
                        this.position.y + (line.data[j] - min) * yStep
                    )
                );
            }
            let multiLine = new MultiLine(this.canvas, {
                color: this.data[i].color,
                lineWidth: 2,
            }, points);
            this.addChild(multiLine);
        }
        for (let i = 0; i < this.thresholds.length; i++) {
            let threshold = this.thresholds[i];
            if (threshold < max && threshold > min) {
                let line = new Line(this.canvas, {
                    lineWidth: 1,
                    color: this.color,
                    lineDash: [2, 4],
                    to: new Point(
                        this.position.x + this.width,
                        this.position.y + (threshold - min) * yStep
                    ),
                    position: new Point(
                        this.position.x,
                        this.position.y + (threshold - min) * yStep
                    ),
                });
                let txt = new Text(this.canvas, {
                    color: this.color,
                    text: threshold,
                    font: 'PingFang SC',
                    size: this.fontSize,
                });
                txt.setPosition(
                    this.position.x + this.width - txt.width,
                    this.position.y + (threshold - min) * yStep - txt.height / 2
                );
                this.addChild(line, txt);
            }
        }
    }
}
