import Layer from '../../core/Layer';
import Circle from '../../base/Circle';
import Point from '../../core/Point';
import Event from '../../event/Event';
import Line from '../../base/Line';
import Text from '../../base/Text';
import Ring from '../../base/Ring';

export default class extends Layer {
    constructor(canvas, style) {
        super(canvas, style);
        this.uuid = style.uuid;
        this.onLineMove = style.onLineMove;
        this.onLineMoved = style.onLineMoved;
        this.hasChange = false;
        this.mesures = style.mesures || [];
        this.colors = style.colors || [];
        this.circle1 = new Circle(this.canvas, {
            radius: 10,
            color: this.color,
            position: style.point1,
        });
        this.circle2 = new Circle(this.canvas, {
            radius: 10,
            color: this.color,
            position: style.point2,
        });
        const radiar = Math.sqrt(
            (this.circle1.position.y - this.circle2.position.y) * (this.circle1.position.y - this.circle2.position.y)
            + (this.circle1.position.x - this.circle2.position.x) * (this.circle1.position.x - this.circle2.position.x)
        );
        const line = new Line(this.canvas, {
           position: this.circle1.position,
           to: this.circle2.position,
           lineWidth: 2,
           color: this.color,
        });
        this.addChild(line, this.circle1, this.circle2);
        this.mesures.sort();
        for(let i = 0; i < this.mesures.length; i++) {
            if (i === 0) {
                let ring = new Ring(this.canvas, {
                    longRadius: this.mesures[i] * radiar,
                    shortRadius: 0,
                    type: Ring.TYPE.FILL,
                    color: this.colors[i],
                    position: this.circle1.position,
                });
                ring.alpha = 0.4;
                this.addChild(ring);
            } else {
                let ring = new Ring(this.canvas, {
                    longRadius: this.mesures[i] * radiar,
                    shortRadius: this.mesures[i - 1] * radiar,
                    type: Ring.TYPE.STROKE,
                    color: this.colors[i],
                    position: this.circle2.position,
                });
                ring.alpha = 0.4;
                this.addChild(ring);
            }
            const text1 = new Text(this.canvas, {
                font: '宋体',
                text: this.mesures[i],
                size: 16,
                color: this.color,
                position: new Point(this.circle2.position.x, this.circle2.position.y + this.mesures[i] * radiar),
            });
            const text2 = new Text(this.canvas, {
                font: '宋体',
                text: this.mesures[i],
                size: 16,
                color: this.color,
                position: new Point(this.circle2.position.x, this.circle2.position.y - this.mesures[i] * radiar),
            });
            this.addChild(text1, text2);
        }
        this.circle1.addEventListener(Event.EVENT_TOUCH_MOVE, (e) => {
           this.circle1.position = new Point(e.curPoint.x, e.curPoint.y);
           this.childs.splice(0, this.childs.length);
           this.onLineMove && this.onLineMove(this);
           this.hasChange = true;
            const radiar = Math.sqrt(
                (this.circle1.position.y - this.circle2.position.y) * (this.circle1.position.y - this.circle2.position.y)
                + (this.circle1.position.x - this.circle2.position.x) * (this.circle1.position.x - this.circle2.position.x)
            );
            const line = new Line(this.canvas, {
                position: this.circle1.position,
                to: this.circle2.position,
                lineWidth: 2,
                color: this.color,
            });
            this.addChild(line, this.circle1, this.circle2);
            this.mesures.sort();
            for(let i = 0; i < this.mesures.length; i++) {
                if (i === 0) {
                    let ring = new Ring(this.canvas, {
                        longRadius: this.mesures[i] * radiar,
                        shortRadius: 0,
                        type: Ring.TYPE.STROKE,
                        color: this.colors[i],
                        position: this.circle2.position,
                    });
                    ring.alpha = 0.4;
                    this.addChild(ring);
                } else {
                    let ring = new Ring(this.canvas, {
                        longRadius: this.mesures[i] * radiar,
                        shortRadius: this.mesures[i - 1] * radiar,
                        type: Ring.TYPE.STROKE,
                        color: this.colors[i],
                        position: this.circle2.position,
                    });
                    ring.alpha = 0.4;
                    this.addChild(ring);
                }
                const text1 = new Text(this.canvas, {
                    font: '宋体',
                    text: this.mesures[i],
                    size: 16,
                    color: this.color,
                    position: new Point(this.circle2.position.x, this.circle2.position.y + this.mesures[i] * radiar),
                });
                const text2 = new Text(this.canvas, {
                    font: '宋体',
                    text: this.mesures[i],
                    size: 16,
                    color: this.color,
                    position: new Point(this.circle2.position.x, this.circle2.position.y - this.mesures[i] * radiar),
                });
                this.addChild(text1, text2);
            }
        });
        this.circle2.addEventListener(Event.EVENT_TOUCH_MOVE, (e) => {
            this.circle2.position = new Point(e.curPoint.x, e.curPoint.y);
            this.childs.splice(0, this.childs.length);
            this.onLineMove && this.onLineMove(this);
            this.hasChange = true;
            const radiar = Math.sqrt(
                (this.circle1.position.y - this.circle2.position.y) * (this.circle1.position.y - this.circle2.position.y)
                + (this.circle1.position.x - this.circle2.position.x) * (this.circle1.position.x - this.circle2.position.x)
            );
            const line = new Line(this.canvas, {
                position: this.circle1.position,
                to: this.circle2.position,
                lineWidth: 2,
                color: this.color,
            });
            this.addChild(line, this.circle1, this.circle2);
            this.mesures.sort();
            for(let i = 0; i < this.mesures.length; i++) {
                if (i === 0) {
                    let ring = new Ring(this.canvas, {
                        longRadius: this.mesures[i] * radiar,
                        shortRadius: 0,
                        type: Ring.TYPE.STROKE,
                        color: this.colors[i],
                        position: this.circle2.position,
                    });
                    ring.alpha = 0.4;
                    this.addChild(ring);
                } else {
                    let ring = new Ring(this.canvas, {
                        longRadius: this.mesures[i] * radiar,
                        shortRadius: this.mesures[i - 1] * radiar,
                        type: Ring.TYPE.STROKE,
                        color: this.colors[i],
                        position: this.circle2.position,
                    });
                    ring.alpha = 0.4;
                    this.addChild(ring);
                }
                const text1 = new Text(this.canvas, {
                    font: '宋体',
                    text: this.mesures[i],
                    size: 16,
                    color: this.color,
                    position: new Point(this.circle2.position.x, this.circle2.position.y + this.mesures[i] * radiar),
                });
                const text2 = new Text(this.canvas, {
                    font: '宋体',
                    text: this.mesures[i],
                    size: 16,
                    color: this.color,
                    position: new Point(this.circle2.position.x, this.circle2.position.y - this.mesures[i] * radiar),
                });
                this.addChild(text1, text2);
            }
        });
        this.circle1.addEventListener(Event.EVENT_TOUCH_END, (e) => {
            if (this.hasChange) {
                this.onLineMoved && this.onLineMoved(this);
            }
            this.hasChange = false;
        });
        this.circle2.addEventListener(Event.EVENT_TOUCH_END, (e) => {
            if (this.hasChange) {
                this.onLineMoved && this.onLineMoved(this);
            }
            this.hasChange = false;
        });
    }

    setSelected(isSelected) {
        this.circle1.visible = isSelected;
        this.circle2.visible = isSelected;
    }

    containsPoint (point) {
        let k = (this.circle1.position.y - this.circle2.position.y)
            / (this.circle1.position.x - this.circle2.position.x);
        let yDown = k * (point.x - this.circle2.position.x) + this.circle2.position.y - 10;
        let yUp = yDown + 20;
        if ((point.x - this.circle1.position.x) * (point.x - this.circle2.position.x) < 0 && (point.y - yDown) * (point.y - yUp) < 0) {
            return true;
        }
        return false;
    }
}
