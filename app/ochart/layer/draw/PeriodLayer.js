import Layer from '../../core/Layer';
import Point from '../../core/Point';
import Line from '../../base/Line';
import Event from '../../event/Event';
import Circle from '../../base/Circle';



export default class extends Layer {
    constructor(canvas, style) {
        super(canvas, style);
        this.uuid = style.uuid;
        this.onLineMove = style.onLineMove;
        this.onLineMoved = style.onLineMoved;
        this.hasChange = false;
        this.circle1 = new Circle(this.canvas, {
            radius: 10,
            color: this.color,
            position: style.point1,
        });
        this.circle2 = new Circle(this.canvas, {
            radius: 10,
            color: this.color,
            position: style.point2,
        });
        const dist = Math.abs(this.circle1.position.x - this.circle2.position.x);
        const line = new Line(this.canvas, {
            position: new Point(this.circle1.position.x, 0),
            to: new Point(this.circle1.position.x, this.height),
            color: this.color,
            lineWidth: 2,
        });
        const line2 = new Line(this.canvas, {
            position: new Point(this.circle2.position.x, 0),
            to: new Point(this.circle2.position.x, this.height),
            color: this.color,
            lineWidth: 2,
        });
        let minX = 0;
        let maxX = 0;
        if (this.circle1.position.x > this.circle2.position.x) {
            minX = this.circle2.position.x;
            maxX = this.circle1.position.x;
        } else {
            minX = this.circle1.position.x;
            maxX = this.circle2.position.x;
        }
        const maxNum = Math.floor((this.width - maxX) / dist);
        const minNum = Math.floor(minX / dist);
        for (let i = 0; i < maxNum; i++) {
            const line = new Line(this.canvas, {
                position: new Point(maxX + dist * i, 0),
                to: new Point(maxX  + dist * i, this.height),
                color: this.color,
                lineWidth: 2,
            });
            this.addChild(line);
        }
        for (let i = 0; i < minNum; i++) {
            const line = new Line(this.canvas, {
                position: new Point(minX - dist * i, 0),
                to: new Point(minX - dist * i, this.height),
                color: this.color,
                lineWidth: 2,
            });
            this.addChild(line);
        }
        this.addChild(line, line2, this.circle1, this.circle2);
        this.circle1.addEventListener(Event.EVENT_TOUCH_MOVE, (e) => {
            this.circle1.position = new Point(e.curPoint.x, e.curPoint.y);
            this.childs.splice(0, this.childs.length);
            this.onLineMove && this.onLineMove(this);
            this.hasChange = false;
            const dist = Math.abs(this.circle1.position.x - this.circle2.position.x);
            const line = new Line(this.canvas, {
                position: new Point(this.circle1.position.x, 0),
                to: new Point(this.circle1.position.x, this.height),
                color: this.color,
                lineWidth: 2,
            });
            const line2 = new Line(this.canvas, {
                position: new Point(this.circle2.position.x, 0),
                to: new Point(this.circle2.position.x, this.height),
                color: this.color,
                lineWidth: 2,
            });
            let minX = 0;
            let maxX = 0;
            if (this.circle1.position.x > this.circle2.position.x) {
                minX = this.circle2.position.x;
                maxX = this.circle1.position.x;
            } else {
                minX = this.circle1.position.x;
                maxX = this.circle2.position.x;
            }
            const maxNum = Math.floor((this.width - maxX) / dist);
            const minNum = Math.floor(minX / dist);
            for (let i = 0; i < maxNum; i++) {
                const line = new Line(this.canvas, {
                    position: new Point(maxX + dist * i, 0),
                    to: new Point(maxX  + dist * i, this.height),
                    color: this.color,
                    lineWidth: 2,
                });
                this.addChild(line);
            }
            for (let i = 0; i < minNum; i++) {
                const line = new Line(this.canvas, {
                    position: new Point(minX - dist * i, 0),
                    to: new Point(minX - dist * i, this.height),
                    color: this.color,
                    lineWidth: 2,
                });
                this.addChild(line);
            }
            this.addChild(line, line2, this.circle1, this.circle2);
        });
        this.circle2.addEventListener(Event.EVENT_TOUCH_MOVE, (e) => {
            this.circle2.position = new Point(e.curPoint.x, e.curPoint.y);
            this.childs.splice(0, this.childs.length);
            this.onLineMove && this.onLineMove(this);
            this.hasChange = false;
            const dist = Math.abs(this.circle1.position.x - this.circle2.position.x);
            const line = new Line(this.canvas, {
                position: new Point(this.circle1.position.x, 0),
                to: new Point(this.circle1.position.x, this.height),
                color: this.color,
                lineWidth: 2,
            });
            const line2 = new Line(this.canvas, {
                position: new Point(this.circle2.position.x, 0),
                to: new Point(this.circle2.position.x, this.height),
                color: this.color,
                lineWidth: 2,
            });
            let minX = 0;
            let maxX = 0;
            if (this.circle1.position.x > this.circle2.position.x) {
                minX = this.circle2.position.x;
                maxX = this.circle1.position.x;
            } else {
                minX = this.circle1.position.x;
                maxX = this.circle2.position.x;
            }
            const maxNum = Math.floor((this.width - maxX) / dist);
            const minNum = Math.floor(minX / dist);
            for (let i = 0; i < maxNum; i++) {
                const line = new Line(this.canvas, {
                    position: new Point(maxX + dist * i, 0),
                    to: new Point(maxX  + dist * i, this.height),
                    color: this.color,
                    lineWidth: 2,
                });
                this.addChild(line);
            }
            for (let i = 0; i < minNum; i++) {
                const line = new Line(this.canvas, {
                    position: new Point(minX - dist * i, 0),
                    to: new Point(minX - dist * i, this.height),
                    color: this.color,
                    lineWidth: 2,
                });
                this.addChild(line);
            }
            this.addChild(line, line2, this.circle1, this.circle2);
        });
    }

    setSelected(isSelected) {
        this.circle1.visible = isSelected;
        this.circle2.visible = isSelected;
    }

    containsPoint (point) {
        let k = (this.circle1.position.y - this.circle2.position.y)
            / (this.circle1.position.x - this.circle2.position.x);
        let yDown = k * (point.x - this.circle2.position.x) + this.circle2.position.y - 10;
        let yUp = yDown + 20;
        if ((point.x - this.circle1.position.x) * (point.x - this.circle2.position.x) < 0 && (point.y - yDown) * (point.y - yUp) < 0) {
            return true;
        }
        return false;
    }
}
