import Layer from '../../core/Layer';
import Line from '../../base/Line';
import Text from '../../base/Text';
import Point from '../../core/Point';
import Circle from '../../base/Circle';
import Event from '../../event/Event';

export default class extends Layer {
    constructor(canvas, style) {
        super(canvas, style);
        this.uuid = style.uuid;
        this.onLineMove = style.onLineMove;
        this.onLineMoved = style.onLineMoved;
        this.content = style.content || '';
        this.circle = new Circle(this.canvas, {
            radius: 10,
            color: this.color,
            position: this.position,
        });
        let line = new Line(this.canvas, {
            position: new Point(0, this.circle.position.y),
            to: new Point(this.width, this.circle.position.y),
            color: this.color,
            lineWidth: 1,
        });
        let text = new Text(this.canvas, {
            font: '宋体',
            color: this.fontColor,
            size: this.fontSize,
            text: this.content,
            position: new Point(0, this.circle.position.y),
        });
        this.addChild(this.circle, line, text);
        this.circle.addEventListener(Event.EVENT_TOUCH_MOVE, (e) => {
            this.childs.splice(0, this.childs.length);
           this.circle.position = new Point(e.curPoint.x, e.curPoint.y);
           this.onLineMove && this.onLineMove(this);
            let line = new Line(this.canvas, {
                position: new Point(0, this.circle.position.y),
                to: new Point(this.width, this.circle.position.y),
                color: this.color,
                lineWidth: 1,
            });
            let text = new Text(this.canvas, {
                font: '宋体',
                color: this.fontColor,
                size: this.fontSize,
                text: this.content,
                position: new Point(0, this.circle.position.y),
            });
            this.addChild(this.circle, line, text);
            this.canvas.paint();
        });
        this.circle.addEventListener(Event.EVENT_TOUCH_END, (e) => {
            if (this.hasChange) {
                this.onLineMoved && this.onLineMoved(this);
            }
            this.hasChange = false;
        })
    }
    setSelected(isSelected) {
        this.circle.visible = isSelected;
    }

    containsPoint(point) {
        if (point.y < this.circle.position.y + 10 && point.y > this.circle.position.y + 10) {
            return true;
        }
        return false;
    }
}
