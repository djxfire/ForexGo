import Layer from '../../core/Layer';
import Text from '../../base/Text';
import Point from '../../core/Point';
import Line from '../../base/Line';
import Polygon from '../../base/Polygon';
import Circle from '../../base/Circle';
import Event from '../../event/Event';

export default class extends Layer {
    constructor(canvas, style) {
        super(canvas, style);
        this.uuid = style.uuid;
        this.onLineMove = style.onLineMove;
        this.onLineMoved = style.onLineMoved;
        this.hasChange = false;
        this.decimal = style.decimal || 4;
        this.maxValue = style.maxValue;
        this.minValue = style.minValue;
        this.mesures = style.mesures || [];
        this.circle1 = new Circle(this.canvas, {
            radius: 10,
            color: this.color,
            position: style.point1,
        });
        this.circle2 = new Circle(this.canvas, {
            radius: 10,
            color: this.color,
            position: style.point2,
        });
        let k = (this.circle1.position.y - this.circle2.position.y) / (this.circle1.position.x - this.circle2.position.x);
        let x1 = (this.height - this.circle1.position.y) / k + this.circle1.position.x;
        let x2 = (0 - this.circle1.position.y) / k + this.circle1.position.x;
        let line = new Line(this.canvas, {
            position: new Point(x2, 0),
            to: new Point(x1, this.height),
            color: this.color,
            lineWidth: 2,
        });
        this.addChild(line, this.circle1, this.circle2);
        this.mesures.sort();
        for(let i = 0; i < this.mesures.length; i++) {
            const minY = this.circle1.position.y > this.circle2.position.y ? this.circle2.position.y : this.circle1.position.y;
            const lineY = minY + this.mesures[i] * Math.abs(this.circle1.position.y - this.circle2.position.y)
            const lineX = (lineY - this.circle1.position.y) / k + this.circle1.position.x;
            let line = new Line(this.canvas, {
                position: new Point(lineX, lineY),
                to: new Point(this.width, lineY),
                color: this.color,
                lineWidth: 2,
            });
            let price = (this.maxValue - this.minValue) / Math.abs(this.circle1.position.y - this.circle2.position.y) * (lineY - minY) + this.minValue;
            let text = new Text(this.canvas, {
                font: '宋体',
                size: 20,
                color: this.color,
                text: `${this.mesures[i]}(${price.toFixed(this.decimal)})`,
                position: new Point(lineX, lineY),
            });
            this.addChild(line, text);
        }
        this.circle1.addEventListener(Event.EVENT_TOUCH_MOVE, (e) => {
            this.circle1.position = new Point(e.curPoint.x, e.curPoint.y);
            this.onLineMove && this.onLineMove(this);
            this.hasChange = true;
            this.childs.splice(0, this.childs.length);
            let k = (this.circle1.position.y - this.circle2.position.y) / (this.circle1.position.x - this.circle2.position.x);
            let x1 = (this.height - this.circle1.position.y) / k + this.circle1.position.x;
            let x2 = (0 - this.circle1.position.y) / k + this.circle1.position.x;
            let line = new Line(this.canvas, {
                position: new Point(x2, 0),
                to: new Point(x1, this.height),
                color: this.color,
                lineWidth: 2,
            });
            this.addChild(line, this.circle1, this.circle2);
            this.mesures.sort();
            for(let i = 0; i < this.mesures.length; i++) {
                const minY = this.circle1.position.y > this.circle2.position.y ? this.circle2.position.y : this.circle1.position.y;
                const lineY = minY + this.mesures[i] * Math.abs(this.circle1.position.y - this.circle2.position.y)
                const lineX = (lineY - this.circle1.position.y) / k + this.circle1.position.x;
                let line = new Line(this.canvas, {
                    position: new Point(lineX, lineY),
                    to: new Point(this.width, lineY),
                    color: this.color,
                    lineWidth: 2,
                });
                let price = (this.maxValue - this.minValue) / Math.abs(this.circle1.position.y - this.circle2.position.y) * (lineY - minY) + this.minValue;
                let text = new Text(this.canvas, {
                    font: '宋体',
                    size: 20,
                    color: this.color,
                    text: `${this.mesures[i]}(${price.toFixed(this.decimal)})`,
                    position: new Point(lineX, lineY),
                });
                this.addChild(line, text);
            }
            this.canvas.paint();
        });
        this.circle2.addEventListener(Event.EVENT_TOUCH_MOVE, (e) => {
            this.circle2.position = new Point(e.curPoint.x, e.curPoint.y);
            this.onLineMove && this.onLineMove(this);
            this.hasChange = true;
            this.childs.splice(0, this.childs.length);
            let k = (this.circle1.position.y - this.circle2.position.y) / (this.circle1.position.x - this.circle2.position.x);
            let x1 = (this.height - this.circle1.position.y) / k + this.circle1.position.x;
            let x2 = (0 - this.circle1.position.y) / k + this.circle1.position.x;
            let line = new Line(this.canvas, {
                position: new Point(x2, 0),
                to: new Point(x1, this.height),
                color: this.color,
                lineWidth: 2,
            });
            this.addChild(line, this.circle1, this.circle2);
            this.mesures.sort();
            for(let i = 0; i < this.mesures.length; i++) {
                const minY = this.circle1.position.y > this.circle2.position.y ? this.circle2.position.y : this.circle1.position.y;
                const lineY = minY + this.mesures[i] * Math.abs(this.circle1.position.y - this.circle2.position.y)
                const lineX = (lineY - this.circle1.position.y) / k + this.circle1.position.x;
                let line = new Line(this.canvas, {
                    position: new Point(lineX, lineY),
                    to: new Point(this.width, lineY),
                    color: this.color,
                    lineWidth: 2,
                });
                let price = (this.maxValue - this.minValue) / Math.abs(this.circle1.position.y - this.circle2.position.y) * (lineY - minY) + this.minValue;
                let text = new Text(this.canvas, {
                    font: '宋体',
                    size: 20,
                    color: this.color,
                    text: `${this.mesures[i]}(${price.toFixed(this.decimal)})`,
                    position: new Point(lineX, lineY),
                });
                this.addChild(line, text);
            }
            console.log('gold line', this);
            this.canvas.paint();
        });
        this.addEventListener(Event.EVENT_TOUCH_END, (e) => {
            if (this.hasChange) {
                this.onLineMoved && this.onLineMoved(this);
            }
            this.hasChange = false;
        });
    }

    setSelected(isSelected) {
        this.circle1.visible = isSelected;
        this.circle2.visible = isSelected;
    }

    containsPoint (point) {
        let k = (this.circle1.position.y - this.circle2.position.y)
            / (this.circle1.position.x - this.circle2.position.x);
        let yDown = k * (point.x - this.circle2.position.x) + this.circle2.position.y - 10;
        let yUp = yDown + 20;
        if ((point.x - this.circle1.position.x)
            * (point.x - this.circle2.position.x) < 0 && (point.y - yDown) * (point.y - yUp) < 0) {
            return true;
        }
        return false;
    }
}
