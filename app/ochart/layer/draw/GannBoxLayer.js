import Layer from '../../core/Layer';
import Point from '../../core/Point';
import Line from '../../base/Line';
import Circle from '../../base/Circle';
import Text from '../../base/Text';
import Event from '../../event/Event';

export default class extends Layer {
    constructor(canvas, style) {
        super(canvas, style);
        this.uuid = style.uuid;
        this.onLineMove = style.onLineMove;
        this.onLineMoved = style.onLineMoved;
        this.onLineSelected = style.onLineSelected;
        this.hasChange = false;
        this.circle1 = new Circle(this.canvas, {
            radius: 10,
            color: this.color,
            position: this.position,
        });
        this.circle2 = new Circle(this.canvas, {
            radius: 10,
            color: this.color,
            position: style.point2,
        });
        let minX = this.circle1.position.x > this.circle2.position.x
            ? this.circle2.position.x : this.circle1.position.x;
        let maxX = this.circle1.position.x > this.circle2.position.x
            ? this.circle1.position.x : this.circle1.position.x;
        let minY = this.circle1.position.y > this.circle2.position.y
            ? this.circle2.position.y : this.circle1.position.y;
        let maxY = this.circle1.position.y > this.circle2.position.y
            ? this.circle1.position.y : this.circle2.position.y;
        const yDist = maxY - minY;
        const xDist = maxX - minX;
        [0, 0.25, 0.382, 0.5, 0.618, 1].forEach((vo) => {
           let line = new Line(this.canvas, {
               position: new Point(minX, minY + vo * yDist),
               to: new Point(maxX, minY + vo * yDist),
               color: this.color,
           });
           let txt = new Text(this.canvas, {
               font: '宋体',
               size: 16,
               color: this.color,
               text: vo,
           });
           txt.setPosition(minX - txt.width, minY + vo * yDist - txt.height / 2);
           let line1 = new Line(this.canvas, {
               position: new Point(minX + vo * xDist, minY),
               to: new Point(minX + vo * xDist, maxY),
               color: this.color,
           });
            let txt1 = new Text(this.canvas, {
                font: '宋体',
                size: 16,
                color: this.color,
                text: vo,
            });
            txt1.setPosition(minX + vo * xDist - txt1.width / 2, maxY);
           this.addChild(line, line1, txt, txt1);
        });
        this.circle1.addEventListener(Event.EVENT_TOUCH_MOVE, (e) => {
           this.circle1.position = new Point(e.curPoint.x, e.curPoint.y);
           this.childs.splice(0, this.childs.length);
           this.onLineMove && this.onLineMove(this);
           this.hasChange = true;
            let minX = this.circle1.position.x > this.circle2.position.x
                ? this.circle2.position.x : this.circle1.position.x;
            let maxX = this.circle1.position.x > this.circle2.position.x
                ? this.circle1.position.x : this.circle1.position.x;
            let minY = this.circle1.position.y > this.circle2.position.y
                ? this.circle2.position.y : this.circle1.position.y;
            let maxY = this.circle1.position.y > this.circle2.position.y
                ? this.circle1.position.y : this.circle2.position.y;
            const yDist = maxY - minY;
            const xDist = maxX - minX;
            [0, 0.25, 0.382, 0.5, 0.618, 1].forEach((vo) => {
                let line = new Line(this.canvas, {
                    position: new Point(minX, minY + vo * yDist),
                    to: new Point(maxX, minY + vo * yDist),
                    color: this.color,
                });
                let txt = new Text(this.canvas, {
                    font: '宋体',
                    size: 16,
                    color: this.color,
                    text: vo,
                });
                txt.setPosition(minX - txt.width, minY + vo * yDist - txt.height / 2);
                let line1 = new Line(this.canvas, {
                    position: new Point(minX + vo * xDist, minY),
                    to: new Point(minX + vo * xDist, maxY),
                    color: this.color,
                });
                let txt1 = new Text(this.canvas, {
                    font: '宋体',
                    size: 16,
                    color: this.color,
                    text: vo,
                });
                txt1.setPosition(minX + vo * xDist - txt1.width / 2, maxY);
                this.addChild(line, line1, txt, txt1);
            });
        });
        this.circle2.addEventListener(Event.EVENT_TOUCH_MOVE, (e) => {
            this.circle2.position = new Point(e.curPoint.x, e.curPoint.y);
            this.childs.splice(0, this.childs.length);
            this.onLineMove && this.onLineMove(this);
            this.hasChange = true;
            let minX = this.circle1.position.x > this.circle2.position.x
                ? this.circle2.position.x : this.circle1.position.x;
            let maxX = this.circle1.position.x > this.circle2.position.x
                ? this.circle1.position.x : this.circle1.position.x;
            let minY = this.circle1.position.y > this.circle2.position.y
                ? this.circle2.position.y : this.circle1.position.y;
            let maxY = this.circle1.position.y > this.circle2.position.y
                ? this.circle1.position.y : this.circle2.position.y;
            const yDist = maxY - minY;
            const xDist = maxX - minX;
            [0, 0.25, 0.382, 0.5, 0.618, 1].forEach((vo) => {
                let line = new Line(this.canvas, {
                    position: new Point(minX, minY + vo * yDist),
                    to: new Point(maxX, minY + vo * yDist),
                    color: this.color,
                });
                let txt = new Text(this.canvas, {
                    font: '宋体',
                    size: 16,
                    color: this.color,
                    text: vo,
                });
                txt.setPosition(minX - txt.width, minY + vo * yDist - txt.height / 2);
                let line1 = new Line(this.canvas, {
                    position: new Point(minX + vo * xDist, minY),
                    to: new Point(minX + vo * xDist, maxY),
                    color: this.color,
                });
                let txt1 = new Text(this.canvas, {
                    font: '宋体',
                    size: 16,
                    color: this.color,
                    text: vo,
                });
                txt1.setPosition(minX + vo * xDist - txt1.width / 2, maxY);
                this.addChild(line, line1, txt, txt1);
            });
        });
        this.circle1.addEventListener(Event.EVENT_TOUCH_END, (e) => {
            if (this.hasChange) {
                this.onLineMoved && this.onLineMoved(this);
            }
            this.hasChange = false;
        });
        this.circle2.addEventListener(Event.EVENT_TOUCH_END, (e) => {
            if (this.hasChange) {
                this.onLineMoved && this.onLineMoved(this);
            }
            this.hasChange = false;
        });
        console.log('gann==>', this);
    }

    setSelected(isSelected) {
        this.circle1.visible = isSelected;
        this.circle2.visible = isSelected;
    }

    containsPoint (point) {
        let minX = this.circle1.position.x > this.circle2.position.x
            ? this.circle2.position.x : this.circle1.position.x;
        let maxX = this.circle1.position.x > this.circle2.position.x
            ? this.circle1.position.x : this.circle1.position.x;
        let minY = this.circle1.position.y > this.circle2.position.y
            ? this.circle2.position.y : this.circle1.position.y;
        let maxY = this.circle1.position.y > this.circle2.position.y
            ? this.circle1.position.y : this.circle2.position.y;
        if (point.x < maxX
            && point.x > minX
            && point.y > minY
            && point.y < maxY
        ) {
            return true;
        }
        return false;
    }
}
