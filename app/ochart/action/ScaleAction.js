import Action from '../core/Action';

export default class ScaleAction extends Action{
  constructor(_duration, _scaleX, _scaleY) {
    super(_duration, 10)
    this.scaleX = _scaleX
    this.scaleY = _scaleY
    this.detaX = (this.scaleX - 1)/this.duration
    this.detaY = (this.scaleY - 1)/this.duration
  }
  update(_sprite) {
    _sprite.scaleX += this.detaX
    _sprite.scaleY += this.detaY;
    _sprite.canvas.paint();
  }

  onStop(_sprite) {
    _sprite.scaleX = 1;
    _sprite.scaleY = 1;
  }
}
