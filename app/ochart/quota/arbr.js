import $V from './$V';

export default function arbr(open, high, low, close, length = 26) {
    let ar = [];
    let br = [];
    for (let i = 0; i <= length; i++) {
        ar[i] = 100;
        br[i] = 100;
    }
    for(let i = length + 1; i < open.length; i++) {
        let ho = 0, ol = 0, hc = 0, cl = 0;
        for (let j = i - length; j < i; j++) {
            ho += high[j] - open[j];
            ol += open[j] - low[j];
            hc += high[j] - close[j - 1];
            cl += close[j - 1] - low[j];
        }
        ar[i] = ho / ol * 100;
        br[i] = hc / cl * 100;
    }
    return { ar, br };
}
