import $V from './$V';

export default function nvi(close, volume) {
    let NVIValue = $V.init(close.length,0);
    for(var i = 0;i < close.length;i++){
        if( i == 0){
            NVIValue[i] = 100;
        }
        if( i > 0){
            if(volume[i] >= volume[i-1]){
                NVIValue[i] = NVIValue[i - 1];
            }else{
                NVIValue[i] = NVIValue[i - 1] + (close[i] - close[i-1])/close[i-1]*NVIValue[i-1];

            }
        }
    }
    return NVIValue;
}
