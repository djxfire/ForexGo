import $V from './$V';
import ma from './ma';

export default function atr(high, low, close, length = 14) {
    let trvalue = $V.init(high.length, 0);
    let tmp  = [];
    tmp.push($V.sub($V.subv(high, 1, high.length), $V.subv(low, 1, low.length)));
    tmp.push($V.abs($V.sub($V.subv(high, 1, high.length), $V.subv(close, 0, close.length - 1))));
    tmp.push($V.abs($V.sub($V.subv(low, 1, low.length), $V.subv(close, 0, close.length - 1))));
    trvalue = $V.copy_by_index(trvalue, 1, $V.maximum(tmp));
    return ma(trvalue, length);
}
