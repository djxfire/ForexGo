import $V from './$V';
import ema from './ema';

export default  function cv(high, low, length) {
    let cvValue = $V.init(high.length, 0);
    const hl = [];
    for (let i = 0; i < high.length; i++) {
        hl.push(high[i] - low[i]);
    }
    let hiloma = ema(hl, length);
    for (let i = length; i < hiloma.length; i++) {
        if (hiloma[i - length] === 0) {
            cvValue[i] = 0;
        } else {
            cvValue[i] = (hiloma[i] - hiloma[i - length]) / hiloma[i - length] * 100;
        }

    }
    return cvValue;
}
