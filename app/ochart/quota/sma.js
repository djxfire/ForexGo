import $V from './$V';

export default function sma(data, length, M) {
  let result = $V.init(data.length, 0);
  let K = M / (length + 1);
  result[0] = data[0]
  for (let i = 1; i < data.length; i++) {
    result[i] += data[i] * K + result[i - 1] * (1 - K);
  }
  return result;
}
