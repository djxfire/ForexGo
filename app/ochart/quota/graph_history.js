import ad from './ad';
import arbr from './arbr';
import aroon from './aroon';
import bias from './bias';
import cci from './cci';
import cmo from './cmo';
import ma from './ma';
import cr from './cr';
import dpo from './dpo';
import emv from './emv';
import forceIndex from './forceIndex';
import ema from './ema';
import kdj from './kdj';
import mfi from './mfi';
import mike from './mike';
import nvi from './nvi';
import psy from './psy';
import wms from './wms';
import $V from './$V';
export function quota_graph_history(bars) {
	let _ad = ad(bars.high, bars.low, bars.close, bars.volume)
	let __ad = [-1, -1]
	for(let i = 2;i < _ad.length;i++) {
		if(_ad[i] > _ad[i-1]
			&& _ad[i-1] > _ad[i-2]) {
			__ad.push(0)
		}else if(_ad[i] < _ad[i-1]
		&& _ad[i-1] < _ad[i-2]) {
			__ad.push(2)
		}else {
			__ad.push(-1)
		}
	}

	let chaikin = $V.sub(ma(_ad, 6), ma(_ad, 14))
	let __chaikin = [-1]
	for(let i = 1;i < chaikin.length;i++){
		if(chaikin[i] >= 0
			&& chaikin[i-1] <= 0){
			__chaikin.push(0)
		}else if(chaikin[i] <= 0
			&& chaikin[i-1] >= 0) {
			__chaikin.push(2)
		}else if(chaikin[i] >= 0
			&& chaikin[i-1] >= 0) {
			__chaikin.push(3)
		}else if(chaikin[i] <= 0
			&& chaikin[i-1] <= 0) {
			__chaikin.push(4)
		}else {
			__chaikin.push(-1)
		}
	}

	let { ar, br } = arbr(bars.open, bars.high, bars.low, bars.close,16);
	let _ar = ar, _br = br;
	let __ar = []
	for(let i = 0;i < _ar.length;i++) {
		if(_ar[i] < 60 && _ar[i] > 40) {
			__ar.push(1)
		}else if(_ar[i] > 75) {
			__ar.push(2)
		}else if(_ar[i] < 30) {
			__ar.push(0)
		}else {
			__ar.push(-1)
		}
	}
	let __ar1 = [-1,-1,-1,-1]
	for(let i = 4;i < _ar.length;i++) {
		if(_ar[i] < 35
			&& _ar[i-1] < _ar[i]
			&& _ar[i-2] < _ar[i-1]
			&& _ar[i-3] > _ar[i-2]
			&& _ar[i-4] > _ar[i-3]
			&& _ar[i-4] < 35
		){
			__ar1.push(0)
		}else if(_ar[i] > 75
			&& _ar[i-1] > _ar[i]
			&& _ar[i-2] > _ar[i-1]
			&& _ar[i-3] < _ar[i-2]
			&& _ar[i-4] < _ar[i-3]
			&& _ar[i-4] > 75){
			__ar1.push(2)
		}else {
			__ar1.push(-1)
		}
	}
	let __ar2 = [-1, -1]
	for(let i = 2;i < _ar.length;i++) {
		if(_ar[i] > _ar[i-1]
			&& _ar[i-1] > _ar[i-2]
			&& _br[i] > _br[i-1]
			&& _br[i-1] > _br[i-2]
			&& _br[i] < 50 && _ar[i] < 50) {
			__ar2.push(0)
		}else if(_ar[i] < _ar[i-1]
			&& _ar[i-1] < _ar[i-2]
			&& _br[i] < _br[i-1]
			&& _br[i-1] < _br[i-2]
			&& _br[i] > 50 && _ar[i] > 50){
			__ar2.push(2)
		}else {
			__ar2.push(-1)
		}
	}
	let __ar3 = [-1, -1]
	for(let i = 2;i < _ar.length;i++) {
		if((_br[i] - _br[i-2])> 15
			&& _ar[i] < 75
			&& _ar[i] - _ar[i-2] < 10
			&& bars.close[i] < bars.close[i-1]) {
			__ar3.push(0)
		}else if((_br[i] - _br[i-2])> 15
			&& _ar[i] > 65
			&& _ar[i] - _ar[i-2] < -10){
			__ar3.push(2)
		}else {
			__ar3.push(-1)
		}
	}
	let __ar4 = [-1]
	for(let i = 1;i < _ar.length;i++) {
		if(_br[i] > _ar[i]
			&& _br[i-1] < _ar[i-1]
			&& _ar[i] > _ar[i-1]
			&& _br[i] > _br[i-1]
			&& _ar[i] < 50 && _br[i] < 50
		){
			__ar4.push(0)
		}else if(_br[i] < _ar[i]
			&& _br[i-1] > _ar[i-1]
			&& _ar[i] < _ar[i-1]
			&& _br[i] < _br[i-1]
			&& _ar[i] > 50 && _br[i] > 50){
			__ar4.push(2)
		}else {
			__ar4.push(-1)
		}
	}

	let {uparoon, downaroon} = aroon(bars.high, bars.low, 20);
	let _aroon_up = uparoon, _aroon_down = downaroon;
	let __aroon = [-1]
	for(let i = 1;i < _aroon_up.length;i++) {
		if(_aroon_up[i] > 60
		&& _aroon_up[i-1] < 60
		&& _aroon_up[i] > _aroon_down[i]) {
			__aroon.push(0)
		}else if(_aroon_up[i] < 50
		&& _aroon_up[i-1]> 50
		&& _aroon_up[i] < _aroon_down[i]) {
			__aroon.push(2)
		}else if(_aroon_down[i] > 60
		&& _aroon_down[i-1] < 60
		&& _aroon_up[i] < _aroon_down[i]){
			__aroon.push(3)
		}else if(_aroon_down[i] < 50
		&& _aroon_down[i-1] > 50
		&& _aroon_upi[i] > _aroon_down[i]){
			__aroon.push(4)
		}else if(_aroon_up[i] > 70
			&& _aroon_up[_aroon_up.length - 2] > 70
			&& _aroon_down[_aroon_down.length - 1] < 50
			&& _aroon_down[_aroon_down.length - 2] < 50
		){
			__aroon.push(5)
		}else if(_aroon_up[i] < 50
			&& _aroon_up[i-1] < 50
			&& _aroon_down[i] > 70
			&& _aroon_down[i-1] > 70){
			__aroon.push(6)
		}else if(_aroon_up[i] < 50
			&& _aroon_up[i-1] < 50
			&& _aroon_down[i] < 50
			&& _aroon_down[i-1] < 50){
			__aroon.push(1)
		}else {
			__aroon.push(-1)
		}
	}
	let bias6 = bias(bars.close, 6),
		bias12 = bias(bars.close, 12),
		bias24 = bias(bars.close, 24)
	let __bias = []
	for(let i = 0;i < bias6.length;i++) {
		if(bias6[i] < -5
		|| bias12[i] < -5.5
		|| bias24[i] < -8) {
			__bias.push(0)
		}else if(bias6[i] > 5
		|| bias12[i] > 6
		|| bias24[i] > 9){
			__bias.push(2)
		}else {
			__bias.push(-1)
		}
	}
	let _cci = cci(bars.high, bars.low, bars.close, 14)
	let __cci = [-1]
	for(let i = 1;i < _cci.length;i++) {
		if(_cci[i] > 100 && _cci[i-1] < 100) {
			__cci.push(0)
		}else if(_cci[i] < 100 && _cci[i-1] > 100) {
			__cci.push(2)
		}else if(_cci[i] > -100 && _cci[i-1] < -100) {
			__cci.push(3)
		}else if(_cci[i] < -100 && _cci[i-1] > -100){
			__cci.push(4)
		}else if(_cci[i] > _cci[i-1]
			&& _cci[i-1] > 100) {
			__cci.push(5)
		}else if(_cci[i] < _cci[i-1]
			&& _cci[i] > 100) {
			__cci.push(6)
		}else if(_cci[i] < _cci[i-1]
			&& _cci[i-1] < -100) {
			__cci.push(7)
		}else if(_cci[i] > _cci[i-1]
			&& _cci[i] < -100) {
			__cci.push(8)
		}else if(_cci[i] < 100 && _cci[i] > -100) {
			__cci.push(1)
		}else {
			__cci.push(-1)
		}
	}
	let _cmo = cmo(bars.close, 14)
	let _cmo_mean = ma(_cmo, 6)
	let __cmo = []
	for(let i = 0;i < _cmo.length;i++) {
		if(_cmo[i] < -50) {
			__cmo.push(0)
		}else if(_cmo[i] > 50) {
			__cmo.push(1)
		}else if(_cmo[i] > 0 && _cmo[i] < 0) {
			__cmo.push(2)
		}else if(_cmo[i] < 50 && _cmo[i] > 0) {
			__cmo.push(3)
		}else if(_cmo[i] > -50 && _cmo[i] < 0) {
			__cmo.push(4)
		}else {
			__cmo.push(-1)
		}
	}
	let __cmo1 = [-1]
	for(let i = 1;i < _cmo.length;i++) {
		if(_cmo[i] > _cmo_mean[i]
			&& _cmo[i-1] < _cmo_mean[i-1]) {
			__cmo1.push(0)
		}else if(_cmo[i] < _cmo_mean[i]
			&& _cmo[i-1] > _cmo_mean[i-1]) {
			__cmo1.push(1)
		}else if(_cmo[i] > _cmo_mean[i] && _cmo[i-1] > _cmo_mean[i-1]){
			__cmo1.push(2)
		}else if(_cmo[i] < _cmo_mean[i] && _cmo[i-1] < _cmo_mean[i-1]){
			__cmo1.push(3)
		}else {
			__cmo1.push(-1)
		}
	}
	let _cr = cr(bars.high, bars.low, bars.close, 5)
	let __cr = [-1,-1]
	for(let i = 2;i < _cr.length;i++) {
		if(_cr[i] < 75
			&& _cr[i] > _cr[i-1]
			&& _cr[i-1] < _cr[i-2]) {
			__cr.push(0)
		}else if(_cr[i] > 150
			&& _cr[i] < _cr[i-1]
			&& _cr[i-1] > _cr[i-2]) {
			__cr.push(1)
		}else if(_cr[i] > _cr[i-1]
			&& _cr[i] > 125
			&& _cr[i] < 300) {
			__cr.push(2)
		}else if(_cr[i] < _cr[i-1]
			&& _cr[i] < 75
			&& _cr[i] > 40) {
			__cr.push(3)
		}else if(_cr[i] < 40) {
			__cr.push(4)
		}else if(_cr[i] > 300) {
			__cr.push(5)
		}else if(_cr[i] < 125 && _cr[i] > 75) {
			__cr.push(6)
		}else {
			__cr.push(-1)
		}
	}
	let _dpo = dpo(bars.close, 20)
	let __dpo = [-1,-1]
	for(let i = 2;i < _dpo.length;i++) {
		if(_dpo[i] > 0 && _dpo[i-1] < 0) {
			__dpo.push(0)
		}else if(_dpo[i] > _dpo[i-1]
			&& _dpo[i-1] < _dpo[i-2]
			&& _dpo[i] < 0) {
			__dpo.push(1)
		}else if(_dpo[i] < _dpo[i-1]
			&& _dpo[i-1] > _dpo[i-2]
			&& _dpo[i] > 0) {
			__dpo.push(2)
		}else if(_dpo[i] < 0 && _dpo[i-1] > 0) {
			__dpo.push(3)
		}else if(_dpo[i] > 0) {
			__dpo.push(4)
		}else if(_dpo[i] < 0) {
			__dpo.push(5)
		}else {
			__dpo.push(-1)
		}
	}

	let { EMVValue, MAEMVVAlue} = emv(bars.high, bars.low, bars.volume, 5);
	let _emv = EMVValue;
	let __emv = [-1]
	for(let i = 1;i < _emv.length;i++) {
		if(_emv[i] > 0 && _emv[i-1] < 0) {
			__emv.push(1)
		}else if(_emv[i] < 0 && _emv[i-1] > 0) {
			__emv.push(2)
		}else if(_emv[i] > 0){
			__emv.push(3)
		}else if(_emv[i] < 0) {
			__emv.push(4)
		}else {
			__emv.push(-1)
		}
	}

	let _forceIndex = forceIndex(bars.close, bars.volume);
	let _forceIndex_ema = ema(_forceIndex, 2)
	let __forceIndex = [-1]
	for(let i = 1;i < _forceIndex.length;i++) {
		if(_forceIndex[i] > _forceIndex_ema[i-1]
		&& _forceIndex[i-1] < _forceIndex_ema[i]) {
			__forceIndex.push(0)
		}else if(_forceIndex[i] < _forceIndex_ema[i]
		&& _forceIndex[i-1] > _forceIndex_ema[i-1]){
			__forceIndex.push(1)
		}else if(_forceIndex[i] > _forceIndex_ema[i]) {
			__forceIndex.push(2)
		}else if(_forceIndex[i] < _forceIndex_ema[i]){
			__forceIndex.push(3)
		}else {
			__forceIndex.push(-1)
		}
	}

	let { KValue, DValue, JValue} = kdj(bars.high, bars.low, bars.close, 5,3,3,3);
	let  K_Value = KValue, D_Value = DValue, J_Value = JValue;
	let __kdj = [-1]
	for(let i = 1;i < K_Value.length;i++) {
		if(K_Value[i] > 80 && D_Value[i] > 80) {
			__kdj.push(0)
		}else if(K_Value[i] < 20 && D_Value[i] < 20){
			__kdj.push(1)
		}else if(K_Value[i] < D_Value[i]
			&& K_Value[i-1] > D_Value[i-1]) {
			__kdj.push(2)
		}else if(K_Value[i] > D_Value[i]
			&& K_Value[i-1] < D_Value[i-1]) {
			__kdj.push(3)
		}else if(J_Value[i] > 100) {
			__kdj.push(4)
		}else if(J_Value[i] < 0) {
			__kdj.push(5)
		}else if(K_Value[i] > D_Value[i]
			&& K_Value[i] - D_Value[i] < 10
			&& K_Value[i] - D_Value[i] < K_Value[i-1] - D_Value[i-1]) {
			__kdj.push(6)
		}else if(K_Value[i] < D_Value[i]
			&& D_Value[i] - K_Value[i] < 6
			&& D_Value[i] - K_Value[i] < D_Value[i-1] - K_Value[i-1]){
				__kdj.push(7)
		}else {
			__kdj.push(-1)
		}
	}

	let _mfi = mfi(bars.high, bars.low, bars.close, bars.volume, 5)
	let __mfi = [-1]
	for(let i = 1;i < _mfi.length;i++) {
		if(_mfi[i] > 80){
			__mfi.push(0)
		}else if(_mfi[i] < 20) {
			__mfi.push(1)
		}else if(_mfi[i] < 80 && _mfi[i-1] > 80) {
			__mfi.push(2)
		}else if(_mfi[i] > 20 && _mfi[i-1] < 20) {
			__mfi.push(3)
		}else if(_mfi[i] > _mfi[i-1]
			&& _mfi[i] > 50) {
			__mfi.push(4)
		}else if(_mfi[i] < _mfi[i-1]) {
			__mfi.push(5)
		}else {
			__mfi.push(-1)
		}
	}

	let _nvi = nvi(bars.close, bars.volume)
	let _nvi_ma = ma(_nvi, 14)
	let __nvi = [-1]
	for(let i = 1;i < _nvi.length;i++) {
		if(_nvi[i] > _nvi_ma[i]
		&& _nvi[i-1] < _nvi_ma[i-1]) {
			__nvi.push(0)
		}else if(_nvi[i] < _nvi_ma[i]
		&& _nvi[i-1] > _nvi_ma[i-1]) {
			__nvi.push(1)
		}else if(_nvi[i] > _nvi_ma[i]) {
			__nvi.push(2)
		}else if(_nvi[i] < _nvi_ma[i]) {
			__nvi.push(3)
		}else {
			__nvi.push(-1)
		}
	}

	let _psy = psy(bars.close, 6)
	let _psy_ma = ma(_psy, 6)
	let __psy = [-1]
	for(let i = 1;i < _psy.length;i++) {
		if(_psy_ma[i] > 75) {
			__psy.push(0)
		}else if(_psy[i] < 25) {
			__psy.push(1)
		}else if(_psy[i] > _psy_ma[i]
			&& _psy[i-1] < _psy_ma[i-1]) {
			__psy.push(2)
		}else if(_psy[i] < _psy_ma[i]
			&& _psy[i-1] > _psy_ma[i-1]){
			__psy.push(3)
		}else if(_psy[i] > 50) {
			__psy.push(4)
		}else if(_psy[i] < 50) {
			__psy.push(5)
		}else {
			__psy.push(-1)
		}
	}

	let _wms = wms(bars.high, bars.low, bars.close, 6)
	let __wms = [-1]
	for(let i = 1;i < _wms.length;i++) {
		if(_wms[i] > 80 && _wms[i-1] < 80){
			__wms.push(0)
		}else if(_wms[i] < 20 && _wms[i-1] > 20){
			__wms.push(1)
		}else if(_wms[i] < _wms[i-1]
			&& _wms[i] > 20) {
			__wms.push(2)
		}else if(_wms[i] > _wms[i-1]
			&& _wms[i] < 80) {
			__wms.push(3)
		}else {
			__wms.push(-1)
		}
	}
	let tt = []
	for(let i = 0;i < bars.close.length;i++) {
		tt.push([__ad[i], __chaikin[i], __ar[i], __ar1[i], __ar2[i], __ar3[i], __ar4[i], __aroon[i],
			__bias[i], __cci[i], __cmo[i], __cmo1[i], __cr[i], __dpo[i], __emv[i], __kdj[i], __mfi[i],
			__nvi[i], __psy[i], __wms[i]])
	}
	return tt

}

export function daily_to_be_label(margin){
	let label = []
	for(let i = 0;i < margin.length;i++) {
		if(margin[i] > 0.5 && margin[i] < 2) {
			label.push('z1')
		}else if(margin[i] >= 2 && margin[i] < 5) {
			label.push('z2')
		}else if(margin[i] >= 5) {
			label.push('z3')
		}else if(margin[i] <= 0.5 && margin[i] >= -0.5) {
			label.push('z4')
		}else if(margin[i] < -0.5 && margin[i] > -2) {
			label.push('z5')
		}else if(margin[i] <= -2 && margin[i] > -5) {
			label.push('z6')
		}else if(margin[i] <= -5) {
			label.push('z7')
		}else {
			label.push('x')
		}
	}
	return label
}
export function weekly_to_be_label(margin) {
  let label = []
  for(let i = 0;i < margin.length;i++) {
    if(margin[i] > 1 && margin[i] < 3) {
      label.push('z1')
    }else if(margin[i] >= 3 && margin[i] < 10) {
      label.push('z2')
    }else if(margin[i] >= 10) {
      label.push('z3')
    }else if(margin[i] <= 1 && margin[i] >= -1) {
      label.push('z4')
    }else if(margin[i] < -1 && margin[i] > -3) {
      label.push('z5')
    }else if(margin[i] <= -3 && margin[i] > -10) {
      label.push('z6')
    }else if(margin[i] <= -10) {
      label.push('z7')
    }else {
      label.push('x')
    }
  }
  return label
}
export function monthly_to_be_label(margin) {
  let label = []
  for(let i = 0;i < margin.length;i++) {
    if(margin[i] > 2 && margin[i] < 5) {
      label.push('z1')
    }else if(margin[i] >= 5 && margin[i] < 15) {
      label.push('z2')
    }else if(margin[i] >= 15) {
      label.push('z3')
    }else if(margin[i] <= 2 && margin[i] >= -2) {
      label.push('z4')
    }else if(margin[i] < -2 && margin[i] > -5) {
      label.push('z5')
    }else if(margin[i] <= -5 && margin[i] > -15) {
      label.push('z6')
    }else if(margin[i] <= -15) {
      label.push('z7')
    }else {
      label.push('x')
    }
  }
  return label
}

export function dapan_to_be_label(margin){
	let label = []
	for(let i = 0;i < margin.length;i++) {

		if (margin[i] >= 2) {
			label.push('z1');
		} else if(margin[i] >= 1.3 && margin[i] < 2) {
			label.push('z2');
		} else if(margin[i] >= 0.8 && margin[i] < 1.3) {
			label.push('z3');
		} else if(margin[i] >= 0.5 && margin[i] < 0.8) {
			label.push('z4');
		} else if(margin[i] >= 0.3 && margin[i] < 0.5) {
			label.push('z5');
		} else if(margin[i] >= 0.1 && margin[i] < 0.3) {
			label.push('z6');
		} else if (margin[i] > 0 && margin[i] < 0.1) {
			label.push('z7');
		} else if (margin[i] === 0) {
			label.push('z8');
		} else if(margin[i] >= -0.1 && margin[i] < 0) {
			label.push('z9');
		} else if(margin[i] >= - 0.3 && margin[i] < -0.1){
			label.push('z10');
		} else if(margin[i] >= -0.5 && margin[i] < -0.3) {
			label.push('z11');
		} else if(margin[i] >= -0.8 && margin[i] < -0.5) {
			label.push('z12');
		} else if(margin[i] >= -1.3 && margin[i] < -0.8) {
			label.push('z13');
		} else if(margin[i] >= -2 && margin[i] < -1.3) {
			label.push('z14');
		} else if(margin[i] < -2) {
			label.push('z15');
		} else {
			console.log(margin[i]);
			label.push('x');
		}
	}
	return label
}
