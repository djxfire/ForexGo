import $V from './$V';

export default function obv(price, volume) {
    let OBVValue = [];
    for(var i = 0;i < price.length;i++){
        if(i == 0){
            OBVValue[i] = volume[i];
        }else{
            if(parseFloat(price[i]) > parseFloat(price[i - 1])){
                OBVValue[i] = OBVValue[i-1] + parseFloat(volume[i]);
            }else if(parseFloat(price[i]) < parseFloat(price[i-1])){
                OBVValue[i] = OBVValue[i-1] - parseFloat(volume[i]);
            }else if(parseFloat(price[i]) == parseFloat(price[i-1])){
                OBVValue[i] = OBVValue[i-1];
            }
        }
    }
    return OBVValue;
}
