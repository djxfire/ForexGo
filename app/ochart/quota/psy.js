import $V from './$V';

export default function psy(price, length) {
    let PSYValue = [];
    for(let i = 0;i < price.length;i++){
        if (i <= length) {
            PSYValue[i] = 50;
        }
        let sum = 0;
        for (let j = i - length + 1; j < i; j++) {
            sum += price[j] > price[j - 1] ? 1 : 0;
        }
        PSYValue[i] = sum / length * 100;
    }
    return PSYValue;
}
