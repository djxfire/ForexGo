import ma from './ma';
export default function forceIndex(price, volume, N) {
    const maValue = ma(price, N);
    let ForceIndexValue = [0];
    for (let i = 1; i < price.length; i++) {
        ForceIndexValue[i] = volume[i] * (maValue[i] - maValue[i - 1]);
    }
    return ForceIndexValue;
}
