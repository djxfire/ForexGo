import $V from './$V';

export default function cmo(price, length) {
    let cmoValue = $V.init(price.length, 0);
    let diffOfPrice = $V.init(price.length, 0);
    diffOfPrice = $V.copy_by_range(diffOfPrice, $V.arange(1, price.length - 1), $V.sub($V.subv(price, 1, price.length), $V.subv(price, 0, price.length - 1)));
    for(let i = length; i < price.length; i++) {
        let tmp = $V.subv(diffOfPrice, i - length, i);
        cmoValue[i] = $V.sum(tmp) / $V.sum($V.abs(tmp)) * 100;
    }
    return cmoValue;
}
