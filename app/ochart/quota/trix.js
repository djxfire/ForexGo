
import ma from './ma';

export default function trix(price, length, smoothLength) {
    let trixValue = [0];
    const tr = ma(ma(ma(price,length),length),length);
    for (let i = 1; i < tr.length; i++) {
        trixValue[i] = (tr[i] - tr[i - 1]) / tr[i - 1] * 100;
    }
    const matrixValue = ma(trixValue,smoothLength);
    return { trixValue, matrixValue };
}
