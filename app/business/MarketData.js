/*
* @Date: 2021/3/6
* @Author: XueChengwu <xuechengwu@erayt.com>
* @Copyright: 2015-2019 Erayt, Inc.
* @Description: If you have some questions, please contact: xuechengwu@erayt.com.
*/

export default {
  url: 'local/api/market/data',
  meta: {
    data() {
      const { page = 1 } = this.state;
      return {
        p: page,
      };
    }
  },
  resolve(res) {
    if (Number(res.code) === 0) {
      return {
        data: res.data.data,
        last_page: res.data.total,
      };
    }
    return {
      data: [],
      last_page: 0,
    };
  }
}
