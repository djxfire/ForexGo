/*
* @Date: 2021/2/27
* @Author: XueChengwu <xuechengwu@erayt.com>
* @Copyright: 2015-2019 Erayt, Inc.
* @Description: If you have some questions, please contact: xuechengwu@erayt.com.
*/

export default {
  url() {
    const { type } = this.props;
    if (type === 0) {
      return 'local/api/post/list';
    }
    if (type === 1) {
      return 'local/api/post/hots';
    }
    if (type === 2) {
      return 'local/api/post/stars';
    }
  },
  meta: {
    data() {
      const { page = 1, currentDate } = this.state;
      return {
        page,
        currentDate,
      };
    }
  },
  resolve: (res) => {
    if (Number(res.code) === 0) {
      return {
        data: res.data.posts.data,
        cur: res.data.posts.cur,
        total: res.data.posts.total,
      };
    }
    return {
      data: [],
      cur: 0,
      total: 0,
    }
  }
}
