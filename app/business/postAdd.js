/*
* @Date: 2021/2/28
* @Author: XueChengwu <xuechengwu@erayt.com>
* @Copyright: 2015-2019 Erayt, Inc.
* @Description: If you have some questions, please contact: xuechengwu@erayt.com.
*/

import Toast from 'components/Toast';

export default {
  url: 'local/api/post/add',
  meta: {
    method: 'POST',
    data() {
      const { files = [], content } = this.state;
      const form = new FormData();
      form.append('content', content);
      files.forEach(vo => form.append('files', vo));
      return form;
    },
    auto: false,
    header() {
      return {
        'Content-Type': 'multipart/form-data',
        'X-Requested-With': 'XMLHttpRequest',
      }
    },
    end(response) {
      if (Number(response.code) === 0) {
        Toast.toast('发布成功');
        setTimeout(() => this.props.history.go(-1), 1000);
        return;
      }
      Toast.toast(response.msg);
    }
  },
}
