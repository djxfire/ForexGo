/*
* @Date: 2021/2/28
* @Author: XueChengwu <xuechengwu@erayt.com>
* @Copyright: 2015-2019 Erayt, Inc.
* @Description: If you have some questions, please contact: xuechengwu@erayt.com.
*/

export default {
  url: 'local/api/post/vote',
  meta: {
    data() {
      const { vote, status, post } = this.props;
      return {
        vote,
        status,
        post,
      };
    },
    auto: false,
    method: 'POST',
  },
}
