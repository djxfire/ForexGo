/*
* @Date: 2021/2/17
* @Author: XueChengwu <xuechengwu@erayt.com>
* @Copyright: 2015-2019 Erayt, Inc.
* @Description: If you have some questions, please contact: xuechengwu@erayt.com.
*/

export default {
  url: 'local/api/forex/news',
  meta: {
    data() {
      const { activeCurrency, page } = this.state;
      return {
        c: activeCurrency,
        p: page,
      };
    },
    auto: false,
  },
  resolve: (res) => {
    console.log('res ===> ', res);
    if (Number(res.code) === 1) {
      return {
        total: 0,
        data: [],
        page: 1,
      };
    }
    return {
      total: res.data.total,
      data: res.data.data,
      page: res.data.page,
    };
  },
}
