/*
* @Date: 2021/2/26
* @Author: XueChengwu <xuechengwu@erayt.com>
* @Copyright: 2015-2019 Erayt, Inc.
* @Description: If you have some questions, please contact: xuechengwu@erayt.com.
*/
import Toast from 'components/Toast';

export default {
  url: 'local/api/register',
  meta: {
    method: 'POST',
    data() {
      const { username, password, email, avatar } = this.state;
      const form = new FormData();
      form.append('username', username);
      form.append('email', email);
      form.append('avatar', avatar);
      form.append('password', password);
      return form;
    },
    auto: false,
    header() {
      return {
        'Content-Type': 'multipart/form-data'
      }
    },
    end(response) {
      if (Number(response.code) === 1) {
        Toast.toast(response.msg);
        return;
      }
      Toast.toast('注册成功');
      this.props.history.go(-1);
    }
  }
}
