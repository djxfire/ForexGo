/*
* @Date: 2021/2/28
* @Author: XueChengwu <xuechengwu@erayt.com>
* @Copyright: 2015-2019 Erayt, Inc.
* @Description: If you have some questions, please contact: xuechengwu@erayt.com.
*/

export default {
  url: 'local/api/post/comment',
  meta: {
    data() {
      const { post, to = 0 } = this.props;
      const { content } = this.state;
      return {
        post,
        to,
        content,
      };
    },
    auto: false,
    method: 'POST',
  }
}
