/*
* @Date: 2021/1/2
* @Author: XueChengwu <xuechengwu@erayt.com>
* @Copyright: 2015-2019 Erayt, Inc.
* @Description: If you have some questions, please contact: xuechengwu@erayt.com.
*/

/**
 * 获取K线数据
 */
export default {
  url: 'indicator-kline/api/qt/stock/kline/get',
  meta: {
    data() {
      const { productId, klineType = '001'} = this.props;
      const end = XUE.formatDate(new Date(), 'yyyyMMdd');
      return {
        secid: productId,
        klt: klineType,
        fqt: 0,
        lmt: 2880,
        end,
        iscca: 1,
        fields1: 'f1,f2,f3,f4,f5',
        fields2: 'f51,f52,f53,f54,f55,f56,f57',
        ut:'f057cbcbce2a86e2866ab8877db1d059',
        forcect: 1
      };
    },
    watch: ['productId', 'klineType'],
    auto: true,
  },
  resolve: (result) => {
    let ret = [];
    if(result.rc === 0) {
      let lines = result.data.klines;
      ret = lines.map((item) => {
        const bar = item.split(',');
        return {
          datetime: new Date(bar[0]).getTime(),
          open: parseFloat(bar[1]),
          close: parseFloat(bar[2]),
          high: parseFloat(bar[3]),
          low: parseFloat(bar[4]),
          volume: parseFloat(bar[5])
        }
      });
    }
    return { data: ret };
  },
}
