/*
* @Date: 2021/2/28
* @Author: XueChengwu <xuechengwu@erayt.com>
* @Copyright: 2015-2019 Erayt, Inc.
* @Description: If you have some questions, please contact: xuechengwu@erayt.com.
*/

export default {
  url: 'local/api/user/star',
  meta: {
    method: 'POST',
    data() {
      const { starUser } = this.props;
      const { status } = this.state;
      return {
        starUser,
        status,
      };
    },
    auto: false,
  },
  resolve: (res) => {

  }
}
