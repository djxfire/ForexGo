/*
* @Date: 2021/2/25
* @Author: XueChengwu <xuechengwu@erayt.com>
* @Copyright: 2015-2019 Erayt, Inc.
* @Description: If you have some questions, please contact: xuechengwu@erayt.com.
*/

export default {
  url: function() {
    let {
      year = '',
      month = '',
      day = ''
    } = this.props;
    const date = new Date();
    year = year === '' ? date.getFullYear() : year;
    month = month === '' ? date.getMonth() + 1 > 9 ? (date.getMonth() + 1) : `0${date.getMonth() + 1}` : month;
    day = day === '' ? date.getDate() > 9 ? date.getDate() : `0${date.getMonth()}` : day > 9 ? day : `0${day}`;
    return `calendar/data/${year}/${month}${day}/economics.json`
  },
  meta: {
    data() {
      return {
        _: new Date().getTime(),
      };
    }
  },
  resolve: (data) => {
    return {
      calendars: data
    }
  }
}
