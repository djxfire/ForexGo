/*
* @Date: 2021/2/22
* @Author: XueChengwu <xuechengwu@erayt.com>
* @Copyright: 2015-2019 Erayt, Inc.
* @Description: If you have some questions, please contact: xuechengwu@erayt.com.
*/
const productSymbol = {
  '美元': 'USD',
  '欧元': 'EUR',
  '英镑': 'GBP',
  '日元': 'JPY',
  '澳元': 'AUD',
  '纽元': 'NZD',
  '瑞郎': 'CHF',
  '加元': 'CAD',
};
export default {
  url: 'local/api/organ/cftc4',
  meta: {
    data() {
      return {};
    },
  },
  resolve: (response) => {
    const res = response.data.data;
    let dates = Object.keys(res.values).sort((a, b) => new Date(a).getTime() - new Date(b).getTime());
    let result = [];
    for(let i = 0; i < dates.length; i++) {
      let obj = res.values[dates[i]];
      let tmp = {};
      for(let key in obj) {
        tmp[productSymbol[key]] = {
          long: obj[key][0],
          short: obj[key][1],
          position: obj[key][2],
        };
      }
      result.push({
        date: dates[i],
        ...tmp
      });
    }
    return {
      data: result
    };
  }
}
