/*
* @Date: 2021/2/23
* @Author: XueChengwu <xuechengwu@erayt.com>
* @Copyright: 2015-2019 Erayt, Inc.
* @Description: If you have some questions, please contact: xuechengwu@erayt.com.
*/

export default {
  url: 'local/api/data/list',
  meta: {
    data() {
      const { mkt, stat } = this.state;
      return {
        mkt,
        stat,
      };
    },
    auto: false,
  },
  resolve: (res) => {
    if (Number(res.code) === 1) {
      return { data: [] };
    }
    const response = res.data.data;
    const xData =response.X.split(',');
    const lastData = response.Y[0].split(',');
    const nowData = response.Y[1].split(',');
    const data = [];
    for (let i = 0; i < xData.length; i++) {
      data.push({
        item: xData[i],
        last: lastData[i] !== '' ? Number(lastData[i]) : 0,
        now: nowData[i] !== '' ? Number(nowData[i]) : 0,
      });
    }
    return { data };
  }
}
