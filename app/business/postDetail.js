/*
* @Date: 2021/2/28
* @Author: XueChengwu <xuechengwu@erayt.com>
* @Copyright: 2015-2019 Erayt, Inc.
* @Description: If you have some questions, please contact: xuechengwu@erayt.com.
*/

export default {
  url: 'local/api/post/detail',
  meta: {
    data() {
      const { post } = this.props;
      return {
        id: post,
      };
    },
    end(res) {
      const { onPostLoad } = this.props;
      onPostLoad && onPostLoad(res);
    }
  },
  resolve: (res) => {
    if (Number(res.code) === 0) {
      const { post } = res.data;
      return {
        ...post,
      };
    }
    return {};
  }
}
