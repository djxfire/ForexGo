/*
* @Date: 2021/2/17
* @Author: XueChengwu <xuechengwu@erayt.com>
* @Copyright: 2015-2019 Erayt, Inc.
* @Description: If you have some questions, please contact: xuechengwu@erayt.com.
*/

export default {
  url: 'local/api/forex/newsDetail',
  meta: {
    data() {
      const { newId } = this.props.match.params;
      return {
        id: newId,
      };
    }
  },
  resolve: (res) => {
    if (Number(res.code) === 1) {
      return {
        title: '',
        abstract: '',
        content: [],
      };
    }
    return {
      ...res.data,
    };
  }
}
