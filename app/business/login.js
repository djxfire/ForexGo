/*
* @Date: 2021/2/26
* @Author: XueChengwu <xuechengwu@erayt.com>
* @Copyright: 2015-2019 Erayt, Inc.
* @Description: If you have some questions, please contact: xuechengwu@erayt.com.
*/
import Toast from 'components/Toast';
import Storage from 'utils/storage';
export default {
  url: 'local/api/login',
  meta: {
    data() {
      const { username, password } = this.state;
      return {
        username,
        password,
      };
    },
    auto: false,
    method: 'POST',
    end(response) {
      if (Number(response.code) === 1) {
        Toast.toast(response.msg);
        return;
      }
      Toast.toast('登录成功');
      const { user } = response.data;
      Storage.set('user', user);
      this.props.userStore.setUser(user);
      this.props.history.go(-1);
      XUE.executeHandler('login-success');
      XUE.executeHandler('user:star:refresh');
    }
  },
  resolve: (res) => {
    return res;
  }

}
