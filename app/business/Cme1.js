/*
* @Date: 2021/2/22
* @Author: XueChengwu <xuechengwu@erayt.com>
* @Copyright: 2015-2019 Erayt, Inc.
* @Description: If you have some questions, please contact: xuechengwu@erayt.com.
*/
const productSymbol = {
  '美元': 'USD',
  '欧元': 'EUR',
  '英镑': 'GBP',
  '日元': 'JPY',
  '澳元': 'AUD',
  '纽元': 'NZD',
  '瑞郎': 'CHF',
  '加元': 'CAD',
};
export default {
  url: 'local/api/organ/cme1',
  meta: {
    data() {
      return {};
    }
  },
  resolve: (response) => {
    const res = response.data.data;
    const dates = Object.keys(res.values).sort((a, b) => new Date(a).getTime() - new Date(b).getTime());
    let results = [];
    for (let i = 0; i < dates.length; i++) {
      let arr = res.values[dates[i]];
      let tmp = {};
      for (let i = 0; i < arr.length; i++) {
        if (arr[i][1] === '期货') {
          tmp[productSymbol[arr[i][0]]] = {
            electric: arr[i][2],
            onside: arr[i][3],
            outside: arr[i][4],
            volume: arr[i][5],
            nop: arr[i][6],
            change: arr[i][7]
          };
        }
      }
      results.push({
        date: dates[i],
        ...tmp
      });
    }
    return {
      data: results
    };
  }
}
