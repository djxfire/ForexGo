/*
* @Date: 2021/2/24
* @Author: XueChengwu <xuechengwu@erayt.com>
* @Copyright: 2015-2019 Erayt, Inc.
* @Description: If you have some questions, please contact: xuechengwu@erayt.com.
*/

export default {
  url: 'local/api/newest',
  meta: {
    data() {
      return {}
    }
  },
  resolve: (res) => {
    if (Number(res.code) !== 0) {
      return { data: [] };
    }
    return {
      data: res.data.data,
    }
  }
}
