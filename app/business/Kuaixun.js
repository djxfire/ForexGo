/*
* @Date: 2021/3/6
* @Author: XueChengwu <xuechengwu@erayt.com>
* @Copyright: 2015-2019 Erayt, Inc.
* @Description: If you have some questions, please contact: xuechengwu@erayt.com.
*/

export default {
  url() {
    const { page = 1 } = this.state;
    return `newsapi/kuaixun/v1/getlist_107__50_${page}_.html`;
  },
  meta: {
    data(){
      return {
        r: Math.random(),
        _: new Date().getTime(),
      };
    }
  },
  resolve(res) {
    return {
      data: res.LivesList,
      total: res.AtPage,
    };
  }
}
