/*
* @Date: 2021/3/6
* @Author: XueChengwu <xuechengwu@erayt.com>
* @Copyright: 2015-2019 Erayt, Inc.
* @Description: If you have some questions, please contact: xuechengwu@erayt.com.
*/
export default {
  url: 'local/api/market/bigbank',
  meta: {
    data() {
      const { page } = this.state;
      return {
        p: page,
      };
    },
  },
  resolve(response) {
    if (Number(response.code) === 0) {
      return {
        data: response.data.data,
        last_page: response.data.total,
      };
    }
    return {
      data: [],
      last_page: 0,
    };
  }
}

