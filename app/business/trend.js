/*
* @Date: 2021/2/15
* @Author: XueChengwu <xuechengwu@erayt.com>
* @Copyright: 2015-2019 Erayt, Inc.
* @Description: If you have some questions, please contact: xuechengwu@erayt.com.
*/
export default {
  url: 'indicator-kline/api/qt/stock/trends2/get',
  meta: {
    data() {
      const { productId } = this.props;
      return {
        secid: productId,
        ut: 'fa5fd1943c7b386f172d6893dbfba10b',
        fields1: 'f1,f2,f3,f4,f5,f6,f7,f8,f9,f10,f11',
        fields2: 'f51,f53,f56,f58',
        iscr: 0,
        ndays: 1,
        _: new Date().getTime()
      }
    },
    watch: ['productId'],
  },
  resolve: (data) => {
    const { trendsTotal, trends, preClose } = data.data;
    let quotations = trends.map((item) => {
      const tick = item.split(',');
      return {
        label: new Date(tick[0]).getTime(),
        value: parseFloat(tick[1]),
        close: parseFloat(tick[1]),
        volume: parseFloat(tick[2])
      }
    });
    return {
      data: quotations,
      trendsTotal,
      preClose
    };
  }
}
