/*
* @Date: 2021/2/15
* @Author: XueChengwu <xuechengwu@erayt.com>
* @Copyright: 2015-2019 Erayt, Inc.
* @Description: If you have some questions, please contact: xuechengwu@erayt.com.
*/
import Toast from 'components/Toast';
import Storage from 'utils/storage';
export default () => {
  const getProducts = new Promise((resolve, reject) => {
    XUE.fetch('/local/api/symbol/list')
      .then(response => {
        if (Number(response.code) === 0) {
          const { data = [] } = response.data;
          Storage.set('fx_products', data);
          resolve(1);
        } else {
          resolve(0);
        }
      }).catch(err => {
        console.error('init error ===>', err);
        reject(0);
    });
  });
  const markvoPromise = new Promise((resolve, reject) => {
    XUE.fetch('/local/brain/markvo.json')
      .then(res => {
        Storage.set('markvo', res);
        resolve(1);
      }).catch(err => {
      console.log('init error ===>', err);
      reject(0);
    });
  });
  XUE.Ws.subscribe(
    [{
      title: 'JIN10_NEWS_PUSH',
      callback: (response) => {
        if (response.id !== 'global') {
          return;
        }
        const res = JSON.parse(response.data);
        const length = res.length;
        let importanceMessage = '';
        for (let i = 0; i < length; i++) {
          if (Number(res[i].important) === 1) {
            importanceMessage = res[i].data.content;
            break;
          }
        }
        if (importanceMessage !== '') {
          Toast.message('danger', importanceMessage);
        }
      }
    }], 'global');
  XUE.addHandler('login-success', () => {
    const user = Storage.get('user', true);
    XUE.Ws.subscribe([{
      title: `TRADE_${user.username}`,
      callback: (response) => {

      }
    }]);
  });
  return Promise.all([
    getProducts,
    markvoPromise,
  ]);
}
