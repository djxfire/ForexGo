/*
* @Date: 2021/3/4
* @Author: XueChengwu <xuechengwu@erayt.com>
* @Copyright: 2015-2019 Erayt, Inc.
* @Description: If you have some questions, please contact: xuechengwu@erayt.com.
*/

export default class {
  constructor(stateCount, matrix, matrixCount) {
    this.matrix = [];
    if (matrix) {
      this.matrix = matrix;
      this.matrixCount = matrixCount;
    } else {
      this.matrix = [];
      this.matrixCount = [];
      for (let i = 0; i < stateCount; i++) {
        let state = [];
        let stateCount = [];
        for (let j = 0; j < stateCount; j++) {
          state.push(1 / stateCount);
          stateCount.push(0);
        }
        this.matrix.push(state);
        this.matrixCount.push(stateCount);
      }
    }
  }

  cast(state) {
    const nextArr = this.matrix[state];
    let maxIndex = 0;
    let maxValue = Number.MIN_VALUE;
    let sum = 0;
    for (let i = 0; i < nextArr.length; i++) {
      sum += nextArr[i];
    }
    for (let i = 0; i < nextArr.length; i++) {
      if (nextArr[i] > maxValue) {
        maxValue = nextArr[i];
        maxIndex = i;
      }
    }
    return {
      nextState: maxIndex,
      nextProp: nextArr,
    };
  }
}

