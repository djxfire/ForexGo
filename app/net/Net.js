import axios from 'axios';
const hostUrl1 = 'remote'; //'remote';//'http://148.70.157.132/';
const hostUrl2 = 'remote/app';

function defaultResolver(data) {
    return data;
}

const Net = (action, params, options = { method: 'GET', header: {} }, resolver = defaultResolver, type = true) => {
    if (options.method === 'GET') {
        return axios.get(`${type ? hostUrl1 : hostUrl2}/${action}`, {
            method: options.method,
            params,
            headers: options.header
        }).then((res) => {
            if (res.data.code === -401) {
              window.location.hash = '/login';
              return;
            }
            return resolver(res.data);
        });
    } else {
        return axios.post(`${type ? hostUrl1 : hostUrl2}/${action}`, params, { headers: options.header})
            .then((res) => {
                console.log('res===>', res);
                if (res.data.code === -401) {
                    window.location.hash = '/login';
                //    UIManager.doLogin();
                    return;
                } else {
                    return resolver(res.data);
                }
            }).catch(err => {
                console.log('err===>', err);
          });
    }
}
Net.hostUrl = hostUrl1;

export default Net;
