import React from 'react';
import { HashRouter, Route, Switch, Redirect } from 'react-router-dom';
import Home from 'pages/Home';
import Search from 'pages/Search';
import Quotation from 'pages/Quotation';
import QuotationNews from 'pages/QuotationNews';
import Login from 'pages/Login';
import Register from 'pages/Register';
import NewPost from 'pages/NewPost';
import PostDetail from 'pages/PostDetail';
import QuoteSetting from 'pages/QuoteSetting';


export default class AppRouter extends React.Component {
    render() {
        return (
          <HashRouter>
              <Switch>
                  {/*<Route path='/index' component={Home} />*/}
                  <Route path='/index' extra component={Home} />
                  <Route path='/search' component={Search} />
                  <Route path='/quotation/:productId' component={Quotation} />
                  <Route path='/news/:newId' component={QuotationNews} />
                  <Route path='/login' component={Login} />
                  <Route path='/register' component={Register} />
                  <Route path='/newPost' component={NewPost} />
                  <Route path='/postDetail/:id' component={PostDetail} />
                  <Route path='/quoteSetting/:indicatorId' component={QuoteSetting} />
                  <Redirect to="/index" />
              </Switch>

          </HashRouter>
        );
    }
}
