import React from 'react';
import QuoteLayer from './bu/QuoteLayer';
import Canvas from '../../core/Canvas';
import OChart from '../../core/OChart';
import Icon from "components/Icon";


export default class extends React.Component {
    constructor(props){
        super(props);
        this.ref = React.createRef();
        this.state = {
            legends: [],
        };
    }

    componentWillReceiveProps (nextProps, nextContext) {
        const { data = [], params } = nextProps;
        if (this.props.data !== data) {
            this.quoteLayer.data = data;
            this.quoteLayer.make().then(() => {
                this.canvas.paint();
            });
        }
        if (params !== this.props.params) {
            this.quoteLayer.params = params;
            this.quoteLayer.make().then(() => {
                this.canvas.paint();
            });
        }
    }

    componentDidMount() {
        const { data = [], indicator = 'macd', params = {}} = this.props;
        this.canvas = new Canvas({
            ele: this.ref.current,
            canAction: false,
        });
        this.quoteLayer = new QuoteLayer(this.canvas, {
            width: this.canvas.width,
            height: this.canvas.height,
            name: indicator,
            params,
        }, data);
        this.quoteLayer.setQuoteEmitter((legends) => {
            this.setState({
                legends
            });
        });
        this.canvas.addChild(this.quoteLayer);
        this.quoteLayer.make().then(() => {
            this.canvas.paint();
        });
        this.listen();
    }

    listen = () => {
        const { chart = '', name = '' } = this.props;
        OChart.on(`${chart}:onChartMaked`, this._onChartMaked);
    }

    _onChartMaked = (option) => {
        const { xStep, start, end } = option;
        this.quoteLayer.xStep = xStep;
        this.quoteLayer.dataStart = start;
        this.quoteLayer.dataEnd = end;
        this.quoteLayer.make().then(() => {
            this.canvas.paint();
        });
    }

    render() {
        const { className = '', style = {} } = this.props;
        const { legends = [] }  = this.state;
        return (
            <div style={style}>
                <div style={{display: 'flex', padding: '2px 12px'}}>
                {
                    legends.map((item) => (
                        <div style={{color: item.color, flex: 1,}}>
                            {item.label} &nbsp;&nbsp;{item.value}
                        </div>
                    ))
                }
                    <Icon type="lajitong"  onClick={this.props.onDelete}/>
                    <Icon type="setting" onClick={this.props.onSetting}/>
                </div>
                <div className={className} ref={this.ref} style={{borderTop: 'solid 1px rgba(220, 220, 220, 0.3)'}} />
            </div>

        )
    }
}
