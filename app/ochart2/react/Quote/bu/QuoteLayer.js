import Layer from '../../../core/Layer';
import getQuote from  '../../../../workers/quoteTool';
import MadcLayer from './MadcLayer';
import QuoteLineLayer from './QuoteLineLayer';

function quoteCaculator(layer, data, start, end) {
    const { name } = layer;
    const quotes = {
        macd: (layer, data, start, end) => {
        const { DIFLen = 12, DEALen = 26, M = 9 } = layer.params;
        let maxParam = DIFLen;
          if (DEALen > maxParam) {
              maxParam = DEALen;
          }
          if (M > maxParam) {
              maxParam = M;
          }
          let dataStart = start - maxParam - 1;
          if (dataStart < 0) {
              dataStart = 0;
          }
          if (end > data.length) {
              end = data.length;
          }
          const close = [];
          for (let i = dataStart; i < end; i++) {
              close.push(data[i].close);
          }
          return new Promise((resolve) => {
              getQuote('macd', {
                  close,
                  DIFLen,
                  DEALen,
                  M
              }).then((result) => {
                  const { DIF, DEA, BAR } = result;
                  console.log('result', result);
                  const dataLen = end - start;
                  const DIFData = [];
                  const DEAData = [];
                  const BARData = [];
                  for (let i = DIF.length - dataLen; i < DIF.length; i++) {
                      DIFData.push(DIF[i]);
                      DEAData.push(DEA[i]);
                      BARData.push(BAR[i]);
                  }
                  let qLayer = new MadcLayer(layer.canvas, {
                      xStep:  layer.xStep,
                      ...(layer.style || {}),
                  }, {
                      DIF: DIFData,
                      DEA: DEAData,
                      BAR: BARData
                  });
                  const legend = (shift) => {
                      if (shift < 0) {
                          shift = DIFData.length + shift;
                      }
                      return [{
                          label: `DIF(${DIFLen})`,
                          value: Number(DIFData[shift]).toFixed(4),
                      }, {
                          label: `DEA(${DEALen})`,
                          value: Number(DEAData[shift]).toFixed(4),
                      }, {
                          label: `BAR(${M})`,
                          value: Number(BARData[shift]).toFixed(4),
                      }]
                  };
                  resolve({
                      qLayer,
                      legend,
                  })
              });
          });
      },
        kdj: (layer, data, start, end) => {
            const { N = 9, M = 3, L = 3, S = 3} = layer.params;
            let dataStart = start - N - M - L - S;
            if (dataStart < 0) {
                dataStart = 0;
            }
            if (end > data.length) {
                end = data.length;
            }
            let high = [], low = [], close = [];
            for (let i = dataStart; i < end; i++) {
                high.push(data[i].high);
                low.push(data[i].low);
                close.push(data[i].close);
            }
            return new Promise((resolve) => {
                getQuote('kdj', {
                    high,
                    low,
                    close,
                    N,
                    M,
                    L,
                    S,
                }).then((result) => {
                    const { KValue, DValue, JValue } = result;
                    const dataLen = end - start;
                    let KData = [];
                    let DData = [];
                    let JData = [];
                    for (let i = KValue.length - dataLen; i < KValue.length; i++) {
                        KData.push(KValue[i]);
                        DData.push(DValue[i]);
                        JData.push(JValue[i]);
                    }
                    let qLayer = new QuoteLineLayer(layer.canvas, {
                        color: '#666666',
                        fontSize: 20,
                        thresholds: [20, 80],
                        max: 100,
                        min: 0,
                        xStep: layer.xStep,
                    }, [{
                        data: KData,
                        color: '#2567B9'
                    }, {
                        data: DData,
                        color: '#9212A2',
                    }, {
                        data: JData,
                        color: '#E99331'
                    }]);
                    const legend = (shift) => {
                        if (shift < 0) {
                            shift = KData.length + shift;
                        }
                        return [{
                            label: `K(${N})`,
                            value: KData[shift]
                        }, {
                            label: `D(${N})`,
                            value: DData[shift],
                        }, {
                            label: `J(${N})`,
                            value: JData[shift],
                        }]
                    };
                    resolve({
                        qLayer,
                        legend,
                    });
                })
            });
        },
        bias: (layer, data, start, end) => {
          const { N1 = 6, N2 = 10, N3 = 30 } = layer.params;
          let maxParam = N1;
          if (N2 > maxParam) {
              maxParam = N2;
          }
          if (N3 > maxParam) {
              maxParam = N3;
          }
          let dataStart = start - maxParam - 1;
          if (dataStart < 0) {
              dataStart = 0;
          }
          if (end > data.length) {
              end = data.length;
          }
          const close = [];
          for (let i = dataStart; i < end; i++) {
              close.push(data[i].close);
          }
          return new Promise((resolve) => {
              getQuote('bias', {
                  close,
                  N1,
                  N2,
                  N3,
              }).then((result) => {
                 const { biasN1, biasN2, biasN3 } = result;
                 const dataLen = end - start;
                 const biasN1Data = [], biasN2Data = [], biasN3Data = [];
                 for (let i = biasN1.length - dataLen; i < biasN1.length; i++) {
                     biasN1Data.push(biasN1[i]);
                     biasN2Data.push(biasN2[i]);
                     biasN3Data.push(biasN3[i]);
                 }
                  let qLayer = new QuoteLineLayer(layer.canvas, {
                      color: '#666666',
                      fontSize: 20,
                      thresholds: [-3, 0, 3],
                      max: 0,
                      min: 0,
                      base: 0,
                      fixed: true,
                      xStep: layer.xStep,
                  }, [{
                      data: biasN1Data,
                      color: '#2567B9'
                  }, {
                      data: biasN2Data,
                      color: '#9212A2',
                  }, {
                      data: biasN3Data,
                      color: '#E99331'
                  }]);
                  const legend = (shift) => {
                      if (shift < 0) {
                          shift = biasN1Data.length + shift;
                      }
                      return [{
                          label: `bias(${N1})`,
                          value: Number(biasN1Data[shift]).toFixed(2),
                          color: '#2567B9',
                      }, {
                          label: `bias(${N2})`,
                          value: Number(biasN2Data[shift]).toFixed(2),
                          color: '#9212A2',
                      }, {
                          label: `bias(${N3})`,
                          value: Number(biasN3Data[shift]).toFixed(2),
                          color: '#E99331',
                      }]
                  };
                  resolve({
                      qLayer,
                      legend,
                  });
              });
          })

        },
        rsi: (layer, data, start, end) => {
            const { N1 = 12, N2 = 16, N3 = 24 } = layer.params;
            let maxParam = N1;
            if (N2 > maxParam) {
                maxParam = N2;
            }
            if (N3 > maxParam) {
                maxParam = N3;
            }
            let dataStart = start - maxParam - 1;
            if (dataStart < 0) {
                dataStart = 0;
            }
            if (end > data.length) {
                end = data.length;
            }
            let close = [];
            for (let i = dataStart; i < end; i++) {
                close.push(data[i].close);
            }
            return new Promise((resolve) => {
               getQuote('rsi', {
                   close,
                   N1,
                   N2,
                   N3,
               }).then((result) => {
                   const { rsiN1, rsiN2, rsiN3 } = result;
                   let dataLen = end - start;
                   const rsiN1Data = [];
                   const rsiN2Data = [];
                   const rsiN3Data = [];
                   for (let i = rsiN1.length - dataLen; i < rsiN1.length; i++) {
                       rsiN1Data.push(rsiN1[i]);
                       rsiN2Data.push(rsiN2[i]);
                       rsiN3Data.push(rsiN3[i]);
                   }
                   let qLayer = new QuoteLineLayer(layer.canvas, {
                       color: '#666666',
                       fontSize: 20,
                       thresholds: [30, 50, 70],
                       max: 100,
                       min: 0,
                       base: 50,
                       fixed: true,
                       xStep: layer.xStep,
                   }, [{
                       data: rsiN1Data,
                       color: '#2567B9'
                   }, {
                       data: rsiN2Data,
                       color: '#9212A2',
                   }, {
                       data: rsiN3Data,
                       color: '#E99331'
                   }]);
                   const legend = (shift) => {
                       if (shift < 0) {
                           shift = rsiN1Data.length + shift;
                       }
                       return [{
                           label: `RSI(${N1})`,
                           value: Math.round(rsiN1Data[shift]),
                           color: '#2567B9'
                       }, {
                           label: `RSI(${N2})`,
                           value: Math.round(rsiN2Data[shift]),
                           color: '#9212A2',
                       }, {
                           label: `RSI(${N3})`,
                           value: Math.round(rsiN3Data[shift]),
                           color: '#E99331'
                       }]
                   };
                   resolve({
                       qLayer,
                       legend,
                   });
               })
            });
        },
        cci: (layer, data, start, end) => {
            const { N = 24 } = layer.params;
            let dataStart = start - N - 1;
            if (dataStart < 0) {
                dataStart = 0;
            }
            if (end > data.length) {
                end = data.length;
            }
            const high = [], low = [], close = [];
            for (let i = dataStart; i < end; i++) {
                high.push(data[i].high);
                low.push(data[i].low);
                close.push(data[i].close);
            }
            return new Promise(((resolve) => {
                getQuote('cci', {
                    high,
                    low,
                    close,
                    N
                }).then((result) => {
                    const { CCI } = result;
                    const dataLen = end - start;
                    const cciData = [];
                    for (let i = CCI.length - dataLen; i < CCI.length; i++) {
                        cciData.push(CCI[i]);
                    }
                    const qLayer = new QuoteLineLayer(layer.canvas, {
                        color: '#666666',
                        fontSize: 20,
                        thresholds: [-100, 0, 100],
                        base: 0,
                        fixed: true,
                        xStep: layer.xStep,
                    }, [{
                        data: cciData,
                        color: '#2567B9',
                    }]);
                    const legend = (shift) => {
                        if (shift < 0) {
                            shift = cciData.length + shift;
                        }
                        return [{
                            label: `CCI(${N})`,
                            value: Math.round(cciData[shift]),
                            color: '#2567B9',
                        }];
                    };
                    resolve({
                        qLayer,
                        legend,
                    });
                });
            }))
        },
        ad(layer, data, start, end) {
            const { M = 12 } = layer.params;
            const high = [], low = [], close = [], volume = [];
            let dataStart = start - M;
            if (dataStart < 0) {
                dataStart = 0;
            }
            if (end > data.length) {
                end = data.length;
            }
            for (let i = dataStart; i < end; i++) {
                high.push(data[i].high);
                low.push(data[i].low);
                close.push(data[i].close);
                volume.push(data[i].volume);
            }
            return new Promise((resolve) => {
                getQuote('ad', {
                    high,
                    low,
                    close,
                    volume,
                    M
                }).then((result) => {
                   const { AD, ADMA } = result;
                   const adData = [], adMaData = [];
                   const dataLen = end - start;
                   for (let i = AD.length - dataLen; i < AD.length; i++) {
                       adData.push(AD[i]);
                       adMaData.push(ADMA[i]);
                   }
                   const qLayer = new QuoteLineLayer(layer, {
                       color: '#666666',
                       fontSize: 20,
                       thresholds: [0],
                       xStep: layer.xStep,
                   }, [{
                       data: adData,
                       color: '#E99331',
                   }, {
                       data: adMaData,
                       color: '#2567B9',
                   }]);
                    const legend = (shift) => {
                        if (shift < 0) {
                            shift = adData.length + shift;
                        }
                        return [{
                            label: `AD`,
                            value: adData[shift],
                            color: '#E99331'
                        }, {
                            label: `MA(${M})`,
                            value: adMaData[shift],
                            color: '#2567B9'
                        }];
                    };
                    resolve({
                        qLayer,
                        legend,
                    });
                });
            })
        },
        arbr(layer, data, start, end) {
            const { N = 26 } = layer.params;
            const high = [], low = [], close = [], open = [];
            let dataStart = start - N - 1;
            if (dataStart < 0) {
                dataStart = 0;
            }
            if (end > data.length) {
                end = data.length;
            }
            for (let i = dataStart; i < end; i++) {
                high.push(data[i].high);
                low.push(data[i].low);
                close.push(data[i].close);
                open.push(data[i].open);
            }
            return new Promise((resolve) => {
                getQuote('arbr', {
                    high,
                    low,
                    open,
                    close,
                    N
                }).then((result) => {
                   const { ar, br } = result;
                   let dataLen = end - start;
                   const arData = [], brData = [];
                   for (let i = ar.length - dataLen; i < ar.length; i++) {
                       arData.push(ar[i]);
                       brData.push(br[i]);
                   }
                   const qLayer = new QuoteLineLayer(layer.canvas, {
                       color: '#666666',
                       fontSize: 20,
                       thresholds: [40, 60, 100, 140, 180, 200],
                       xStep: layer.xStep,
                       fixed: true,
                       base: 100,
                   }, [{
                       data: arData,
                       color: '#D43323',
                   }, {
                       data: brData,
                       color: '#25D414',
                   }]);
                   const legend = (shift) => {
                       if (shift < 0) {
                           shift = cciData.length + shift;
                       }
                       return [{
                           label: `AR(${N})`,
                           value: arData[shift],
                           color: '#D43323',
                       }, {
                           label: `BR(${N})`,
                           value: brData[shift],
                           color: '#25D414',
                       }];
                   };
                   resolve({
                       qLayer,
                       legend,
                   });
                });
            });
        },
        aroon(layer, data, start, end) {
            const { N = 20 } = layer.params;
            const high = [], low = [];
            let dataStart = start - N - 1;
            if (dataStart < 0) {
                dataStart = 0;
            }
            if (end > data.length) {
                end = data.length;
            }
            for (let i = dataStart; i < end; i++) {
                high.push(data[i].high);
                low.push(data[i].low);
            }
            return new Promise((resolve) => {
                getQuote('aroon', {
                    high,
                    low,
                    N,
                }).then((result) => {
                   const { uparoon, downaroon } = result;
                   const dataLen = end - start;
                   let upData = [],
                       downData = [];
                   for (let i = uparoon.length - dataLen; i < uparoon.length; i++) {
                       upData.push(uparoon[i]);
                       downData.push(downaroon[i]);
                   }
                   const qLayer = new QuoteLineLayer(layer.canvas, {
                       color: '#666666',
                       fontSize: 20,
                       thresholds: [30, 50, 70],
                       base: 50,
                       fixed: true,
                       xStep: layer.xStep,
                   }, [{
                       data: upData,
                       color: '#D43323',
                   }, {
                       data: downData,
                       color: '#25D414',
                   }]);
                   const legend = (shift) => {
                       if (shift < 0) {
                           shift = upData.length + shift;
                       }
                       return [{
                           label: `UP(${N})`,
                           value: upData[shift],
                           color: '#D43323',
                       }, {
                           label: `DOWN(${N})`,
                           value: downData[shift],
                           color: '#25D414'
                       }];
                   };
                   resolve({
                       qLayer,
                       legend
                   });
                });
            })
        },
        atr(layer, data, start, end) {
          const { N = 14, M = 6 } = layer.params;
          const high = [], close = [], low = [];
          let dataStart = start - N - M - 1;
          if (dataStart < 0) {
              dataStart = 0;
          }
          if (end > data.length) {
              end = data.length;
          }
          for (let  i = dataStart; i < end; i++) {
              high.push(data[i].high);
              low.push(data[i].low);
              close.push(data[i].close);
          }
          return new Promise((resolve) => {
             getQuote('atr', {
                 high,
                 low,
                 close,
                 N,
                 M,
             }).then((result) => {
                const { atrV, atrMa } = result;
                let dataLen = end - start;
                let atrData = [];
                let atrMaData = [];
                for (let i = atrV.length - dataLen; i < atrV.length; i++) {
                    atrData.push(atrV[i]);
                    atrMaData.push(atrMa[i]);
                }
                const qLayer = new QuoteLineLayer(layer.canvas, {
                    color: '#666666',
                    fontSize: 20,
                    thresholds: [],
                    xStep: layer.xStep,
                }, [{
                    data: atrData,
                    color: '#D43323',
                }, {
                    data: atrMaData,
                    color: '#25D414',
                }]);
                const legend = (shift) => {
                    if (shift < 0) {
                        shift = atrData.length + shift;
                    }
                    return [{
                        label: `ATR(${N})`,
                        value: Number(atrData[shift]).toFixed(3),
                        color: '#D43323',
                    }, {
                        label: `MA(${M})`,
                        value: Number(atrMaData[shift]).toFixed(3),
                        color: '#25D414',
                    }];
                };
                resolve({
                   qLayer,
                   legend,
                });
             });
          });
        },
        cmo(layer, data, start, end) {
            const { N = 14 } = layer.params;
            const close = [];
            let dataStart = start - N - 1;
            if (dataStart < 0) {
                dataStart = 0;
            }
            if (end > data.length) {
                end = data.length;
            }
            for (let i = dataStart; i < end; i++) {
                close.push(data[i].close);
            }
            return new Promise((resolve) => {
               getQuote('cmo', {
                   close,
                   N,
               }).then((result) => {
                   const { cmoV } = result;
                   let dataLen = end - start;
                   let cmoData = [];
                   for (let i = cmoV.length - dataLen; i < cmoV.length; i++) {
                       cmoData.push(cmoV[i]);
                   }
                   const qLayer = new QuoteLineLayer(layer.canvas, {
                       color: '#666666',
                       fontSize: 20,
                       thresholds: [-50, 0, 50],
                       xStep: layer.xStep,
                       fixed: true,
                       base: 0,
                   }, [{
                       data: cmoData,
                       color: '#2567B9',
                   }]);
                   const legend = (shift) => {
                     if (shift < 0) {
                         shift = cmoData.length + shift;
                     }
                     return [{
                         label: `CMO(${N})`,
                         value: cmoData[shift],
                         color: '#2567B9'
                     }];
                   };
                   resolve({
                       qLayer,
                       legend,
                   });
               })
            });
        },
        cr(layer, data, start, end) {
            const { N = 14 } = layer.params;
            const close = [], high = [], low = [];
            let dataStart = start - N - 1;
            if (end > data.length) {
                end = data.length;
            }
            if (dataStart < 0) {
                dataStart = 0;
            }
            for (let i = dataStart; i < end; i++) {
                close.push(data[i].close);
                high.push(data[i].high);
                low.push(data[i].low);
            }
            return new Promise((resolve) => {
               getQuote('cr', {
                   high,
                   low,
                   close,
                   N
               }).then((result) => {
                   const { crV } = result;
                   const dataLen = end - start;
                   let crData = [];
                   for (let i = crV.length - dataLen; i < crV.length; i++) {
                       crData.push(crV[i]);
                   }
                   const qLayer = new QuoteLineLayer(layer.canvas, {
                       color: '#666666',
                       fontSize: 20,
                       thresholds: [40, 100, 160],
                       xStep: layer.xStep,
                       fixed: true,
                       base: 100,
                   }, [{
                       data: crData,
                       color: '#2567B9',
                   }]);
                   const legend = (shift) => {
                      if (shift < 0) {
                          shift = crData.length + shift;
                      }
                       return [{
                           label: `CR(${N})`,
                           value: crData[shift],
                           color: '#2567B9',
                       }];
                   };
                   resolve({
                       qLayer,
                       legend
                   });
               }) ;
            });
        },
        cv(layer, data, start, end) {
            const { N = 14 } = layer.params;
            const high = [], low = [];
            let dataStart = start - N - 1;
            if (dataStart < 0) {
                dataStart = 0;
            }
            if (end > data.length) {
                end = data.length;
            }
            for (let i = dataStart; i < end; i++) {
                high.push(data[i].high);
                low.push(data[i].low);
            }
            return new Promise((resolve) => {
               getQuote('cv', {
                   high,
                   low,
                   N
               }).then((result) => {
                   const { cvV } = result;
                   let dataLen = end - start;
                   let cvData = [];
                   for (let i = cvV.length - dataLen; i < cvV.length; i++) {
                       cvData.push(cvV[i]);
                   }
                   const qLayer = new QuoteLineLayer(layer.canvas, {
                       color: '#666666',
                       fontSize: 20,
                       thresholds: [-50, 0, 50],
                       xStep: layer.xStep,
                       fixed: true,
                       base: 0,
                   }, [{
                       data: cvData,
                       color: '#2567B9',
                   }]);
                   const legend = (shift) => {
                     if (shift < 0) {
                         shift = cvData.length + shift;
                     }
                     return [{
                         label: `CV(${N})`,
                         value: cvData[shift],
                         color: '#2567B9',
                     }];
                   };
                   resolve({
                       qLayer,
                       legend,
                   });
               })
            });
        },
        dma(layer, data, start, end) {
            const { fast = 6, slow = 12, smooth = 6 } = layer.params;
            const close = [];
            let dataStart = start - fast - slow - smooth - 1;
            if (dataStart < 0) {
                dataStart = 0;
            }
            if (end > data.length) {
                end = data.length;
            }
            for (let i = dataStart; i < end; i++) {
                close.push(data[i].close);
            }
            return new Promise((resolve) => {
               getQuote('dma', {
                   close,
                   fast,
                   slow,
                   smooth
               }).then((result) => {
                   const { dmaValue, amaValue } = result;
                   let dataLen = end - start;
                   let dmaData = [];
                   let amaData = [];
                   for (let i = dmaValue.length - dataLen; i < dmaValue.length; i++) {
                       dmaData.push(dmaValue[i]);
                       amaData.push(amaValue[i]);
                   }
                   const qLayer = new QuoteLineLayer(layer.canvas, {
                       color: '#666666',
                       fontSize: 20,
                       thresholds: [0],
                       base: 0,
                       fixed: true,
                       xStep: layer.xStep,
                   }, [{
                       data: dmaData,
                       color: '#D43323',
                   }, {
                       data: amaData,
                       color:  '#25D414',
                   }]);
                   const legend = (shift) => {
                       if (shift < 0) {
                           shift = dmaData.length + shift;
                       }
                       return [{
                           label: `DMA(${fast}, ${slow})`,
                           value: dmaData[shift],
                           color: '#D43323',
                       }, {
                           label: `AMA(${smooth})`,
                           value: amaData[shift],
                           color: '#25D414',
                       }]
                   };
                   resolve({
                       qLayer,
                       legend
                   });
               });
            });
        },
        dmi(layer, data, start, end) {
            const { N = 12 } = layer.params;
            const close = [], low = [], high = [];
            let dataStart = start - N - 1;
            if (dataStart < 0) {
                dataStart = 0;
            }
            if (end > data.length) {
                end = data.length;
            }
            for (let i = dataStart; i < end; i++) {
                close.push(data[i].close);
                high.push(data[i].high);
                low.push(data[i].low);
            }
            return new Promise((resolve) => {
               getQuote('dmi', {
                   high, low, close, N,
               }).then((result) => {
                   const { PosDI, NegDI, ADX } = result;
                   const dataLen = end - start;
                   const PosDIData = [],
                       NegDIData = [],
                       ADXData = [];
                   for (let i = PosDI.length - dataLen; i < PosDI.length; i++) {
                       PosDIData.push(PosDI[i]);
                       NegDIData.push(NegDI[i]);
                       ADXData.push(ADX[i]);
                   }
                   const qLayer = new QuoteLineLayer(layer.canvas, {
                       color: '#666666',
                       fontSize: 20,
                       thresholds: [20, 50, 80],
                       base: 50,
                       fixed: true,
                       xStep: layer.xStep,
                   }, [{
                       data: PosDIData,
                       color: '#D43323'
                   }, {
                       data: NegDIData,
                       color: '#25D414'
                   }, {
                       data: ADXData,
                       color: '#2567B9',
                   }]);
                   const legend = (shift) => {
                     if (shift < 0) {
                         shift = PosDIData.length + shift;
                     }
                     return [{
                         label: `PosDI(${N})`,
                         value: PosDIData[shift],
                         color: '#D43323',
                     }, {
                         label: `NegDI${N}`,
                         value: NegDIData[shift],
                         color: '#25D414',
                     }, {
                         label: 'ADX',
                         value: ADXData[shift],
                         color: '#2567B9',
                     }];
                   };
                   resolve({
                       qLayer,
                       legend,
                   });
               }) ;
            });
        },
        dpo(layer, data, start, end) {
            const { N = 12 } = layer.params;
            const close = [];
            let dataStart = start - 2 * N - 1;
            if (dataStart < 0) {
                dataStart = 0;
            }
            if (end > data.length) {
                end = data.length;
            }
            for (let i = dataStart; i < end; i++) {
                close.push(data[i].close);
            }
            return new Promise((resolve) => {
               getQuote('dpo', {
                   close,
                   N
               }).then((result) => {
                   const { dpoV } = result;
                   const dataLen = end - start;
                   const dpoData = [];
                   for (let i = dpoV.length - dataLen; i < dpoV.length; i++) {
                       dpoData.push(dpoV[i]);
                   }
                   const qLayer = new QuoteLineLayer(layer.canvas, {
                       color: '#666666',
                       fontSize: 20,
                       thresholds: [-50, 0, 50],
                       base: 0,
                       fixed: true,
                       xStep: layer.xStep,
                   }, [{
                       data: dpoData,
                       color: '#2567B9',
                   }]);
                   const legend = (shift) => {
                       if (shift < 0) {
                           shift = dpoData.length + shift;
                       }
                       return [{
                           label: `DPO(${N})`,
                           value: dpoData[shift],
                           color: '#2567B9',
                       }];
                   };
                   resolve({
                       qLayer,
                       legend,
                   });
               });
            });
        },
        emv(layer, data, start, end) {
            const { N = 12, M = 6 } = layer.params;
            const volume = [],
                low = [],
                high = [];
            let dataStart = start - N - M - 1;
            if (dataStart < 0) {
                dataStart = 0;
            }
            if (end > data.length) {
                end = data.length;
            }
            for (let i = dataStart; i < end; i++) {
                volume.push(data[i].volume);
                high.push(data[i].high);
                low.push(data[i].low);
            }
            return new Promise((resolve) => {
               getQuote('emv', {
                   high, low, volume, N, M
               }).then((result) => {
                  const { EMVValue, MAEMVValue } = result;
                  let dataLen = end - start;
                  let EMVData = [],
                      MAEMVData = [];
                  for (let i = EMVValue.length - dataLen; i < EMVValue.length; i++) {
                      EMVData.push(EMVValue[i]);
                      MAEMVData.push(MAEMVValue[i]);
                  }
                  const qLayer = new QuoteLineLayer(layer.canvas, {
                      color: '#666666',
                      fontSize: 20,
                      thresholds: [0],
                      base: 0,
                      fixed: true,
                      xStep: layer.xStep,
                  }, [{
                      data: EMVData,
                      color: '#D43323',
                  }, {
                      data: MAEMVData,
                      color: '#25D414',
                  }]);
                  const legend = (shift) => {
                      if (shift < 0) {
                          shift = EMVData.length + shift;
                      }
                      return [{
                          label: `EMV(${N})`,
                          color: '#D43323',
                          value: EMVData[shift],
                      }, {
                          label: `MA(${M})`,
                          color: '#25D414',
                          value: MAEMVData[shift],
                      }];
                  };
                  resolve({
                      qLayer,
                      legend
                  });
               });
            });
        },
        fi(layer, data, start, end) {
            const { N = 6 } = layer.params;
            const volume = [],
                close = [];
            let dataStart = start - N - 1;
            if (dataStart < 0) {
                dataStart = 0;
            }
            if (end > data.length) {
                end = data.length;
            }
            for (let i = dataStart; i < end; i++) {
                volume.push(data[i].volume);
                close.push(data[i].close);
            }
            return new Promise((resolve) => {
                getQuote('fi', {
                    close, volume, N
                }).then((result) => {
                    const { fi } = result;
                    const dataLen = end - start;
                    let fiData = [];
                    for (let i = fi.length - dataLen; i < fi.length; i++) {
                        fiData.push(fi[i]);
                    }
                    const qLayer = new QuoteLineLayer(layer.canvas, {
                        color: '#666666',
                        fontSize: 20,
                        thresholds: [0],
                        base: 0,
                        fixed: true,
                        xStep: layer.xStep,
                    }, [{
                        color: '#2567B9',
                        data: fiData
                    }]);
                    const legend = (shift) => {
                        if (shift < 0) {
                            shift = fiData.length + shift;
                        }
                        return [{
                            color: '#2567B9',
                            label: `FI(${N})`,
                            value: fiData[shift],
                        }];
                    };
                    resolve({
                        qLayer,
                        legend,
                    });
                });
            });
        },
        mfi(layer, data, start, end) {
            const { N = 24 } = layer.params;
            const volume = [],
                high = [],
                low = [],
                close = [];
            let dataStart =  start - N - 1;
            if (dataStart < 0) {
                dataStart = 0;
            }
            if (end > data.length) {
                end = data.length;
            }
            for (let i = dataStart; i < end; i++) {
                volume.push(data[i].volume);
                close.push(data[i].close);
                high.push(data[i].high);
                low.push(data[i].low);
            }
            return new Promise((resolve) => {
               getQuote('mfi', {
                   volume, high, low, close, N
               }).then((result) => {
                  const { mfiValue } = result;
                  const dataLen = end - start;
                  const mfiData = [];
                  for (let i = mfiValue.length - dataLen; i < mfiValue.length; i++) {
                      mfiData.push(mfiValue[i]);
                  }
                  const qLayer = new QuoteLineLayer(layer.canvas, {
                      color: '#666666',
                      fontSize: 20,
                      thresholds: [20, 50, 80],
                      base: 50,
                      fixed: true,
                      xStep: layer.xStep,
                  }, [{
                      data: mfiData,
                      color: '#2567B9',
                  }]);
                  const legend = (shift) => {
                    if (shift < 0) {
                        shift = mfiData.length + shift;
                    }
                    return [{
                        color: '#2567B9',
                        label: `MFI(${N})`,
                        value: mfiData[shift],
                    }];
                  };
                  resolve({
                      qLayer,
                      legend,
                  });
               });
            });
        },
        mtm(layer, data, start, end) {
            const { N = 12, M = 6 } = layer.params;
            const close = [];
            let dataStart = start - N - M - 1;
            if (dataStart < 0) {
                dataStart = 0;
            }
            if (end > data.length) {
                end = data.length;
            }
            for (let i = dataStart; i < end; i++) {
                close.push(data[i].close);
            }
            return new Promise((resolve) => {
               getQuote('mtm', {
                   close, N, M
               }).then((result) => {
                  const { mtmValue, mtmMaValue } = result;
                   const dataLen = end - start;
                   let mtmData = [], mtmMaData = [];
                   for (let i = mtmValue.length - dataLen; i < mtmValue.length; i++) {
                       mtmData.push(mtmValue[i]);
                       mtmMaData.push(mtmMaValue[i]);
                   }
                   const qLayer = new QuoteLineLayer(layer.canvas, {
                       color: '#666666',
                       fontSize: 20,
                       thresholds: [0],
                       base: 0,
                       fixed: true,
                       xStep: layer.xStep,
                   }, [{
                       color: '#D43323',
                       data: mtmData,
                   }, {
                       color: '#2567B9',
                       data: mtmMaData,
                   }]);
                   const legend = (shift) => {
                       if (shift < 0) {
                           shift = mtmData.length + shift;
                       }
                       return [{
                           color: '#D43323',
                           label: `MTM(${N})`,
                           value: mtmData[shift],
                       }, {
                           color: '#2567B9',
                           label: `MA(${M})`,
                           value: mtmMaData[shift],
                       }];
                   };
                   resolve({
                       qLayer,
                       legend,
                   });
               });
            });
        },
        nvi(layer, data, start, end) {
            const { M = 6 } = layer.params;
            const close = [], volume = [];
            if (end > data.length) {
                end = data.length;
            }
            for (let i = 0; i < end; i++) {
                close.push(data[i].close);
                volume.push(data[i].volume);
            }
            return new Promise((resolve) => {
               getQuote('nvi', {
                   close, volume, M
               }).then((result) => {
                   const { nviValue, nviMaValue } = result;
                   const nviData = [], nviMaData = [];
                   for (let i = start; i < end; i++) {
                       nviData.push(nviValue[i]);
                       nviMaData.push(nviMaValue[i]);
                   }
                   const qLayer = new QuoteLineLayer(layer.canvas, {
                       color: '#666666',
                       fontSize: 20,
                       thresholds: [100],
                       xStep: layer.xStep,
                   }, [{
                       color: '#25D414',
                       data: nviData,
                   }, {
                       color: '#2567B9',
                       data: nviMaData,
                   }]);
                   const legend = (shift) => {
                       if (shift < 0) {
                           shift = nviData.length + shift;
                       }
                       return [{
                           color: '#25D414',
                           label: `NVI`,
                           value: nviData[shift],
                       }, {
                           color: '#2567B9',
                           label: `MA(${M})`,
                           value: nviMaData[shift],
                       }];
                   };
                   resolve({
                       qLayer,
                       legend,
                   });
               });
            });
        },
        obv(layer, data, start, end) {
            const { M = 12 } = layer.params;
            const close = [], volume = [];
            if (end > data.length) {
                end = data.length;
            }
            for (let i = 0; i < end; i++) {
                close.push(data[i].close);
                volume.push(data[i].volume);
            }
            return new Promise((resolve) => {
               getQuote('obv', {
                   close, volume, M
               }).then((result) => {
                   const { obvValue, obvMaValue } = result;
                   const obvData = [], obvMaData = [];
                   for (let i = start; i < end; i++) {
                       obvData.push(obvValue[i]);
                       obvMaData.push(obvMaValue[i]);
                   }
                   const qLayer = new QuoteLineLayer(layer.canvas, {
                       color: '#666666',
                       fontSize: 20,
                       thresholds: [0],
                       xStep: layer.xStep,
                   }, [{
                       color: '#D43323',
                       data: obvData,
                   }, {
                       color: '#2567B9',
                       data: obvMaData,
                   }]);
                   const legend = (shift) => {
                       if (shift < 0) {
                           shift = obvData.length + shift;
                       }
                       return [{
                           color: '#D43323',
                           label: `OBV`,
                           value: obvData[shift],
                       }, {
                           color: '#2567B9',
                           label: `MA(${M})`,
                           value: obvMaData[shift],
                       }];
                   };
                   resolve({
                       qLayer,
                       legend,
                   });
               });
            });
        },
        psy(layer, data, start, end) {
          const { N = 14, M = 6 } = layer.params;
          const close = [];
          let dataStart = start - N - M - 1;
          if (dataStart < 0) {
              dataStart = 0;
          }
          if (end > data.length) {
              end = data.length;
          }
          for (let i = dataStart; i < end; i++) {
              close.push(data[i].close);
          }
          return new Promise((resolve) => {
              getQuote('psy', {
                  close, N, M
              }).then((result) => {
                  const { psyValue, psyMaValue } = result;
                  const psyData = [], psyMaData = [];
                  const dataLen = end - start;
                  for (let i = psyValue.length - dataLen; i < psyValue.length; i++) {
                      psyData.push(psyValue[i]);
                      psyMaData.push(psyMaValue[i]);
                  }
                  const qLayer = new QuoteLineLayer(layer.canvas, {
                      color: '#666666',
                      fontSize: 20,
                      thresholds: [25, 50, 75],
                      max: 100,
                      min: 0,
                      xStep: layer.xStep,
                  }, [{
                      color: '#D43323',
                      data: psyData,
                  }, {
                      color: '#2567B9',
                      data: psyMaData,
                  }]);
                  const legend = (shift) => {
                      if (shift < 0) {
                          shift = psyData.length + shift;
                      }
                      return [{
                          color: '#D43323',
                          label: `PSY`,
                          value: psyData[shift],
                      }, {
                          color: '#2567B9',
                          label: `MA(${M})`,
                          value: psyMaData[shift],
                      }];
                  };
                  resolve({
                      qLayer,
                      legend,
                  });
              });
          });
        },
        pvi(layer, data, start, end) {
            const { M = 6 } = layer.params;
            const close = [], volume = [];
            if (end > data.length) {
                end = data.length;
            }
            for (let i = 0; i < end; i++) {
                close.push(data[i].close);
                volume.push(data[i].volume);
            }
            return new Promise((resolve) => {
                getQuote('pvi', {
                    close, volume, M
                }).then((result) => {
                    const { pviValue, pviMaValue } = result;
                    const pviData = [], pviMaData = [];
                    for (let i = start; i < end; i++) {
                        pviData.push(pviValue[i]);
                        pviMaData.push(pviMaValue[i]);
                    }
                    const qLayer = new QuoteLineLayer(layer.canvas, {
                        color: '#666666',
                        fontSize: 20,
                        thresholds: [100],
                        xStep: layer.xStep,
                    }, [{
                        color: '#D43323',
                        data: pviData,
                    }, {
                        color: '#2567B9',
                        data: pviMaData,
                    }]);
                    const legend = (shift) => {
                        if (shift < 0) {
                            shift = pviData.length + shift;
                        }
                        return [{
                            color: '#D43323',
                            label: `PVI`,
                            value: pviData[shift],
                        }, {
                            color: '#2567B9',
                            label: `MA(${M})`,
                            value: pviMaData[shift],
                        }];
                    };
                    resolve({
                        qLayer,
                        legend,
                    });
                });
            });
        },
        roc(layer, data, start, end) {
            const { N = 14, M = 6 } = layer.params;
            let dataStart = start - N - M - 1;
            if (dataStart < 0) {
                dataStart = 0;
            }
            if (end > data.length) {
                end = data.length;
            }
            const close = [];
            console.log(start, dataStart, end);
            for (let i = dataStart; i < end; i++) {
                close.push(data[i].close);
            }
            return new Promise((resolve) => {
               getQuote('roc', {
                   close, N, M
               }).then((result) => {
                  const { rocValue, rocMaValue } = result;
                  const rocData = [];
                  const rocMaData = [];
                  const dataLen = end - start;
                  console.log(result);
                  for (let i = rocValue.length - dataLen; i < rocValue.length; i++) {
                      rocData.push(rocValue[i]);
                      rocMaData.push(rocMaValue[i]);
                  }
                   const qLayer = new QuoteLineLayer(layer.canvas, {
                       color: '#666666',
                       fontSize: 20,
                       thresholds: [0],
                       fixed: true,
                       base: 0,
                       xStep: layer.xStep,
                   }, [{
                       color: '#D43323',
                       data: rocData,
                   }, {
                       color: '#2567B9',
                       data: rocMaData,
                   }]);
                   const legend = (shift) => {
                       if (shift < 0) {
                           shift = rocData.length + shift;
                       }
                       return [{
                           color: '#D43323',
                           label: `ROC`,
                           value: rocData[shift],
                       }, {
                           color: '#2567B9',
                           label: `MA(${M})`,
                           value: rocMaData[shift],
                       }];
                   };
                   resolve({
                       qLayer,
                       legend,
                   });
               });
            });
        },
        rvi(layer, data, start, end) {
            const { S = 10, N = 14, M = 6 } = layer.params;
            let dataStart = start - S - N - M - 1;
            if (dataStart < 0) {
                dataStart = 0;
            }
            if (end > data.length) {
                end = data.length;
            }
            const close = [];
            for (let i = dataStart; i < end; i++) {
                close.push(data[i].close);
            }
            return new Promise((resolve) => {
               getQuote('rvi', {
                   close, S, N, M
               }).then((result) => {
                  const { rviValue, rviMaValue } = result;
                  const rviData = [], rviMaData = [];
                  const dataLen = end - start;
                  for (let i = rviValue.length - dataLen; i < rviValue.length; i++) {
                      rviData.push(rviValue[i]);
                      rviMaData.push(rviMaValue[i]);
                  }
                   const qLayer = new QuoteLineLayer(layer.canvas, {
                       color: '#666666',
                       fontSize: 20,
                       thresholds: [50],
                       fixed: true,
                       base: 50,
                       xStep: layer.xStep,
                   }, [{
                       color: '#D43323',
                       data: rviData,
                   }, {
                       color: '#2567B9',
                       data: rviMaData,
                   }]);
                   const legend = (shift) => {
                       if (shift < 0) {
                           shift = rviData.length + shift;
                       }
                       return [{
                           color: '#D43323',
                           label: `RVI`,
                           value: rviData[shift],
                       }, {
                           color: '#2567B9',
                           label: `MA(${M})`,
                           value: rviMaData[shift],
                       }];
                   };
                   resolve({
                       qLayer,
                       legend,
                   });
               });
            });
        },
        trix(layer, data, start, end) {
            const { N = 12, S = 6 } = layer.params;
            let dataStart = start - 3 * N - S - 1;
            if (dataStart < 0) {
                dataStart = 0;
            }
            if (end > data.length) {
                end = data.length;
            }
            const close = [];
            for (let i = dataStart; i < end; i++) {
                close.push(data[i].close);
            }
            return new Promise((resolve) => {
               getQuote('trix', {
                   close, N, S
               }).then((result) => {
                   const { trixValue, matrixValue } = result;
                   const trixData = [];
                   const trixMaData = [];
                   const dataLen = end - start;
                   for (let i = trixValue.length - dataLen; i < trixValue.length; i++) {
                       trixData.push(trixValue[i]);
                       trixMaData.push(matrixValue[i]);
                   }
                   const qLayer = new QuoteLineLayer(layer.canvas, {
                       color: '#666666',
                       fontSize: 20,
                       thresholds: [0],
                       fixed: true,
                       base: 0,
                       xStep: layer.xStep,
                   }, [{
                       color: '#D43323',
                       data: trixData,
                   }, {
                       color: '#2567B9',
                       data: trixMaData,
                   }]);
                   const legend = (shift) => {
                       if (shift < 0) {
                           shift = trixData.length + shift;
                       }
                       return [{
                           color: '#D43323',
                           label: `TRIX`,
                           value: trixData[shift],
                       }, {
                           color: '#2567B9',
                           label: `MA(${M})`,
                           value: trixMaData[shift],
                       }];
                   };
                   resolve({
                       qLayer,
                       legend,
                   });
               })
            });
        },
        vhf(layer, data, start, end) {
            const { N = 24 } = layer.params;
            let dataStart = start - N - 1;
            if (dataStart < 0) {
                dataStart = 0;
            }
            if (end > data.length) {
                end = data.length;
            }
            const close = [];
            for (let i = dataStart; i < end; i++) {
                close.push(data[i].close);
            }
            return new Promise((resolve) => {
                getQuote('vhf', {
                    close, N
                }).then((result) => {
                    const { vhfValue } = result;
                    const vhfData = [];
                    const dataLen = end - start;
                    for (let i = vhfValue.length - dataLen; i < vhfValue.length; i++) {
                        vhfData.push(vhfValue[i]);
                    }
                    const qLayer = new QuoteLineLayer(layer.canvas, {
                        color: '#666666',
                        fontSize: 20,
                        thresholds: [0.5],
                        fixed: true,
                        base: 0.5,
                        max: 1,
                        min: 0,
                        xStep: layer.xStep,
                    }, [{
                        color: '#2567B9',
                        data: vhfData,
                    }]);
                    const legend = (shift) => {
                        if (shift < 0) {
                            shift = vhfData.length + shift;
                        }
                        return [{
                            color: '#2567B9',
                            label: `VHF(${N})`,
                            value: vhfData[shift],
                        }];
                    };
                    resolve({
                        qLayer,
                        legend,
                    });
                });
            });
        },
        vr(layer, data, start, end) {
            const { N = 12, M = 6 } = layer.params;
            let dataStart = start - N - M - 1;
            if (dataStart < 0) {
                dataStart = 0;
            }
            if (end > data.length) {
                end = data.length;
            }
            const close = [], volume = [];
            for (let i = dataStart; i < end; i++) {
                close.push(data[i].close);
                volume.push(data[i].volume);
            }
            return new Promise((resolve) => {
               getQuote('vr', {
                   close, volume, N, M
               }).then((result) => {
                  const { vrValue, vrMaValue } = result;
                  const dataLen = end - start;
                  const vrData = [], vrMaData = [];
                  for (let i = vrValue.length - dataLen; i< vrValue.length; i++) {
                      vrData.push(vrValue[i]);
                      vrMaData.push(vrMaValue[i]);
                  }
                   const qLayer = new QuoteLineLayer(layer.canvas, {
                       color: '#666666',
                       fontSize: 20,
                       thresholds: [50],
                       fixed: true,
                       base: 50,
                       xStep: layer.xStep,
                   }, [{
                       color: '#D43323',
                       data: vrData,
                   }, {
                       color: '#2567B9',
                       data: vrMaData,
                   }]);
                   const legend = (shift) => {
                       if (shift < 0) {
                           shift = vrData.length + shift;
                       }
                       return [{
                           color: '#D43323',
                           label: `VR(${N})`,
                           value: vrData[shift],
                       }, {
                           color: '#2567B9',
                           label: `MA(${M})`,
                           value: vrMaData[shift],
                       }];
                   };
                   resolve({
                       qLayer,
                       legend,
                   });
               });
            });
        },
        wad(layer, data, start, end) {
            const { M = 6 } = layer.params;
            let dataStart = start - M - 1;
            if (dataStart < 0) {
                dataStart = 0;
            }
            if (end > data.length) {
                end = data.length;
            }
            const close = [], high = [], low = [];
            for (let i = dataStart; i < end; i++) {
                close.push(data[i].close);
                high.push(data[i].high);
                low.push(data[i].low);
            }
            console.log('close====>', close, high, low);
            return new Promise((resolve) => {
               getQuote('wad', {
                   high, low, close, M
               }).then((result) => {
                   const { wadValue, wadMaValue } = result;
                   const dataLen = end - start;
                   const wadData = [], wadMaData = [];
                   for (let i = wadValue.length - dataLen; i < wadValue.length; i++) {
                       wadData.push(wadValue[i]);
                       wadMaData.push(wadMaValue[i]);
                   }
                   const qLayer = new QuoteLineLayer(layer.canvas, {
                       color: '#666666',
                       fontSize: 20,
                       thresholds: [0],
                       fixed: true,
                       base: 0,
                       xStep: layer.xStep,
                   }, [{
                       color: '#D43323',
                       data: wadData,
                   }, {
                       color: '#2567B9',
                       data: wadMaData,
                   }]);
                   const legend = (shift) => {
                       if (shift < 0) {
                           shift = wadData.length + shift;
                       }
                       return [{
                           color: '#D43323',
                           label: `WVAD`,
                           value: wadData[shift],
                       }, {
                           color: '#2567B9',
                           label: `MA(${M})`,
                           value: wadMaData[shift],
                       }];
                   };
                   resolve({
                       qLayer,
                       legend,
                   });
               });
            });
        },
        wms(layer, data, start, end) {
            const { N = 24 } = layer.params;
            let dataStart = start - N - 1;
            if (dataStart < 0) {
                dataStart = 0;
            }
            if (end > data.length) {
                end = data.length;
            }
            const high = [], low = [], close = [];
            for (let i = dataStart; i < end; i++) {
                high.push(data[i].high);
                low.push(data[i].low);
                close.push(data[i].close);
            }
            return new Promise((resolve) => {
               getQuote('wms', {
                   high, low, close, N,
               }).then((result) => {
                  const { wmsValue } = result;
                  const dataLen = end - start;
                  const wmsData = [];
                  for (let i = wmsValue.length - dataLen; i < wmsValue.length; i++) {
                      wmsData.push(wmsValue[i]);
                  }
                   const qLayer = new QuoteLineLayer(layer.canvas, {
                       color: '#666666',
                       fontSize: 20,
                       thresholds: [50],
                       fixed: true,
                       base: 50,
                       max: 100,
                       min: 0,
                       xStep: layer.xStep,
                   }, [{
                       color: '#2567B9',
                       data: wmsData,
                   }]);
                   const legend = (shift) => {
                       if (shift < 0) {
                           shift = wmsData.length + shift;
                       }
                       return [{
                           color: '#2567B9',
                           label: `WMS(${N})`,
                           value: wmsData[shift],
                       }];
                   };
                   resolve({
                       qLayer,
                       legend,
                   });
               });
            });
        },
        wvad(layer, data, start, end) {
            const { N = 24, M = 6 } = layer.params;
            let dataStart = start - N - M -1;
            if (dataStart < 0) {
                dataStart = 0;
            }
            if (end > data.length) {
                end = data.length;
            }
            const open = [], high = [], low = [], close = [], volume = [];
            for (let i = dataStart; i < end; i++) {
                open.push(data[i].open);
                high.push(data[i].high);
                low.push(data[i].low);
                close.push(data[i].close);
                volume.push(data[i].volume);
            }
            return new Promise((resolve) => {
               getQuote('wvad', {
                   open, high, low, close, volume, N, M
               }).then((result) => {
                  const { wvadValue, wvadMaValue } = result;
                  const wvadData = [], wvadMaData = [];
                  const dataLen = end - start;
                  for (let i = wvadValue.length - dataLen; i < wvadValue.length; i++) {
                      wvadData.push(wvadValue[i]);
                      wvadMaData.push(wvadMaValue[i]);
                  }
                   const qLayer = new QuoteLineLayer(layer.canvas, {
                       color: '#666666',
                       fontSize: 20,
                       thresholds: [0],
                       fixed: true,
                       base: 0,
                       xStep: layer.xStep,
                   }, [{
                       color: '#D43323',
                       data: wvadData,
                   }, {
                       color: '#2567B9',
                       data: wvadMaData,
                   }]);
                   const legend = (shift) => {
                       if (shift < 0) {
                           shift = wvadData.length + shift;
                       }
                       return [{
                           color: '#D43323',
                           label: `WAD`,
                           value: Number(wvadData[shift]).toFixed(2),
                       }, {
                           color: '#2567B9',
                           label: `MA(${M})`,
                           value: Number(wvadMaData[shift]).toFixed(2),
                       }];
                   };
                   resolve({
                       qLayer,
                       legend,
                   });
               });
            });
        }

    };
    return new Promise((resolve) => {
        quotes[name](layer, data, start, end).then(result => {
            const { qLayer, legend } = result;
            layer.quoteLayer = qLayer;
            layer.legendCallback = legend;
            layer.childs.splice(0, layer.childs.length);
            layer.addChild(qLayer);
            qLayer.make();
            resolve(1);
        });
    })

}

export default class extends Layer {
    constructor(canvas, style, data) {
        super(canvas, style);
        this.data = data || [];
        this.dataStart = style.dataStart || 0;
        this.dataEnd = style.dataEnd || 0;
        this.name = style.name || 'macd';
        this.params = style.params || {};
        this.xStep = style.xStep || 1;
        this.legendIndex = style.legendIndex || -1;
    }

    setQuoteEmitter(quoteEmitter) {
        this.quoteEmmiter = quoteEmitter;
    }

    make() {
        this.childs.splice(0, this.childs.length);
        return new Promise((resolve) => {
            quoteCaculator(this, this.data, this.dataStart, this.dataEnd).then(res => {
                this.quoteEmmiter && this.quoteEmmiter(this.legendCallback && this.legendCallback(this.legendIndex));
                resolve(1);
            });
        });


    }

    containsPoint (point) {
        return point.x >= this.position.x
            && point.x <= this.position.x + this.width
            && point.y >= this.position.y
            && point.y <= this.position.y + this.height
    }
}
