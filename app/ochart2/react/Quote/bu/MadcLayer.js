import Layer from '../../../core/Layer';
import Point from '../../../core/Point';
import Rectangle from '../../../base/Rectangle';
import MultiLine from '../../../base/MultiLine';

export default class extends Layer {
    constructor(canvas, style, data) {
        super(canvas, style);
        this.xStep = style.xStep;
        this.data = data || {};
        this.barRed = style.barRed || '#F75A5A';
        this.barGreen = style.barGreen || '#008737';
        this.difColor = style.difColor || '#FF0000';
        this.deaColor = style.deaColor || '#3393B8';
    }

    make() {
        this.childs.splice(0, this.childs.length);
        if (this.data.DIF.length === 0) {
            return;
        }
        const { DIF, DEA, BAR } = this.data;
        let maxOfBar = BAR[0];
        let maxOfValue = DIF[0];
        for (let i = 0; i < DIF.length; i++) {
            if (Math.abs(DIF[i]) > maxOfValue) {
                maxOfValue = Math.abs(DIF[i]);
            }
            if (Math.abs(DEA[i]) > maxOfValue) {
                maxOfValue = Math.abs(DEA[i]);
            }
            if (Math.abs(BAR[i]) > maxOfBar) {
                maxOfBar = Math.abs(BAR[i]);
            }
        }
        const stepOfBar = this.height * 0.9 / maxOfBar / 2;
        const stepOfValue = this.height * 0.9 / maxOfValue / 2;
        const DIFPoint = [];
        const DEAPoint = [];
        for (let i = 0; i < DIF.length; i++) {
            DIFPoint.push(
                new Point(
                    this.position.x + i * this.xStep,
                    this.position.y + (DIF[i] + maxOfValue) * stepOfValue
                )
            );
            DEAPoint.push(
                new Point(
                    this.position.x + i * this.xStep,
                    this.position.y + (DEA[i] + maxOfValue) * stepOfValue
                )
            );
            if (BAR[i] > 0) {
                const barHeight = stepOfBar * BAR[i];
                const rect = new Rectangle(this.canvas, {
                    width: this.xStep * 0.2,
                    height: barHeight,
                    color: this.barRed,
                    type: Rectangle.TYPE.FILL,
                    position: new Point(
                        (i + 0.4) * this.xStep,
                        this.height / 2 + barHeight / 2
                    ),
                });
                this.addChild(rect);
            } else {
                const barHeight = stepOfBar * Math.abs(BAR[i]);
                const rect = new Rectangle(this.canvas, {
                    width: this.xStep * 0.2,
                    height: barHeight,
                    color: this.barGreen,
                    type: Rectangle.TYPE.FILL,
                    position: new Point(
                        (i + 0.4) * this.xStep,
                        this.height / 2 - barHeight / 2
                    ),
                });
                this.addChild(rect);
            }
        }

        const DIFLine = new MultiLine(this.canvas, {
            lineWidth: 2,
            color: this.difColor,
        }, DIFPoint);
        const DEALine = new MultiLine(this.canvas, {
            lineWidth: 2,
            color: this.deaColor,
        }, DEAPoint);
        this.addChild(DIFLine, DEALine);
    }

}
