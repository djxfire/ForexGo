/*
* @Date: 2020/5/18
* @Author: XueChengwu <xuechengwu@erayt.com>
* @Copyright: 2015-2019 Erayt, Inc.
* @Description: If you have some questions, please contact: xuechengwu@erayt.com.
*/
import React from 'react';
import AxisLayer from '../../layer/AxisLayer';
import Canvas from '../../core/Canvas';
import Point from '../../core/Point';
import KBarLayer from './bu/KBarLayer';
import Event from '../../event/Event';
import Bar from './bu/KBar';
import AccelerateAction from '../../action/AccelerateAction';
import IndicatorLayer from './bu/IndicatorLayer';
import DrawLayer from '../../layer/draw';
import OChart from '../../core/OChart';

export default class extends React.Component {
  static BAR_TYPE = {
    FILL: Bar.BAR_TYPE.FILL,
    STROKE: Bar.BAR_TYPE.STROKE,
  }
  constructor(props) {
    super(props);
    this.ref = React.createRef();
    this.indicators = []
  }

  drawLine = (options) => {
    // 线条绘制监听
    const x1 = Math.round(
        (this.barLayer.barEnd - this.barLayer.barStart) / 3 + this.barLayer.barStart
    );
    const x2 = Math.round(
        (this.barLayer.barEnd - this.barLayer.barStart) / 3 * 2 + this.barLayer.barStart
    );
    const price1 = (this.barLayer.max - this.barLayer.min) / 3 + this.barLayer.min;
    const price2 = (this.barLayer.max - this.barLayer.min) / 3 * 2 + this.barLayer.min;
    const point1 = {
      date: this.barLayer.data[x1].datetime,
      price: price1,
    };
    const point2 = {
      date: this.barLayer.data[x2].datetime,
      price: price2,
    };
    const target = {
      point1,
      point2,
      type: options.type,
    };
    if (options.type === 8) {
      target.point3 = {
        date: this.barLayer.data[Math.round((x1 + x2) / 2)].datetime,
        price1: (price1 + price2) * 0.75
      };
    }
    target.color = options.color;
    target.isSelected = true;
    this.drawLayer.clearSelected();
    this.drawLayer.lines.push(target);
    this.drawLayer.make();
    this.canvas.paint();
    options.onCreated && options.onCreated(this.drawLayer.lines);
  }

  onDrawLineMove = ({callback}) => {
    this.drawLayer.onLineMove = (line) => {
      console.log('on locked');
      this.barLayer.locked = true;
      callback && callback(line);
    };
  }

  onDrawLineMoved = ({callback}) => {
    this.drawLayer.onLineMoved = (line, description) => {
      this.barLayer.locked = false;
      callback && callback(line, description);
    };
  }

  deleteLine = ({uuid}) => {
    const index = this.drawLayer.lines.findIndex(item => item.uuid === uuid);
    if (index >= 0) {
      this.drawLayer.lines.splice(index, 1);
      this.drawLayer.make();
      this.canvas.paint();
    }
  }

  onDrawLineSelected = ({callback}) => {
    this.drawLayer.onSelected = callback;
  }

  editLine = (options) => {
    const index = this.drawLayer.lines.findIndex(item => item.uuid === options.uuid);
    if(index >= 0){
      this.drawLayer.lines[index].color = options.color || this.drawLayer.lines[index].color;
      this.drawLayer.make();
      this.canvas.paint();
    }
  }

  addQuote = (options) => {
    const { indicatorLayers = [] } = this.barLayer;
    const { style } = this.props;
    indicatorLayers.push({
      id: options.id,
      layer: new IndicatorLayer(this.canvas, {
        height: (this.canvas.height - style.xFontSize * this.canvas.ratio) * 0.9,
        name: options.name,
        params: options.params,
        position: new Point(0, style.xFontSize * this.canvas.ratio * 0.95 + 0.05  * this.canvas.height),
      }, this.barLayer.data),
    });
    this.indicators.push(options);
    this.barLayer.indicatorLayers = indicatorLayers;
    this.barLayer.make();
    this.canvas.paint();
  }

  removeQuote = (options) => {
    const { id } = options;
    const { indicatorLayers = [] } = this.barLayer;
    const indicatorIndex = this.indicators.findIndex(vo => vo.id === id);
    const indicatorLayerIndex = indicatorLayers.findIndex(vo => vo.id === id);
    if (indicatorIndex >= 0) {
      this.indicators.splice(indicatorIndex, 1);
    }
    if (indicatorLayerIndex >= 0) {
      indicatorLayers.splice(indicatorLayerIndex, 1);
    }
    this.barLayer.indicatorLayers = indicatorLayers;
    this.barLayer.make();
    this.canvas.paint();
  }

  editQuote = (options) => {
    const { id, params } = options;
    const { indicatorLayers = [] } = this.barLayer;
    const indicatorIndex = this.indicators.findIndex(vo => vo.id === id);
    const indicatorLayerIndex = indicatorLayers.findIndex(vo => vo.id === id);
    if (indicatorIndex >= 0) {
      this.indicators[indicatorIndex].params = params;
    }
    if (indicatorLayerIndex >= 0) {
      indicatorLayers[indicatorLayerIndex].layer.params = params;
    }
    this.barLayer.indicatorLayers = indicatorLayers;
    this.barLayer.make();
    this.canvas.paint();
  }

  componentWillReceiveProps (nextProps, nextContext) {
    const { data = [], name = '' } = nextProps;
    if (this.props.data !== data) {
      this.barLayer.data = data;
      this.drawLayer.xAxis = data.map(item => item.datetime);
      this.barLayer.make();
      this.canvas.paint();
    }
    if (this.props.name !== name) {
      OChart.remove(`${this.props.name}:drawLine`, this.drawLine);
      OChart.remove(`${this.props.name}:onDrawLineMove`, this.onDrawLineMove);
      OChart.remove(`${this.props.name}:onDrawLineMoved`, this.onDrawLineMoved);
      OChart.remove(`${this.props.name}:deleteLine`, this.deleteLine);
      OChart.remove(`${this.props.name}:onDrawLineSelected`, this.onDrawLineSelected);
      OChart.remove(`${this.props.name}:editLine`, this.editLine);
      OChart.remove(`${this.props.name}:addQuote`, this.addQuote);
      OChart.remove(`${this.props.name}:removeQuote`, this.removeQuote);
      OChart.remove(`${this.props.name}:editQuote`, this.editQuote);
      OChart.on(`${name}:drawLine`, this.drawLine);
      OChart.on(`${name}:onDrawLineMove`, this.onDrawLineMove);
      OChart.on(`${name}:onDrawLineMoved`, this.onDrawLineMoved);
      OChart.on(`${name}:deleteLine`, this.deleteLine);
      OChart.on(`${name}:onDrawLineSelected`, this.onDrawLineSelected);
      OChart.on(`${name}:editLine`, this.editLine);
      OChart.on(`${name}:addQuote`, this.addQuote);
      OChart.on(`${name}:removeQuote`, this.removeQuote);
      OChart.on(`${name}:editQuote`, this.editQuote);
    }
  }

  componentDidMount () {
    const { style = { xFontSize: 20, yFontSize: 20 }, lines = [],  data = [], name, } = this.props;
    const { indicators = [] } = this;
    this.canvas = new Canvas({
      ele: this.ref.current,
      canAction: false,
    });
    // 坐标系基础配置
    this.axisLayer = new AxisLayer(this.canvas, {
      yAxisType: AxisLayer.AxisType.NUMBER, // y轴为数值型
      xAxisType: AxisLayer.AxisType.LABEL,  // x轴时间为字符型
      xAxisGraduations: style.xAxis || 5,   // 网格5列
      yAxisGraduations: style.yAxis || 5,   // 网格5行
      xAxisPosition: AxisLayer.AxisPosition.BLOCK,  // X轴坐标不计算
      yAxisPosition: AxisLayer.AxisPosition.INNER,  // Y轴坐标计算
      yAxisRender: (value) => {
        const enob = style.enob || 2;
        return {
          text: Number(value).toFixed(enob),
          size: Number(style.yFontSize || 20),
          color: style.axisColor || '#999999',
          font: style.fontFamily || 'PingFang SC',
        };
      },
      xAxisRender: (label) => {
        const { value } = label;
        return {
          text: value,
          size: Number(style.xFontSize || 20),
          color: style.axisColor || '#999999',
          font: style.fontFamily || 'PingFang SC',
        };
      },
      color: style.color,
    });
    // K线坐标
    this.barLayer = new KBarLayer(this.canvas, {
      height: (this.canvas.height - style.xFontSize * this.canvas.ratio) * 0.9, // 预留20%的空白空间
      positiveColor: style.positiveColor,
      negativeColor: style.negativeColor,
      positiveType: style.positiveType,
      negativeType: style.negativeType,
      position: new Point(0, style.xFontSize * this.canvas.ratio * 0.95 + 0.05 * this.canvas.height), // 预留的10% + 坐标的高度
    }, data);
    const indicatorLayers = [];
    for (let i = 0; i < indicators.length; i++) {
      indicatorLayers.push({
        id: indicators[i].id,
        layer: new IndicatorLayer(this.canvas, {
          height: (this.canvas.height - style.xFontSize * this.canvas.ratio) * 0.9,
          name: indicators[i].name,
          params: indicators[i].params,
          position: new Point(0, style.xFontSize * this.canvas.ratio * 0.95 + 0.05  * this.canvas.height),
        }, data),
      });
    }
    this.barLayer.indicatorLayers = indicatorLayers;
    this.drawLayer = new DrawLayer(this.canvas, {
      width: this.canvas.width,
      height: this.canvas.height,
      lines,
      onLineMove: () => {
        console.log('on lock...');
        this.barLayer.locked = true;
      },
      onLineMoved: () => {
        console.log('moved.....')
        this.barLayer.locked = false;
      },
      position: new Point(0, 0),
    })
    this.barLayer.onMaked = (layer, option) => {
      const { max, min, barWidth, yDelta, start, end } = option;
      // 计算坐标的最大值与最小值，加减预留部分的数值
      let yAxisMax = max + (this.canvas.height - style.xFontSize * this.canvas.ratio) * 0.05 / yDelta;
      let yAxisMin = min - (this.canvas.height - style.xFontSize * this.canvas.ratio) * 0.05 / yDelta;
      this.axisLayer.yAxisMin = yAxisMin;
      this.axisLayer.yAxisMax = yAxisMax;
      this.drawLayer.start = start;
      this.drawLayer.end = end;
      this.drawLayer.min = yAxisMin;
      this.drawLayer.max = yAxisMax;
      this.drawLayer.xStep = barWidth;
      this.drawLayer.yStep = yDelta;
      // 设置X轴时间的坐标
      let dataWidth = (end - start) * barWidth;
      // 假设间距为100个画布像素
      let dataNum = Math.round(dataWidth / 500);
      // 计算100画布像素索引距离
      let indexStep = Math.round(500 / barWidth);
      // x轴坐标数组
      let xAxisData = [];
      for (let i = 0; i < dataNum; i++) {
        const date = layer.data[end - 1 - i * indexStep].datetime;
        xAxisData.unshift({
          value: date,
          position: new Point((end - 1 - i * indexStep - start) * barWidth, 0),
        });
      }
      this.axisLayer.xAxisLabels = xAxisData;
      OChart.emit(`${name}:onChartMaked`, {
        xStep: barWidth,
        yStep: yDelta,
        start,
        end,
        yAxisMax,
        yAxisMin,
      });
      this.axisLayer.make();
      this.drawLayer.make();
    }
    this.canvas.addChild(
        this.axisLayer, this.barLayer,
        this.drawLayer,
    );
    this.barLayer.make();
    this.canvas.paint();
    // 监听拖动事件
    this.barLayer.addEventListener(Event.EVENT_DRAG, (e) => {
      this.onChartDrag(e);
    });
    this.barLayer.addEventListener(Event.EVENT_DRAG_END, (e) => {
      this.onChartDragEnd(e);
    });
    // 监听滚轮K线缩放
    this.barLayer.addEventListener(Event.EVENT_WHEEL, (e) => {
      this.onChartScale(e);
    });

    OChart.on(`${name}:drawLine`, this.drawLine);
    OChart.on(`${name}:onDrawLineMove`, this.onDrawLineMove);
    OChart.on(`${name}:onDrawLineMoved`, this.onDrawLineMoved);
    OChart.on(`${name}:deleteLine`, this.deleteLine);
    OChart.on(`${name}:onDrawLineSelected`, this.onDrawLineSelected);
    OChart.on(`${name}:editLine`, this.editLine);
    OChart.on(`${name}:addQuote`, this.addQuote);
    OChart.on(`${name}:removeQuote`, this.removeQuote);
    OChart.on(`${name}:editQuote`, this.editQuote);
  }

  /**
   * 图表缩放
   * @param e
   */
  onChartScale = (e) => {
    if(this.barLayer.locked) {
      return;
    }
    if (e.nativeEvent.wheelDeltaY < 0) {
      if (this.barLayer.showNum < this.barLayer.data.length) {
        this.barLayer.showNum = Math.round(this.barLayer.showNum * 1.1);
        this.barLayer.make();
        this.canvas.paint();
      }
    } else {
      if (this.barLayer.showNum > 20) {
        this.barLayer.showNum = Math.round(this.barLayer.showNum * 0.9);
        this.barLayer.make();
        this.canvas.paint();
      }
    }
  }

  onChartDrag = (e) => {
    if (this.barLayer.locked) {
      return;
    }
    if (
      this.barLayer.position.x + e.distanceX
      >= (this.barLayer.data.length - this.barLayer.showNum) * this.barLayer.barWidth
    ) {
      // 移动的距离超过所有数据的长度， 设置为最大长度
      this.barLayer.setPosition((this.barLayer.data.length - this.barLayer.showNum) * this.barLayer.barWidth, this.barLayer.position.y);
    } else if(this.barLayer.position.x <= - this.barLayer.width / 2) {
      // 至少保证K线数据占据半屏
      this.barLayer.setPosition(-this.barLayer.width / 2, this.barLayer.position.y);
    }else {
      // K线平移相应的距离
      this.barLayer.setPosition(this.barLayer.position.x + e.distanceX, this.barLayer.position.y);
    }
    this.barLayer.make();
    this.canvas.paint();
  }

  // 移动结束的关心效果
  onChartDragEnd = (e) => {
    if (this.barLayer.locked) {
      return;
    }
    const accelerate = e.speedX > 0 ? 3000 : -3000; // 加速度3000画布像素每秒
    const duration = Math.abs(e.speedX / accelerate);
    this.accelerateAction = new AccelerateAction(duration, {
      speedX: e.speedX,
      accelerateX: accelerate,
      beforeUpdate: (node, frame) => {
        if (node.position.x >= (this.barLayer.data.length - this.barLayer.showNum) * node.barWidth) {
          node.setPosition((this.barLayer.data.length - this.barLayer.showNum) * node.barWidth, node.position.y);
        } else if(node.position.x <= - this.barLayer.width / 2) {
          node.setPosition(- this.barLayer.width / 2, node.position.y);
        }
        node.make();
      }
    });
    this.barLayer.runAction(this.accelerateAction, (node, action) => {
      // 保证不移除画布
      if (node.position.x >= (this.barLayer.data.length - this.barLayer.showNum) * node.barWidth) {
        node.stopAction(action);
        node.setPosition((this.barLayer.data.length - this.barLayer.showNum) * node.barWidth, node.position.y);
      } else if (node.position.x <= - this.barLayer.width / 2) {
        node.stopAction(action);
        node.setPosition(- this.barLayer.width / 2, node.position.y);
      }
    });
  }

  componentWillUnmount() {
    OChart.remove(`${this.props.name}:drawLine`, this.drawLine);
    OChart.remove(`${this.props.name}:onDrawLineMove`, this.onDrawLineMove);
    OChart.remove(`${this.props.name}:onDrawLineMoved`, this.onDrawLineMoved);
    OChart.remove(`${this.props.name}:deleteLine`, this.deleteLine);
    OChart.remove(`${this.props.name}:onDrawLineSelected`, this.onDrawLineSelected);
    OChart.remove(`${this.props.name}:editLine`, this.editLine);
    OChart.remove(`${this.props.name}:editLine`, this.editLine);
    OChart.remove(`${this.props.name}:addQuote`, this.addQuote);
    OChart.remove(`${this.props.name}:removeQuote`, this.removeQuote);
    OChart.remove(`${this.props.name}:editQuote`, this.editQuote);
  }



  render() {
    const { className = ''} = this.props;
    return (
      <div className={className} ref={this.ref} />
    );
  }
}
