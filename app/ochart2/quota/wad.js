export default function wad(high, low, close) {
    let wadValue = [];
    for(let i = 0;i < high.length;i++){
        if(i === 0){
            wadValue[i] = 0;
        }
        if(i > 0){
            let trh = parseFloat(close[i-1]) > parseFloat(high[i]) ? parseFloat(close[i-1]) : parseFloat(high[i]);
            let trl = parseFloat(close[i-1]) > parseFloat(low[i]) ? parseFloat(low[i]):parseFloat(close[i-1]);
            let ad = 0;
            if(parseFloat(close[i]) > parseFloat(close[i-1])){
                ad = parseFloat(close[i]) - trl;
            }else if(parseFloat(close[i]) < parseFloat(close[i-1])){
                ad = parseFloat(close[i]) - trh;
            }
            wadValue[i] = wadValue[i - 1] + ad;
        }
    }
    return wadValue;
}
