import $V from './$V';

export default function ad(high, low, close, volume) {
    const adv = [];
    for (let i = 0; i < high.length; i++) {
        let shift = (close[i] - low[i]) - (high[i] - close[i]);
        let range = high[i] - low[i];
        let data = 0;
        if (range !== 0) {
            data = shift / range * volume[i];
        }
        if (i === 0) {
            adv[i] = data;
        } else {
            adv[i] = adv[i - 1] + data;
        }
    }
    return adv;
}
