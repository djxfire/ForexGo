import $V from './$V';
import ma from './ma';

export default function wvad(open, high, low, close, volume, length) {
    let vadValue = [];
    for(let i = 0; i < open.length; i++) {
        if (high[i] === low[i]) {
            vadValue[i] = 0;
        } else {
            vadValue[i] = (close[i] - open[i]) / (high[i] - low[i]) * volume[i];
        }

    }
    let wvadValue = $V.mul_constant(ma(vadValue,length), length);
    return wvadValue;
}
