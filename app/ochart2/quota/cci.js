import $V from './$V';
import ma from './ma';

export default function cci(high, low, close, length = 14) {
    let tp = [];
    for (let i = 0; i < high.length; i++) {
        tp[i] = (high[i] + low[i] + close[i]) / 3;
    }
    let matp = ma(tp, length);
    let md = [];
    for (let i = length; i < high.length; i++) {
        let sum = 0;
        for (let j = i - length; j < i; j++) {
            sum += Math.abs(tp[j] - matp[i])
        }
        md[i] = sum / length;
    }
    let result = $V.div($V.sub(tp, matp), $V.mul_constant(md, 0.015));
    for (let i = 0; i < result.length; i++) {
        if (isNaN(result[i])) {
            result[i] = 0;
        }
        if (result[i] === Infinity) {
            result[i] = 0;
        }
    }
    return result;
}
