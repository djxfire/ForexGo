import $V from './$V';
import ema from './ema';

export default function dma(price, fast, slow, smooth) {
    let dmaValue = $V.sub(ema(price, fast), ema(price, slow));
    let amaValue = ema(dmaValue, smooth);
    return { dmaValue, amaValue };
}
