import Layer from '../../core/Layer';
import Circle from '../../base/Circle';
import Line from '../../base/Line';
import Event from '../../event/Event';
import Point from '../../core/Point';

export default class extends Layer {
    constructor(canvas, style) {
        super(canvas, style);
        this.uuid = style.uuid;
        this.onLineMove = style.onLineMove;
        this.onLineMoved = style.onLineMoved;
        this.hasChange = false;
        this.circle1 = new Circle(this.canvas, {
            radius: 10,
            color: this.color,
            position: this.position,
            type: Circle.TYPE.FILL,
        });
        this.circle2 = new Circle(this.canvas, {
            radius: 10,
            color: this.color,
            position: style.point2,
            type: Circle.TYPE.FILL,
        });
        let xDist = this.circle2.position.x - this.circle1.position.x;
        let yDist = this.circle2.position.y - this.circle1.position.y;
        let targetY = this.circle1.position.y + yDist * (this.width - this.circle1.position.x) / xDist;
        let zeroY = this.circle1.position.y + yDist * (0 - this.circle1.position.x) / xDist;
        const line = new Line(this.canvas, {
            position: new Point(0, zeroY),
            to: new Point(this.width, targetY),
            color: this.color,
            lineWidth: 2,
        });
        this.addChild(line, this.circle1, this.circle2);
        this.circle1.addEventListener(Event.EVENT_TOUCH_MOVE, (e) => {
            this.childs.splice(0, this.childs.length);
            this.circle1.position = new Point(e.curPoint.x, e.curPoint.y);
            this.onLineMove && this.onLineMove(this);
            let xDist = this.circle2.position.x - this.circle1.position.x;
            let yDist = this.circle2.position.y - this.circle1.position.y;
            let targetY = this.circle1.position.y + yDist * (this.width - this.circle1.position.x) / xDist;
            let zeroY = this.circle1.position.y + yDist * (0 - this.circle1.position.x) / xDist;
            const line = new Line(this.canvas, {
                position: new Point(0, zeroY),
                to: new Point(this.width, targetY),
                color: this.color,
                lineWidth: 2,
            });
            this.addChild(line, this.circle1, this.circle2);
            this.hasChange = true;
            this.canvas.paint();
        });
        this.circle2.addEventListener(Event.EVENT_TOUCH_MOVE, (e) => {
            this.childs.splice(0, this.childs.length);
            this.circle2.position = new Point(e.curPoint.x, e.curPoint.y);
            this.onLineMove && this.onLineMove(this);
            let xDist = this.circle2.position.x - this.circle1.position.x;
            let yDist = this.circle2.position.y - this.circle1.position.y;
            let targetY = this.circle1.position.y + yDist * (this.width - this.circle1.position.x) / xDist;
            let zeroY = this.circle1.position.y + yDist * (0 - this.circle1.position.x) / xDist;
            const line = new Line(this.canvas, {
                position: new Point(0, zeroY),
                to: new Point(this.width, targetY),
                color: this.color,
                lineWidth: 2,
            });
            this.addChild(line, this.circle1, this.circle2);
            this.hasChange = true;
            this.canvas.paint();
        });
        this.circle1.addEventListener(Event.EVENT_TOUCH_END, (e) => {
            if (this.hasChange) {
                this.onLineMoved && this.onLineMoved(this);
            }
            this.hasChange = false;
        });
        this.circle2.addEventListener(Event.EVENT_TOUCH_END, (e) => {
            if (this.hasChange) {
                this.onLineMoved && this.onLineMoved(this);
            }
            this.hasChange = false;
        });
    }

    setSelected(isSelected) {
        this.circle1.visible = isSelected;
        this.circle2.visible = isSelected;
    }

    containsPoint (point) {
        let k = (this.circle1.position.y - this.circle2.position.y)
            / (this.circle1.position.x - this.circle2.position.x);
        let yDown = k * (point.x - this.circle2.position.x) + this.circle2.position.y - 10;
        let yUp = yDown + 20;
        if ((point.x - this.circle1.position.x)
            * (point.x - this.circle2.position.x) < 0 && (point.y - yDown) * (point.y - yUp) < 0) {
            return true;
        }
        return false;
    }
}
