import Layer from '../../core/Layer';
import Circle from '../../base/Circle';
import Line from '../../base/Line';
import Event from '../../event/Event';
import Point from '../../core/Point';


export default class extends Layer {
    constructor(canvas, style) {
        super(canvas, style);
        this.uuid = style.uuid;
        this.onLineMove = style.onLineMove;
        this.onLineMoved = style.onLineMoved;
        this.hasChange = false;
        this.circle1 = new Circle(this.canvas, {
            radius: 10,
            color: this.color,
            position: style.point1,
            type: Circle.TYPE.FILL,
        });
        this.circle2 = new Circle(this.canvas, {
            radius: 10,
            color: this.color,
            position: style.point2,
            type: Circle.TYPE.FILL,
        });
        this.circle3 = new Circle(this.canvas, {
            radius: 10,
            color: this.color,
            position: style.point3,
            type: Circle.TYPE.FILL,
        });
        let line = new Line(this.canvas, {
            position: this.circle1.position,
            color: this.color,
            lineWidth: 2,
            to: this.circle2.position,
        });
        let k = (this.circle1.position.y - this.circle2.position.y) / (this.circle1.position.x - this.circle2.position.x);
        let lineY = (this.circle3.position.x - this.circle1.position.x) * k + this.circle1.position.y;
        let dist = Math.abs(this.circle3.position.y - lineY);
        let line2 = new Line(this.canvas, {
            position: new Point(this.circle1.position.x, this.circle1.position.y + dist),
            color: this.color,
            lineWidth: 2,
            to: new Point(this.circle2.position.x, this.circle2.position.y + dist),
        });
        let line3 = new Point(this.canvas, {
            position: new Point(this.circle1.position.x, this.circle1.position.y - dist),
            color: this.color,
            lineWidth: 2,
            to: new Point(this.circle2.position.x, this.circle2.position.y - dist),
        });
        this.addChild(line, line2, line3, this.circle1, this.circle2, this.circle3);
        this.circle1.addEventListener(Event.EVENT_TOUCH_MOVE, (e) => {
            this.circle1.position = new Point(e.curPoint.x, e.curPoint.y);
            this.childs.splice(0, this.childs.length);
            this.onLineMove && this.onLineMove(this);
            let line = new Line(this.canvas, {
                position: this.circle1.position,
                color: this.color,
                lineWidth: 2,
                to: this.circle2.position,
            });
            let k = (this.circle1.position.y - this.circle2.position.y) / (this.circle1.position.x - this.circle2.position.x);
            let lineY = (this.circle3.position.x - this.circle1.position.x) * k + this.circle1.position.y;
            let dist = Math.abs(this.circle3.position.y - lineY);
            let line2 = new Line(this.canvas, {
                position: new Point(this.circle1.position.x, this.circle1.position.y + dist),
                color: this.color,
                lineWidth: 2,
                to: new Point(this.circle2.position.x, this.circle2.position.y + dist),
            });
            let line3 = new Point(this.canvas, {
                position: new Point(this.circle1.position.x, this.circle1.position.y - dist),
                color: this.color,
                lineWidth: 2,
                to: new Point(this.circle2.position.x, this.circle2.position.y - dist),
            });
            this.addChild(line, line2, line3, this.circle1, this.circle2, this.circle3);
            this.hasChange = true;
            this.canvas.paint();
        });
        this.circle2.addEventListener(Event.EVENT_TOUCH_MOVE, (e) => {
            this.circle2.position = new Point(e.curPoint.x, e.curPoint.y);
            this.childs.splice(0, this.childs.length);
            this.onLineMove && this.onLineMove(this);
            let line = new Line(this.canvas, {
                position: this.circle1.position,
                color: this.color,
                lineWidth: 2,
                to: this.circle2.position,
            });
            let k = (this.circle1.position.y - this.circle2.position.y) / (this.circle1.position.x - this.circle2.position.x);
            let lineY = (this.circle3.position.x - this.circle1.position.x) * k + this.circle1.position.y;
            let dist = Math.abs(this.circle3.position.y - lineY);
            let line2 = new Line(this.canvas, {
                position: new Point(this.circle1.position.x, this.circle1.position.y + dist),
                color: this.color,
                lineWidth: 2,
                to: new Point(this.circle2.position.x, this.circle2.position.y + dist),
            });
            let line3 = new Point(this.canvas, {
                position: new Point(this.circle1.position.x, this.circle1.position.y - dist),
                color: this.color,
                lineWidth: 2,
                to: new Point(this.circle2.position.x, this.circle2.position.y - dist),
            });
            this.addChild(line, line2, line3, this.circle1, this.circle2, this.circle3);
            this.hasChange = true;
            this.canvas.paint();
        });
        this.circle3.addEventListener(Event.EVENT_TOUCH_MOVE, (e) => {
            this.circle3.position = new Point(e.curPoint.x, e.curPoint.y);
            this.childs.splice(0, this.childs.length);
            this.onLineMove && this.onLineMove(this);
            let line = new Line(this.canvas, {
                position: this.circle1.position,
                color: this.color,
                lineWidth: 2,
                to: this.circle2.position,
            });
            let k = (this.circle1.position.y - this.circle2.position.y) / (this.circle1.position.x - this.circle2.position.x);
            let lineY = (this.circle3.position.x - this.circle1.position.x) * k + this.circle1.position.y;
            let dist = Math.abs(this.circle3.position.y - lineY);
            let line2 = new Line(this.canvas, {
                position: new Point(this.circle1.position.x, this.circle1.position.y + dist),
                color: this.color,
                lineWidth: 2,
                to: new Point(this.circle2.position.x, this.circle2.position.y + dist),
            });
            let line3 = new Point(this.canvas, {
                position: new Point(this.circle1.position.x, this.circle1.position.y - dist),
                color: this.color,
                lineWidth: 2,
                to: new Point(this.circle2.position.x, this.circle2.position.y - dist),
            });
            this.addChild(line, line2, line3, this.circle1, this.circle2, this.circle3);
            this.hasChange = true;
            this.canvas.paint();
        });
        this.circle1.addEventListener(Event.EVENT_TOUCH_END, (e) => {
            if (this.hasChange) {
                this.onLineMoved && this.onLineMoved(this);
            }
            this.hasChange = false;
        });
        this.circle2.addEventListener(Event.EVENT_TOUCH_END, (e) => {
            if (this.hasChange) {
                this.onLineMoved && this.onLineMoved(this);
            }
            this.hasChange = false;
        });
        this.circle3.addEventListener(Event.EVENT_TOUCH_END, (e) => {
            if (this.hasChange) {
                this.onLineMoved && this.onLineMoved(this);
            }
            this.hasChange = false;
        });
    }

    setSelected(isSelected) {
        this.circle1.visible = isSelected;
        this.circle2.visible = isSelected;
        this.circle3.visible = isSelected;
    }

    containsPoint (point) {
        let k = (this.circle1.position.y - this.circle2.position.y)
            / (this.circle1.position.x - this.circle2.position.x);
        let yDown = k * (point.x - this.circle2.position.x) + this.circle2.position.y - 10;
        let yUp = yDown + 20;
        if ((point.x - this.circle1.position.x) * (point.x - this.circle2.position.x) < 0 && (point.y - yDown) * (point.y - yUp) < 0) {
            return true;
        }
        return false;
    }
}
