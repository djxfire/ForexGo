import Layer from '../../core/Layer';
import Circle from '../../base/Circle';
import Line from '../../base/Line';
import Polygon from '../../base/Polygon';
import Point from '../../core/Point';
import Event from '../../event/Event';

let polyginColor = [
    '#23D4B2',
    '#33F1A1',
    '#F18681',
    '#F34443',
];
let rate = [2, 3, 4, 8]

export default class extends Layer {
    constructor(canvas, style) {
        super(canvas, style);
        this.uuid = style.uuid;
        this.onLineMove = style.onLineMove;
        this.onLineMoved = style.onLineMoved;
        this.onLineSelected = style.onLineSelected;
        this.hasChange = false;
        this.circle1 = new Circle(this.canvas, {
            radius: 10,
            color: this.color,
            position: this.position,
            type: Circle.TYPE.FILL,
        });
        this.circle2 = new Circle(this.canvas, {
            radius: 10,
            color: this.color,
            position: style.point2,
            type: Circle.TYPE.FILL,
        });
        let xDist = this.circle2.position.x - this.circle1.position.x;
        let yDist = this.circle2.position.y - this.circle1.position.y;
        let targetY = this.circle1.position.y + yDist * (this.width - this.circle1.position.x) / xDist;
        let line = new Line(this.canvas, {
            to: new Point(this.width, targetY),
            color: this.color,
            position: this.circle1.position,
        });
        this.addChild(line);
        // 绘制轴方向
        for (let i = 1; i <= 4; i++) {
            let y = this.circle1.position.y + (this.width - this.circle1.position.x) / xDist * yDist / rate[i - 1];
            let line = new Line(this.canvas, {
                to: new Point(this.width, y),
                color: this.color,
                position: this.circle1.position,
            });
            if (i === 1) {
                let polygon = new Polygon(this.canvas, {
                    type: Polygon.TYPE.FILL,
                    color: polyginColor[i - 1]
                }, [
                    this.circle1.position,
                    new Point(this.width, targetY),
                    new Point(this.width, y),
                ]);
                polygon.alpha = 0.3;
                this.addChild(polygon);
            } else {
                let lastY = this.circle1.position.y + (this.width - this.circle1.position.x) / xDist * yDist / rate[i - 2]
                let polygon = new Polygon(this.canvas, {
                    type: Polygon.TYPE.FILL,
                    color: polyginColor[i - 1],
                }, [
                    this.circle1.position,
                    new Point(this.width, y),
                    new Point(this.width, lastY),
                ]);
                polygon.alpha = 0.3;
                this.addChild(polygon);
            }
            this.addChild(line);
        }
        for (let i = 1; i <= 4; i++) {
            let ty = yDist > 0 ? this.height : 0;
            let x = this.circle1.position.x + (ty - this.circle1.position.y) / yDist * xDist / rate[i - 1];
            let line = new Line(this.canvas, {
                to: new Point(x, ty),
                color: this.color,
                position: this.circle1.position,
            });
            if (i === 1) {
                let polygon = new Polygon(this.canvas, {
                    type: Polygon.TYPE.FILL,
                    color: polyginColor[i - 1]
                }, [
                    this.circle1.position,
                    new Point(x, ty),
                    new Point(this.width, targetY),
                ]);
                polygon.alpha = 0.3;
                this.addChild(polygon);
            } else {
                let lastX = this.circle1.position.x + (ty - this.circle1.position.y) / yDist * xDist / rate[i - 2];
                let polygon = new Polygon(this.canvas, {
                    type: Polygon.TYPE.FILL,
                    color: polyginColor[i - 1],
                }, [
                    this.circle1.position,
                    new Point(x, ty),
                    new Point(lastX, ty),
                ]);
                polygon.alpha = 0.3;
                this.addChild(polygon);
            }
            this.addChild(line);
        }
        this.addChild(this.circle1, this.circle2);
        this.circle1.addEventListener(Event.EVENT_TOUCH_MOVE, (e) => {
            this.childs.splice(0, this.childs.length);
            this.circle1.position = new Point(e.curPoint.x, e.curPoint.y);
            let xDist = this.circle2.position.x - this.circle1.position.x;
            let yDist = this.circle2.position.y - this.circle1.position.y;
            let targetY = this.circle1.position.y + yDist * (this.width - this.circle1.position.x) / xDist;
            // 绘制轴方向
            for (let i = 1; i <= 4; i++) {
                let y = this.circle1.position.y + (this.width - this.circle1.position.x) / xDist * yDist / rate[i - 1];
                let line = new Line(this.canvas, {
                    to: new Point(this.width, y),
                    color: this.color,
                    position: this.circle1.position,
                });
                if (i === 1) {
                    let polygon = new Polygon(this.canvas, {
                        type: Polygon.TYPE.FILL,
                        color: polyginColor[i - 1]
                    }, [
                        this.circle1.position,
                        new Point(this.width, targetY),
                        new Point(this.width, y),
                    ]);
                    polygon.alpha = 0.3;
                    this.addChild(polygon);
                } else {
                    let lastY = this.circle1.position.y + (this.width - this.circle1.position.x) / xDist * yDist / rate[i - 2]
                    let polygon = new Polygon(this.canvas, {
                        type: Polygon.TYPE.FILL,
                        color: polyginColor[i - 1],
                    }, [
                        this.circle1.position,
                        new Point(this.width, y),
                        new Point(this.width, lastY),
                    ]);
                    polygon.alpha = 0.3;
                    this.addChild(polygon);
                }
                this.addChild(line);
            }
            for (let i = 1; i <= 4; i++) {
                let x = this.circle1.position.x + (this.height - this.circle1.position.y) / yDist * xDist / rate[i - 1];
                let line = new Line(this.canvas, {
                    to: new Point(x, this.height),
                    color: this.color,
                    position: this.circle1.position,
                });
                if (i === 1) {
                    let polygon = new Polygon(this.canvas, {
                        type: Polygon.TYPE.FILL,
                        color: polyginColor[i - 1]
                    }, [
                        this.circle1.position,
                        new Point(x, this.height),
                        new Point(this.width, targetY),
                    ]);
                    polygon.alpha = 0.3;
                    this.addChild(polygon);
                } else {
                    let lastX = this.circle1.position.x + (this.height - this.circle1.position.y) / yDist * xDist / rate[i - 2];
                    let polygon = new Polygon(this.canvas, {
                        type: Polygon.TYPE.FILL,
                        color: polyginColor[i - 1],
                    }, [
                        this.circle1.position,
                        new Point(x, this.height),
                        new Point(lastX, this.height),
                    ]);
                    polygon.alpha = 0.3;
                    this.addChild(polygon);
                }
            }
            this.addChild(this.circle1, this.circle2);
            this.canvas.paint();
            this.hasChange = true;
            this.onLineMove && this.onLineMove(this);
        });
        this.circle2.addEventListener(Event.EVENT_TOUCH_MOVE, (e) => {
            this.childs.splice(0, this.childs.length);
            this.circle2.position = new Point(e.curPoint.x, e.curPoint.y);
            let xDist = this.circle2.position.x - this.circle1.position.x;
            let yDist = this.circle2.position.y - this.circle1.position.y;
            let targetY = this.circle1.position.y + yDist * (this.width - this.circle1.position.x) / xDist;
            // 绘制轴方向
            for (let i = 1; i <= 4; i++) {
                let y = this.circle1.position.y + (this.width - this.circle1.position.x) / xDist * yDist / rate[i - 1];
                let line = new Line(this.canvas, {
                    to: new Point(this.width, y),
                    color: this.color,
                    position: this.circle1.position,
                });
                if (i === 1) {
                    let polygon = new Polygon(this.canvas, {
                        type: Polygon.TYPE.FILL,
                        color: polyginColor[i - 1]
                    }, [
                        this.circle1.position,
                        new Point(this.width, targetY),
                        new Point(this.width, y),
                    ]);
                    polygon.alpha = 0.3;
                    this.addChild(polygon);
                } else {
                    let lastY = this.circle1.position.y + (this.width - this.circle1.position.x) / xDist * yDist / rate[i - 2]
                    let polygon = new Polygon(this.canvas, {
                        type: Polygon.TYPE.FILL,
                        color: polyginColor[i - 1],
                    }, [
                        this.circle1.position,
                        new Point(this.width, y),
                        new Point(this.width, lastY),
                    ]);
                    polygon.alpha = 0.3;
                    this.addChild(polygon);
                }
                this.addChild(line);
            }
            for (let i = 1; i <= 4; i++) {
                let x = this.circle1.position.x + (this.height - this.circle1.position.y) / yDist * xDist / rate[i - 1];
                let line = new Line(this.canvas, {
                    to: new Point(x, this.height),
                    color: this.color,
                    position: this.circle1.position,
                });
                if (i === 1) {
                    let polygon = new Polygon(this.canvas, {
                        type: Polygon.TYPE.FILL,
                        color: polyginColor[i - 1]
                    }, [
                        this.circle1.position,
                        new Point(x, this.height),
                        new Point(this.width, targetY),
                    ]);
                    polygon.alpha = 0.3;
                    this.addChild(polygon);
                } else {
                    let lastX = this.circle1.position.x + (this.height - this.circle1.position.y) / yDist * xDist / rate[i - 2];
                    let polygon = new Polygon(this.canvas, {
                        type: Polygon.TYPE.FILL,
                        color: polyginColor[i - 1],
                    }, [
                        this.circle1.position,
                        new Point(x, this.height),
                        new Point(lastX, this.height),
                    ]);
                    polygon.alpha = 0.3;
                    this.addChild(polygon);
                }
            }
            this.addChild(this.circle1, this.circle2);
            this.canvas.paint();
            this.hasChange = true;
            this.onLineMove && this.onLineMove(this);
        });
        this.circle1.addEventListener(Event.EVENT_TOUCH_END, (e) => {
           if (this.hasChange) {
               this.onLineMoved && this.onLineMoved(this);
           }
           this.hasChange = false;
        });
        this.circle2.addEventListener(Event.EVENT_TOUCH_END, (e) => {
            if (this.hasChange) {
                this.onLineMoved && this.onLineMoved(this);
            }
            this.hasChange = false;
        })
    }

    setSelected(isSelected) {
        this.circle1.visible = isSelected;
        this.circle2.visible = isSelected;
    }

    containsPoint (point) {
        let k = (this.circle1.position.y - this.circle2.position.y)
            / (this.circle1.position.x - this.circle2.position.x);
        let yDown = k * (point.x - this.circle2.position.x) + this.circle2.position.y - 10;
        let yUp = yDown + 20;
        if ((point.x - this.circle1.position.x)
            * (point.x - this.circle2.position.x) < 0 && (point.y - yDown) * (point.y - yUp) < 0) {
            return true;
        }
        return false;
    }
}
