import Layer from '../../core/Layer';
import Circle from "../../base/Circle";
import Line from "../../base/Line";
import Event from '../../event/Event';
import Point from '../../core/Point';

export default class extends Layer {
    constructor(canvas, style) {
        super(canvas, style);
        this.uuid = style.uuid;
        this.onLineMove = style.onLineMove;
        this.onLineMoved = style.onLineMoved;
        this.hasChange = false;
        this.mesures = style.mesures || [];
        this.circle1 = new Circle(this.canvas, {
            radius: 10,
            color: this.color,
            position: style.point1,
            type: Circle.TYPE.FILL,
        });
        this.circle2 = new Circle(this.canvas, {
            radius: 10,
            color: this.color,
            position: style.point2,
            type: Circle.TYPE.FILL,
        });
        const dist = Math.abs(this.circle1.position.x - this.circle2.position.x);
        const line = new Line(this.canvas, {
            position: this.circle1.position,
            to: this.circle2.position,
            lineWidth: 2,
            color: this.color,
        });
        this.addChild(line, this.circle1, this.circle2);
        this.mesures.sort();
        const minX = this.circle1.position.x > this.circle2.position.x
          ? this.circle2.position.x
          : this.circle1.position.x;
        for(let i = 0; i < this.mesures.length; i++) {
            const targetX = minX + dist * this.mesures[i];
            const line = new Line(this.canvas, {
                position: new Point(targetX, 0),
                to: new Point(targetX, this.height),
                color: this.color,
            });
            this.addChild(line);
        }
        this.circle1.addEventListener(Event.EVENT_TOUCH_MOVE, (e) => {
           this.circle1.position = new Point(e.curPoint.x, e.curPoint.y);
           this.childs.splice(0, this.childs.length);
           this.onLineMove && this.onLineMove(this);
           this.hasChange = true;
            const dist = Math.abs(this.circle1.position.x - this.circle2.position.x);
            const line = new Line(this.canvas, {
                position: this.circle1.position,
                to: this.circle2.position,
                lineWidth: 2,
                color: this.color,
            });
            this.addChild(line, this.circle1, this.circle2);
            this.mesures.sort();
            for(let i = 0; i < this.mesures.length; i++) {
                const minX = this.circle1.position.x > this.circle2.position.x
                    ? this.circle2.position.x
                    : this.circle1.position.x;
                const targetX = minX + dist * this.mesures[i];
                const line = new Line(this.canvas, {
                    position: new Point(targetX, 0),
                    to: new Point(targetX, this.height),
                    color: this.color,
                });
                this.addChild(line);
            }
            this.hasChange = true;
            this.canvas.paint();
        });
        this.circle2.addEventListener(Event.EVENT_TOUCH_MOVE, (e) => {
            this.circle2.position = new Point(e.curPoint.x, e.curPoint.y);
            this.childs.splice(0, this.childs.length);
            this.onLineMove && this.onLineMove(this);
            this.hasChange = true;
            const dist = Math.abs(this.circle1.position.x - this.circle2.position.x);
            const line = new Line(this.canvas, {
                position: this.circle1.position,
                to: this.circle2.position,
                lineWidth: 2,
                color: this.color,
            });
            this.addChild(line, this.circle1, this.circle2);
            this.mesures.sort();
            for(let i = 0; i < this.mesures.length; i++) {
                const minX = this.circle1.position.x > this.circle2.position.x
                    ? this.circle2.position.x
                    : this.circle1.position.x;
                const targetX = minX + dist * this.mesures[i];
                const line = new Line(this.canvas, {
                    position: new Point(targetX, 0),
                    to: new Point(targetX, this.height),
                    color: this.color,
                });
                this.addChild(line);
            }
            this.hasChange = true;
            this.canvas.paint();
        });
    }

    setSelected(isSelected) {
        this.circle1.visible = isSelected;
        this.circle2.visible = isSelected;
    }

    containsPoint (point) {
        let k = (this.circle1.position.y - this.circle2.position.y)
            / (this.circle1.position.x - this.circle2.position.x);
        let yDown = k * (point.x - this.circle2.position.x) + this.circle2.position.y - 10;
        let yUp = yDown + 20;
        if ((point.x - this.circle1.position.x) * (point.x - this.circle2.position.x) < 0 && (point.y - yDown) * (point.y - yUp) < 0) {
            return true;
        }
        return false;
    }
}
