import Layer from '../../core/Layer';
import Point from '../../core/Point';
import Line from '../../base/Line';
import Circle from '../../base/Circle';


export default class extends Layer {
    constructor(canvas, style) {
        super(canvas, style);
        this.uuid = style.uuid;
        this.onLineMove = style.onLineMove;
        this.onLineMoved = style.onLineMoved;
        this.hasChange = false;
        this.circle1 = new Circle(this.canvas, {
            radius: 10,
            color: this.color,
            position: style.point1,
        });
        this.circle2 = new Circle(this.canvas, {
            radius: 10,
            color: this.color,
            position: style.point2,
        });
    }
}
