const webpack = require('webpack')
const path = require('path')
const HtmlWebpackPlugin = require('html-webpack-plugin')

module.exports = {
    entry: //path.join(__dirname, '/app/index.jsx'),
      {
          main: path.join(__dirname, '/app/index.js'),
          markvo: path.join(__dirname, '/app/workers/markvo.js'),
          quote: path.join(__dirname, '/app/workers/quote.js'),
      },
    output: {
        path: __dirname + '/dist',
        filename: '[name].js'
    },
    module:{
        rules:[{
            test:/\.jsx?$/,
            use: {
                loader: 'babel-loader',
                options: {
                    presets: [
                        "@babel/preset-react", "@babel/preset-env", "mobx"
                    ],
                    plugins: [
                        [
                            "@babel/plugin-proposal-decorators",
                            {
                                "legacy": true
                            }
                        ]
                    ]
                }
            },
            exclude: /node_modules/,

        },
        {
            test:/\.css$/,
            use: ['style-loader', 'css-loader'],
            //    exclude: /node_module/,
        },
        {
            test:/\.(scss|css)$/,
            use:[
                "style-loader",
                {
                    loader:'css-loader',
                    options:{url: false, sourceMap: true},
                },
                {
                    loader:'sass-loader',
                    options: {sourceMap: true}
                }
            ],
            exclude: /node_module/
        },
        {
            test: /\.(png|jpg)$/,
            loader: 'url-loader?limit=8192&name=images/[hash:8].[name].[ext]',
            options: {
                publicPath: '/'
            }
        }, {
            test: /\.mp3$/,
            use: 'file-loader',
        }]
    },
    devtool:"eval-source-map",
    devServer: {
        contentBase: path.join(__dirname, 'dist'),
        port: 8808,
        host: '0.0.0.0',
        proxy: {
            '/remote': {
                target: 'http://localhost', //'http://148.70.157.132',
                pathRewrite: {
                    '^/remote' : ''
                }
            },
            '/app': {
                target: 'http://localhost:8090',
                pathRewrite: {
                    '^/app': ''
                }
            }
        }
    },
    resolve: {
        // 设置别名
        alias: {
            '@': path.join(__dirname, '/app'),
            'components': path.join(__dirname, '/app/components'),
            'ochart': path.join(__dirname, '/app/ochart2'),
            'pages': path.join(__dirname, '/app/pages'),
            'decorate': path.join(__dirname, '/app/decorate'),
            'resolvers': path.join(__dirname, '/app/resolvers'),
            'utils': path.join(__dirname, '/app/utils'),
            'business': path.join(__dirname, '/app/business'),
        }
    },
    plugins: [
        new HtmlWebpackPlugin({
            title: 'Stock Go',
            meta: {
                keywords: 'Stock Go',
                description: '为交易而生'
            },
            inject: true,
            excludeChunks: ['markvo', 'quote'],
            template: path.join(__dirname, '/template.html'),
            filename:'index.html',
            minify: {
                collapseWhitespace: true,
                preserveLineBreaks: true
            }
        })
    ]
}
