# 外汇行情App

#### 介绍
ForexGo是一款外汇行情及分析软件，通过抓取东方财富网，金十数据整合的外汇行情APP。  
服务端采用Koa框架，服务端地址：[https://gitee.com/djxfire/forex-server.git](https://gitee.com/djxfire/forex-server.git)  
Forex实现了主图及副图总共35种指标，如下：  
| 名称      | 说明 | 名称     | 说明 | 名称     | 说明 |
|---------|----|--------|----|--------|----|
| 移动平均线   | 主图 | 顺势指标   | 副图 | 趋向指标   | 副图 |
| 布林线     | 主图 | 收集派发AD | 副图 | 区间震荡指标 | 副图 |
| 包络线     | 主图 | ARBR指标 | 副图 | 简易波动指标 | 副图 |
| 麦克线     | 主图 | 阿隆指标   | 副图 | 强力指数指标 | 副图 |
| 抛物线     | 主图 | 均幅指标   | 副图 | 资金流量指标 | 副图 |
| MACD    | 副图 | 钱德动量   | 副图 | 动量指标   | 副图 |
| KDJ     | 副图 | 中间意愿指标 | 副图 | 负能量指标  | 副图 |
| 乖离率BIAS | 副图 | 相对变异指标 | 副图 | 能量潮指标  | 副图 |
| 相对强弱RSI | 副图 | 平行线差指标 | 副图 | 心理线指标  | 副图 |
| 正能量指标 | 副图 | 变动率指标 | 副图 | 相对活力指标 | 副图 |
| 十字过滤线指标 | 副图 | 成交量变异指标 | 副图 | 威廉多空力度指标 | 副图 |
| 威廉指标 | 副图 | 威廉变异离散动量指标| 副图 | | |  

作为行情分析应用，ForexGo提供了10种分析图表绘制功能，功能及效果如下：
| 名称 |
|---------|
| 直线 |
| 线段 |
| 水平线 |
| 黄金分割线 |
| 江恩线 |
| 黄金分割线 |
| 斐纳波契时间线 |
| 回归线 |
| 甘氏线 |
| 周期线 |

#### 软件架构
![输入图片说明](https://images.gitee.com/uploads/images/2021/0307/195743_76eb11c9_1308501.png "架构图.png")

#### 效果图

##### 全景图
  全景图提供了外汇主要货币涨跌幅的雷达图，以及依据马尔可夫模型预测的预测走势。效果图如下：  
  ![输入图片说明](https://images.gitee.com/uploads/images/2021/0307/185133_b6ee2ad9_1308501.png "首页.png")
##### 资讯
  资讯模块通过抓取金十数据定时推送接收获取最新资讯信息，效果图如下：  
  ![输入图片说明](https://images.gitee.com/uploads/images/2021/0307/185405_db4e7d55_1308501.png "资讯.png") ![输入图片说明](https://images.gitee.com/uploads/images/2021/0307/185513_acf01aef_1308501.png "日历.png")
##### 社区
  社区模块为基础的论坛朋友全功能，包括发布、点赞、评论等基础功能，效果图如下：  
  ![输入图片说明](https://images.gitee.com/uploads/images/2021/0307/185721_34ba5259_1308501.png "社区.png")  
##### 自选行情
  行情功能提供了用户自选、分时图、K线图、画线功能，效果图如下：  
![输入图片说明](https://images.gitee.com/uploads/images/2021/0307/190031_7aee580a_1308501.png "自选.png") ![输入图片说明](https://images.gitee.com/uploads/images/2021/0307/190043_db8e9749_1308501.png "行情图.png") ![输入图片说明](https://images.gitee.com/uploads/images/2021/0307/190103_ff8a137d_1308501.png "直线.png") ![输入图片说明](https://images.gitee.com/uploads/images/2021/0307/190123_2f8b1551_1308501.png "黄金分割线.png") ![输入图片说明](https://images.gitee.com/uploads/images/2021/0307/190136_ce60582d_1308501.png "黄金分割环.png") ![输入图片说明](https://images.gitee.com/uploads/images/2021/0307/190151_0c07ebc9_1308501.png "江恩线.png") ![输入图片说明](https://images.gitee.com/uploads/images/2021/0307/190201_d43f13af_1308501.png "回归线.png") ![输入图片说明](https://images.gitee.com/uploads/images/2021/0307/190224_e19e1f61_1308501.png "甘氏线.png") ![输入图片说明](https://images.gitee.com/uploads/images/2021/0307/190239_44fc1639_1308501.png "K线图.png")
  

#### 安装教程

1.  npm install
2.  npm run start

#### 使用说明

无

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
